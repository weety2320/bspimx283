/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                SylixOS(TM)  LW : long wing
**
**                               Copyright All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: bspLib.c
**
** 创   建   人: Lu.Zheping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: IMX283 需要为 SylixOS 提供的功能支持.
*********************************************************************************************************/
#define  __SYLIXOS_KERNEL
#include "SylixOS.h"
#include "config.h"
#include "driver/regs/regs_timrot.h"
#include "driver/regs/imx283_int.h"
#include "driver/timer/imx28x_timer.h"
#include "driver/int/imx28x_intc.h"
#include "driver/tty/uart.h"
/*********************************************************************************************************
  BSP 信息
*********************************************************************************************************/
static const char   _G_pcCpuInfo[]     = "Freescale (ARM926ej-s Max@454MHz NonFPU)";
static const char   _G_pcCacheInfo[]   = "32KBytes L1-Cache (D-16K/I-16K)";
static const char   _G_pcPacketInfo[]  = CONFIG_IMX28_PKTINFO;
static const char   _G_pcVersionInfo[] = "BSP version 1.1.0 (build time : "__DATE__" "__TIME__")";

/*********************************************************************************************************
  中断相关
*********************************************************************************************************/
/*********************************************************************************************************
** 函数名称: bspIntInit
** 功能描述: 中断系统初始化
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspIntInit (VOID)
{
    /*
     *  初始化 IMX283 中断控制器
     */
    imx28_intc_init(NULL);
}
/*********************************************************************************************************
** 函数名称: bspIntHandle
** 功能描述: 中断入口
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspIntHandle (VOID)
{
    REGISTER UINT32   uiIrqVic = imx28_intc_get_vector();

    archIntHandle((ULONG)uiIrqVic, LW_FALSE);

    imx28_intc_irq_service();
}
/*********************************************************************************************************
** 函数名称: bspIntVecterEnable
** 功能描述: 使能指定的中断向量
** 输  入  : ulVector     向量
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspIntVecterEnable (ULONG  ulVector)
{
    imx28_intc_enable(ulVector);
}
/*********************************************************************************************************
** 函数名称: bspIntVecterDisable
** 功能描述: 禁能指定的中断向量
** 输  入  : ulVector     向量
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspIntVecterDisable (ULONG  ulVector)
{
    imx28_intc_disable(ulVector);
}
/*********************************************************************************************************
** 函数名称: bspIntVecterIsEnable
** 功能描述: 检查指定的中断向量是否
** 输  入  : ulVector     向量
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
BOOL  bspIntVecterIsEnable (ULONG  ulVector)
{
    return imx28_intc_state(ulVector) ? LW_TRUE : LW_FALSE;
}
/*********************************************************************************************************
  BSP 信息
*********************************************************************************************************/
/*********************************************************************************************************
** 函数名称: bspInfoCpu
** 功能描述: BSP CPU 信息
** 输　入  : NONE
** 输　出  : info
** 全局变量:
** 调用模块:
*********************************************************************************************************/
CPCHAR  bspInfoCpu (VOID)
{
    return  (_G_pcCpuInfo);
}
/*********************************************************************************************************
** 函数名称: bspInfoCache
** 功能描述: BSP CACHE 信息
** 输　入  : NONE
** 输　出  : info
** 全局变量:
** 调用模块:
*********************************************************************************************************/
CPCHAR  bspInfoCache (VOID)
{
    return  (_G_pcCacheInfo);
}
/*********************************************************************************************************
** 函数名称: bspInfoPacket
** 功能描述: BSP PACKET 信息
** 输　入  : NONE
** 输　出  : info
** 全局变量:
** 调用模块:
*********************************************************************************************************/
CPCHAR  bspInfoPacket (VOID)
{
    return  (_G_pcPacketInfo);
}
/*********************************************************************************************************
** 函数名称: bspInfoVersion
** 功能描述: BSP VERSION 信息
** 输　入  : NONE
** 输　出  : info
** 全局变量:
** 调用模块:
*********************************************************************************************************/
CPCHAR  bspInfoVersion (VOID)
{
    return  (_G_pcVersionInfo);
}
/*********************************************************************************************************
** 函数名称: bspInfoHwcap
** 功能描述: BSP 硬件特性
** 输　入  : NONE
** 输　出  : 硬件特性 (如果支持硬浮点, 可以加入 HWCAP_VFP , HWCAP_VFPv3 , HWCAP_VFPv3D16 , HWCAP_NEON)
** 全局变量:
** 调用模块:
*********************************************************************************************************/
ULONG  bspInfoHwcap (VOID)
{
    return  (0ul);
}
/*********************************************************************************************************
** 函数名称: bspInfoRomBase
** 功能描述: BSP ROM 信息
** 输　入  : NONE
** 输　出  : info
** 全局变量:
** 调用模块:
*********************************************************************************************************/
addr_t bspInfoRomBase (VOID)
{
    return  (0x00000000);
}
/*********************************************************************************************************
** 函数名称: bspInfoRomSize
** 功能描述: BSP ROM 信息
** 输　入  : NONE
** 输　出  : info
** 全局变量:
** 调用模块:
*********************************************************************************************************/
size_t bspInfoRomSize (VOID)
{
    return  (1024 * 1024 * 0);
}
/*********************************************************************************************************
** 函数名称: bspInfoRamBase
** 功能描述: BSP RAM 信息
** 输　入  : NONE
** 输　出  : info
** 全局变量:
** 调用模块:
*********************************************************************************************************/
addr_t bspInfoRamBase (VOID)
{
    return  (0x40000000);
}
/*********************************************************************************************************
** 函数名称: bspInfoRamSize
** 功能描述: BSP RAM 信息
** 输　入  : NONE
** 输　出  : info
** 全局变量:
** 调用模块:
*********************************************************************************************************/
size_t bspInfoRamSize (VOID)
{
    return  (1024 * 1024 * 128);                                          /*  128M                      */
}
/*********************************************************************************************************
  BSP HOOK
*********************************************************************************************************/
/*********************************************************************************************************
** 函数名称: bspTaskCreateHook
** 功能描述: 任务创建 hook
** 输  入  : ulId     任务 ID
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspTaskCreateHook (LW_OBJECT_ID  ulId)
{
    (VOID)ulId;
}
/*********************************************************************************************************
** 函数名称: bspTaskDeleteHook
** 功能描述: 任务删除 hook
** 输  入  : ulId         任务 ID
**           pvReturnVal  返回值
**           ptcb         任务控制块
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspTaskDeleteHook (LW_OBJECT_ID  ulId, PVOID  pvReturnVal, PLW_CLASS_TCB  ptcb)
{
    (VOID)ulId;
    (VOID)pvReturnVal;
    (VOID)ptcb;
}
/*********************************************************************************************************
** 函数名称: bspTaskSwapHook
** 功能描述: 任务切换 hook
** 输  入  : hOldThread       被换出的任务
**           hNewThread       要运行的任务
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspTaskSwapHook (LW_OBJECT_HANDLE   hOldThread, LW_OBJECT_HANDLE   hNewThread)
{
    (VOID)hOldThread;
    (VOID)hNewThread;
}
/*********************************************************************************************************
** 函数名称: bspTaskIdleHook
** 功能描述: idle 任务调用此函数
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspTaskIdleHook (VOID)
{
}
/*********************************************************************************************************
** 函数名称: bspTaskIdleHook
** 功能描述: 每个操作系统时钟节拍，系统将调用这个函数
** 输  入  : i64Tick      系统当前时钟
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspTickHook (INT64   i64Tick)
{
    (VOID)i64Tick;
}
/*********************************************************************************************************
** 函数名称: bspWdTimerHook
** 功能描述: 看门狗定时器到时间时都会触发这个函数
** 输  入  : ulId     任务 ID
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspWdTimerHook (LW_OBJECT_ID  ulId)
{
    (VOID)ulId;
}
/*********************************************************************************************************
** 函数名称: bspTCBInitHook
** 功能描述: 初始化 TCB 会调用此函数
** 输  入  : ulId     任务 ID
**           ptcb     任务控制块
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspTCBInitHook (LW_OBJECT_ID  ulId, PLW_CLASS_TCB   ptcb)
{
    (VOID)ulId;
    (VOID)ptcb;
}
/*********************************************************************************************************
** 函数名称: bspKernelInitHook
** 功能描述: 系统初始化完成将调用此函数
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspKernelInitHook (VOID)
{
}
/*********************************************************************************************************
** 函数名称: bspReboot
** 功能描述: 系统重新启动
** 输　入  : iRebootType   重启类型
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspReboot (INT  iRebootType, addr_t  ulStartAddress)
{
    (VOID)ulStartAddress;

#ifdef __BOOT_INRAM
    ((VOID (*)(INT))bspInfoRamBase())(iRebootType);
#else
    ((VOID (*)(INT))bspInfoRomBase())(iRebootType);
#endif                                                                  /*  __BOOT_INRAM                */
}
/*********************************************************************************************************
** 函数名称: bspDebugMsg
** 功能描述: 打印系统调试信息
** 输　入  : pcMsg     信息
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspDebugMsg (CPCHAR pcMsg)
{
    for (;(*pcMsg);) {
        send_char((PUCHAR)pcMsg++);
    }
}
/*********************************************************************************************************
  CACHE 相关接口
*********************************************************************************************************/
/*********************************************************************************************************
** 函数名称: bspL2CBase
** 功能描述: 获得 L2 控制器基地址
** 输  入  : pulBase      返回的基地址
** 输  出  : -1 表示不启用 L2
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  bspL2CBase (addr_t *pulBase)
{
    return  (PX_ERROR);
}
/*********************************************************************************************************
** 函数名称: bspL2CAux
** 功能描述: 获得 L2 控制器 Aux 控制掩码
** 输  入  : pulBase      返回的基地址
** 输  出  : -1 表示不启用 L2
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  bspL2CAux (UINT32 *puiAuxVal, UINT32 *puiAuxMask)
{
    *puiAuxVal  =  0u;
    *puiAuxMask = ~0u;
    
    return  (ERROR_NONE);
}
/*********************************************************************************************************
  MMU 相关接口
*********************************************************************************************************/
/*********************************************************************************************************
** 函数名称: bspMmuPgdMaxNum
** 功能描述: 获得 PGD 池的数量
** 输  入  : NONE
** 输  出  : PGD 池的数量 (1 个池可映射 4GB 空间, 推荐返回 1)
** 全局变量:
** 调用模块:
*********************************************************************************************************/
ULONG  bspMmuPgdMaxNum (VOID)
{
    return  (1);
}
/*********************************************************************************************************
** 函数名称: bspMmuPgdMaxNum
** 功能描述: 获得 PTE 池的数量
** 输  入  : NONE
** 输  出  : PTE 池的数量 (映射 4GB 空间, 需要 4096 个 PTE 池)
** 全局变量:
** 调用模块:
*********************************************************************************************************/
ULONG  bspMmuPteMaxNum (VOID)
{
    return  (2048);
}
/*********************************************************************************************************
  操作系统多核接口
*********************************************************************************************************/
/*********************************************************************************************************
** 函数名称: bspMpInt
** 功能描述: 产生一个核间中断
** 输  入  : ulCPUId      目标 CPU
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID   bspMpInt (ULONG  ulCPUId)
{
    (VOID)ulCPUId;
}
/*********************************************************************************************************
** 函数名称: bspCpuUp
** 功能描述: 启动一个 CPU
** 输  入  : ulCPUId      目标 CPU
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID   bspCpuUp (ULONG  ulCPUId)
{
    bspDebugMsg("bspCpuUp() error: this cpu CAN NOT support this operate!\r\n");
}
/*********************************************************************************************************
** 函数名称: bspCpuDown
** 功能描述: 停止一个 CPU
** 输  入  : ulCPUId      目标 CPU
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID   bspCpuDown (ULONG  ulCPUId)
{
    bspDebugMsg("bspCpuDown() error: this cpu CAN NOT support this operate!\r\n");
}
/*********************************************************************************************************
  操作系统 CPU 速度控制接口
*********************************************************************************************************/
/*********************************************************************************************************
** 函数名称: bspSuspend
** 功能描述: 系统进入休眠状态
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID    bspSuspend (VOID)
{
    bspDebugMsg("bspSuspend() error: this BSP CAN NOT support this operate!\r\n");
}
/*********************************************************************************************************
** 函数名称: bspCpuPowerSet
** 功能描述: CPU 设置运行速度
** 输  入  : uiPowerLevel     运行速度级别
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID    bspCpuPowerSet (UINT  uiPowerLevel)
{
}
/*********************************************************************************************************
** 函数名称: bspCpuPowerGet
** 功能描述: CPU 获取运行速度
** 输  入  : puiPowerLevel    运行速度级别
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID    bspCpuPowerGet (UINT  *puiPowerLevel)
{
    if (puiPowerLevel) {
        *puiPowerLevel = LW_CPU_POWERLEVEL_TOP;
    }
}
/*********************************************************************************************************
  操作系统时间相关函数
*********************************************************************************************************/
#define TICK_IN_THREAD  0
#if TICK_IN_THREAD > 0
static LW_HANDLE    htKernelTicks;                                      /*  操作系统时钟服务线程句柄    */
#endif                                                                  /*  TICK_IN_THREAD > 0          */
static IM283_TIMER_DEF(timer0,
                       HW_TIMROT_TIMCTRL0,
                       HW_TIMROT_ROTCTRL_BASE,
                       INUM_TIMER0_IRQ,
                       FREQ_24MHZ);                                     /*  定义系统 TIMER              */
/*********************************************************************************************************
** 函数名称: bspTickHighResolution
** 功能描述: 修正从最近一次 tick 到当前的精确时间.
** 输　入  : ptv       需要修正的时间
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspTickHighResolution (struct timespec *ptv)
{
    timer_tick_high_resolution(&timer0, ptv);
}
/*********************************************************************************************************
** 函数名称: __tickThread
** 功能描述: 初始化 tick 服务线程
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
#if TICK_IN_THREAD > 0

static VOID  __tickThread (VOID)
{
    for (;;) {
        API_ThreadSuspend(htKernelTicks);
        API_KernelTicks();                                              /*  内核 TICKS 通知             */
        API_TimerHTicks();                                              /*  高速 TIMER TICKS 通知       */
    }
}

#endif                                                                  /*  TICK_IN_THREAD > 0          */
/*********************************************************************************************************
** 函数名称: __tickTimer
** 功能描述: tick 定时器中断服务
** 输  入  : NONE
** 输  出  : 中断服务返回
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static irqreturn_t  __tickTimer (struct freescale_timer *timer)
{
    API_KernelTicksContext();                                           /*  保存被时钟中断的线程控制块  */

#if TICK_IN_THREAD > 0
    API_ThreadResume(htKernelTicks);
#else
    API_KernelTicks();                                                  /*  内核 TICKS 通知             */
    API_TimerHTicks();                                                  /*  高速 TIMER TICKS 通知       */
#endif                                                                  /*  TICK_IN_THREAD > 0          */

    timer_isr(timer);

    return  (LW_IRQ_HANDLED);
}
/*********************************************************************************************************
** 函数名称: bspTickInit
** 功能描述: 初始化 tick 时钟
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspTickInit (VOID)
{
#if TICK_IN_THREAD > 0
    LW_CLASS_THREADATTR threadattr;
#endif                                                                  /*  TICK_IN_THREAD > 0          */

    timer_init(&timer0);

#if TICK_IN_THREAD > 0
    API_ThreadAttrBuild(&threadattr, (8 * LW_CFG_KB_SIZE),
                        LW_PRIO_T_TICK,
                        LW_OPTION_THREAD_STK_CHK |
                        LW_OPTION_THREAD_UNSELECT |
                        LW_OPTION_OBJECT_GLOBAL |
                        LW_OPTION_THREAD_SAFE, LW_NULL);

    htKernelTicks = API_ThreadCreate("t_tick",
                                     (PTHREAD_START_ROUTINE)__tickThread,
                                     &threadattr,
                                     NULL);
#endif                                                                  /*  TICK_IN_THREAD > 0          */

    API_InterVectorConnect(timer0.inum,
                           (PINT_SVR_ROUTINE)__tickTimer,
                           &timer0,
                           "tick_timer");

    API_InterVectorEnable(timer0.inum);

    timer_enable(&timer0, LW_TICK_HZ);

    API_InterVectorSetFlag(timer0.inum, LW_IRQ_FLAG_PREEMPTIVE);        /*  TICK 中断可抢占             */
}
/*********************************************************************************************************
** 函数名称: bspDelayUs
** 功能描述: 延迟微秒
** 输  入  : ulUs     微秒数
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID bspDelayUs (ULONG us)
{
    timer_delay_us(us);
}
/*********************************************************************************************************
** 函数名称: bspDelayNs
** 功能描述: 延迟纳秒
** 输  入  : ulNs     纳秒数
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  bspDelayNs (ULONG ulNs)
{
    volatile UINT i;

    while (ulNs) {
        ulNs = (ulNs < 100) ? (0) : (ulNs - 100);
        for (i = 0; i < 9; i++) {
        }
    }
}
/*********************************************************************************************************
  GCC 需求
*********************************************************************************************************/
int __aeabi_read_tp (void)
{
    return  (0);
}
/*********************************************************************************************************
  END
*********************************************************************************************************/


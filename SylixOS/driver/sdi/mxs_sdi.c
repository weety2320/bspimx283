/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                SylixOS(TM)  LW : long wing
**
**                               Copyright All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: mxs_sdi.c
**
** 创   建   人: Zeng.Bo(曾波)
**
** 文件创建日期: 2015 年 06 月 01 日
**
** 描        述: mxs(mx28) SD 主控制器硬件驱动源文件

** BUG:
2015.06.04  增加 DMA 分裂传输功能, 以弥补 DMA 缓冲区大小不能满足上层一次数据请求过大的缺陷.
*********************************************************************************************************/
#define __SYLIXOS_KERNEL
#include "SylixOS.h"
#include "system/device/sdcard/core/sdstd.h"
#include "system/device/sdcard/core/sdiostd.h"
#include <linux/compat.h>

#include "config.h"
#include "driver/clock/clk.h"
#include "driver/regs/mx28.h"
#include "driver/regs/imx283_int.h"
#include "driver/regs/regs_pinctrl.h"
#include "driver/regs/regs_apbh.h"
#include "driver/dma/apbh_dma.h"
#include "mxs_sdi_regs.h"
/*********************************************************************************************************
  宏定义
*********************************************************************************************************/
#define __SDI_CHAN_NUM              4                                   /*  配置使用的通道数量          */

#define __SDI_CHAN0_BASE            SSP0_PHYS_ADDR
#define __SDI_CHAN1_BASE            SSP1_PHYS_ADDR
#define __SDI_CHAN2_BASE            SSP2_PHYS_ADDR
#define __SDI_CHAN3_BASE            SSP3_PHYS_ADDR

#define __SDI_CHAN0_INT             INUM_SSP0_ERROR_IRQ
#define __SDI_CHAN1_INT             INUM_SSP1_ERROR_IRQ
#define __SDI_CHAN2_INT             INUM_SSP2_ERROR_IRQ
#define __SDI_CHAN3_INT             INUM_SSP3_ERROR_IRQ

#define __SDI_DMA0_INT              INUM_SSP0_DMA_IRQ
#define __SDI_DMA1_INT              INUM_SSP1_DMA_IRQ
#define __SDI_DMA2_INT              INUM_SSP2_DMA_IRQ
#define __SDI_DMA3_INT              INUM_SSP3_DMA_IRQ

#define __SDI_PIO_NUM               SSP_PIO_NUM

#define __SDI_SDIOINTSVR_PRIO       197
#define __SDI_SDIOINTSVR_STKSZ      (4 * 1024)
/*********************************************************************************************************
  工具 & 调试相关
*********************************************************************************************************/
#define __SDI_EN_INFO               0

#define __SDI_DEBUG(fmt, arg...)    printk("[SDI] %s() " fmt, __func__, ##arg)

#if __SDI_EN_INFO > 0
#define __SDI_INFO(fmt, arg...)     printk("[SDI] " fmt, ##arg)
#else
#define __SDI_INFO(fmt, arg...)
#endif

#ifndef __OFFSET_OF
#define __OFFSET_OF(t, m)           ((ULONG)((CHAR *)&((t *)0)->m - (CHAR *)(t *)0))
#endif
#ifndef __CONTAINER_OF
#define __CONTAINER_OF(p, t, m)     ((t *)((CHAR *)p - __OFFSET_OF(t, m)))
#endif
/*********************************************************************************************************
   分裂传输配置
*********************************************************************************************************/
#define __SDI_SPLIT_MAX             4
#define __SDI_BUFFER_MAX            (__SDI_SPLIT_MAX * SSP_BUFFER_SIZE)
/*********************************************************************************************************
  GPIO 描述
*********************************************************************************************************/
typedef struct {
    UINT                    SDIGPIO_uiCdPin;
    UINT                    SDIGPIO_uiWpPin;
} __SDI_GPIO;
/*********************************************************************************************************
  通道 描述
*********************************************************************************************************/
struct __sdi_channel;
struct __sdi_dma_channel;
struct __sdi_sdm_host;
typedef struct __sdi_channel     __SDI_CHANNEL;
typedef struct __sdi_dma_channel __SDI_DMA_CHANNEL;
typedef struct __sdi_sdm_host    __SDI_SDM_HOST;
/*********************************************************************************************************
  DMA 通道 描述
*********************************************************************************************************/
struct __sdi_dma_channel {
    UINT                    SDIDMA_uiDmaChannel;
    ULONG                   SDIDMA_uiIntVector;
    UINT8                  *SDIDMA_pucDmaBuffer;
    struct mxs_dma_desc    *SDIDMA_pdmadescTbl[__SDI_SPLIT_MAX];
    __SDI_CHANNEL          *SDIDMA_psdichannel;
};
#define __SDI_DMA_SYNC_BEFORE       0
#define __SDI_DMA_SYNC_AFTER        1
/*********************************************************************************************************
  SDI 分裂传输描述符
  由于 DMA 传输缓冲区有限, 对于上层请求的数据量大于该缓冲区的情况,
  将分为几次 DMA 传输
*********************************************************************************************************/
typedef struct {
    UINT8                  *SDISD_pucDmaBuffer;
    UINT                    SDISD_uiBufferSize;
} __SDI_SPLIT_DESC;
/*********************************************************************************************************
  注册到 SDM 模块的数据结构
*********************************************************************************************************/
struct __sdi_sdm_host {
    SD_HOST                 SDISDMH_sdhost;
    __SDI_CHANNEL          *SDISDMH_psdichannel;
    SD_CALLBACK             SDISDMH_callbackChkDev;
    PVOID                   SDISDMH_pvCallBackArg;
    PVOID                   SDISDMH_pvDevAttached;
};
/*********************************************************************************************************
  SDI 通道结构
*********************************************************************************************************/
struct __sdi_channel {
    __SDI_SDM_HOST          SDICH_sdmhost;                              /*  必须为第一个成员            */
    PVOID                   SDICH_pvSdmHost;                            /*  SDM 层信息                  */

    LW_SD_FUNCS             SDICH_sdfunc;                               /*  SD 总线驱动函数             */

    INT                     SDICH_iChannel;
    CPCHAR                  SDICH_cpcHostName;
    PVOID                   SDICH_pvRegBase;
    INT                     SDICH_iCardSta;
    __SDI_GPIO              SDICH_sdigpio;
    UINT32                  SDICH_uiVersion;
    BOOL                    SDICH_bSdioEnable;
    INT                     SDICH_iBusWidth;
    struct clk             *SDICH_pClockObj;
    UINT32                  SDICH_uiMaxClock;
    ULONG                   SDICH_ulInterVector;
    LW_SPINLOCK_DEFINE     (SDICH_slLock);                              /*  通道基本信息数据            */

    LW_SD_MESSAGE          *SDICH_psdmessage;
    LW_SD_COMMAND          *SDICH_psdcmdCurr;
    UINT32                  SDICH_uiPioWords[__SDI_PIO_NUM];            /*  DMA pio 命令字              */
    BOOL                    SDICH_bIsRead;
    INT                     SDICH_iXferStage;
#define SDI_XFER_STA_NONE          0                                    /*  空闲阶段                    */
#define SDI_XFER_STA_REQ           1                                    /*  执行用户请求阶段            */
#define SDI_XFER_STA_STOP          2                                    /*  执行 STOP 命令阶段          */

    INT                     SDICH_iCmdError;
    INT                     SDICH_iDatError;                            /*  传输状态控制                */

    LW_OBJECT_HANDLE        SDICH_hFinishSync;                          /*  传输完成同步信号            */
    LW_OBJECT_HANDLE        SDICH_hSdioIntSvr;                          /*  负责 SDIO 中断处理线程      */
    LW_OBJECT_HANDLE        SDICH_hSdioIntSem;                          /*  SDIO 中断同步信号           */

    __SDI_DMA_CHANNEL       SDICH_dmachannel;                           /*  DMA 通道信息                */
    __SDI_SPLIT_DESC        SDICH_splitdesc[__SDI_SPLIT_MAX];
    INT                     SDICH_iSplitCnt;                            /*  当前请求所需分裂传输数      */
};

#define __TO_SDMHOST(p)            ((__SDI_SDM_HOST *)p)
#define __TO_SDHOST(p)             ((SD_HOST *)p)
#define __TO_CHANNEL(p)            ((__SDI_CHANNEL *)p)

#define __SDI_LOCK_PREPARE()       INTREG iregInterLevel
#define __SDI_CHANNEL_LOCK(pch)    LW_SPIN_LOCK_QUICK(&pch->SDICH_slLock, &iregInterLevel)
#define __SDI_CHANNEL_UNLOCK(pch)  LW_SPIN_UNLOCK_QUICK(&pch->SDICH_slLock, iregInterLevel)

#define __SDI_SDIO_WAIT(pch)       API_SemaphoreCPend(pch->SDICH_hSdioIntSem, LW_OPTION_WAIT_INFINITE)
#define __SDI_SDIO_NOTIFY(pch)     API_SemaphoreCPost(pch->SDICH_hSdioIntSem)
/*********************************************************************************************************
  全局变量
*********************************************************************************************************/
static __SDI_CHANNEL  _G_sdichannelTbl[__SDI_CHAN_NUM];
/*********************************************************************************************************
  控制器私有功能函数
*********************************************************************************************************/
static INT    __sdiChannelDataInit(INT  iChannel,  UINT uiCdPin,  UINT  uiWpPin);
static UINT32 __sdiClockInit(INT  iChannel);

static INT    __sdiPortInit(__SDI_CHANNEL     *psdichannel);
static INT    __sdiExtRegInit(__SDI_CHANNEL   *psdichannel);
static INT    __sdiHotPlugInit(__SDI_CHANNEL  *psdichannel);
static INT    __sdiInterInit(__SDI_CHANNEL    *psdichannel);
static VOID   __sdiCdScan(__SDI_CHANNEL       *psdichannel);
static INT    __sdiCardStaGet(__SDI_CHANNEL   *psdichannel);
static INT    __sdiCardWpGet(__SDI_CHANNEL    *psdichannel);
static INT    __sdiHwReset(__SDI_CHANNEL      *psdichannel);
static INT    __sdiDevStaCheck(__SDI_CHANNEL  *psdichannel, INT iDevSta);
/*********************************************************************************************************
  SD 总线传输相关
*********************************************************************************************************/
static INT    __sdiBusRegister(__SDI_CHANNEL *psdichannel);

static INT    __sdiTransfer(PLW_SD_ADAPTER   psdadapter,
                            PLW_SD_DEVICE    psddev,
                            PLW_SD_MESSAGE   psdmsg,
                            INT              iNum);
static INT    __sdiIoCtl(PLW_SD_ADAPTER  psdadapter,
                         INT             iCmd,
                         LONG            lArg);
/*********************************************************************************************************
  SD I/O 控制私有函数
*********************************************************************************************************/
static INT    __sdiClockSet(__SDI_CHANNEL     *psdichannel, UINT32 uiSetClk);
static INT    __sdiBusWidthSet(__SDI_CHANNEL  *psdichannel, UINT32 uiBusWidth);
/*********************************************************************************************************
  SDM 层接口函数
*********************************************************************************************************/
static INT    __sdiSdmHostRegister(__SDI_CHANNEL   *psdichannel);
static INT    __sdiSdmCallBackInstall(SD_HOST      *psdhost,
                                      INT           iCallbackType,
                                      SD_CALLBACK   callback,
                                      PVOID         pvCallbackArg);
static INT    __sdiSdmCallBackUnInstall(SD_HOST    *psdhost,
                                        INT         iCallbackType);
static VOID   __sdiSdmSdioIntEn(SD_HOST *psdhost, BOOL bEnable);
static BOOL   __sdiSdmIsCardWp(SD_HOST  *psdhost);
static VOID   __sdiSdmDevAttach(SD_HOST *psdhost, CPCHAR cpcDevName);
static VOID   __sdiSdmDevDetach(SD_HOST *psdhost);
/*********************************************************************************************************
  SDI 内部传输相关
*********************************************************************************************************/
static INT    __sdiXferDataInit(__SDI_CHANNEL     *psdichannel);
static INT    __sdiXferPrepare(__SDI_CHANNEL      *psdichannel, LW_SD_MESSAGE  *psdmsg);
static INT    __sdiXferStart(__SDI_CHANNEL        *psdichannel, INT iXferStage);
static INT    __sdiXferBc(__SDI_CHANNEL           *psdichannel);
static INT    __sdiXferAc(__SDI_CHANNEL           *psdichannel);
static INT    __sdiXferAdtc(__SDI_CHANNEL         *psdichannel);
static INT    __sdiXferFinishWait(__SDI_CHANNEL   *psdichannel);
static INT    __sdiXferFinishHandle(__SDI_CHANNEL *psdichannel);
static VOID   __sdiXferFinishNotify(__SDI_CHANNEL *psdichannel);
static INT    __sdiXferStop(__SDI_CHANNEL        *psdichannel);

static VOID   __sdiSdioIntEn(__SDI_CHANNEL  *psdichannel, BOOL bEnable);
static PVOID  __sdiSdioIntSvr(VOID *pvArg);

static irqreturn_t  __sdiIntIrq(VOID *pvArg, ULONG ulVector);
/*********************************************************************************************************
  SDI DMA 操作
*********************************************************************************************************/
static INT    __sdiDmaChanAlloc(__SDI_CHANNEL *psdichannel);
static INT    __sdiDmaStart(__SDI_CHANNEL     *psdichannel, BOOL bWithData);
static INT    __sdiDmaClean(__SDI_CHANNEL     *psdichannel);
static VOID   __sdiDmaSync(__SDI_CHANNEL      *psdichannel, INT iFlag);

static irqreturn_t  __sdiDmaIrq(VOID *pvArg, ULONG ulVector);
/*********************************************************************************************************
  SDI 分裂传输相关
*********************************************************************************************************/
static INT     __sdiSplitDescInit(__SDI_SPLIT_DESC    *psplitdesc,
                                  LW_SD_MESSAGE       *psdmsgRaw,
                                  UINT8               *pucDmaBufferStart);
/*********************************************************************************************************
** 函数名称: mxsSdiDrvInstall
** 功能描述: 安装 SDI 控制器驱动
** 输    入: iChannel      通道号
**           uiCdPin       卡插拔检测引脚 GPIO 编号
**           uiWpPin       卡写保护引脚 GPIO 编号
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  mxsSdiDrvInstall (INT  iChannel, UINT  uiCdPin, UINT  uiWpPin)
{
    __SDI_CHANNEL  *psdichannel;
    INT             iRet;

    iRet = __sdiChannelDataInit(iChannel, uiCdPin, uiWpPin);
    if (iRet != ERROR_NONE) {
        return  (PX_ERROR);
    }

    psdichannel = &_G_sdichannelTbl[iChannel];

    iRet = __sdiPortInit(psdichannel);
    if (iRet != ERROR_NONE) {
        return  (PX_ERROR);
    }

    iRet = __sdiExtRegInit(psdichannel);
    if (iRet != ERROR_NONE) {
        return  (PX_ERROR);
    }

    iRet = __sdiDmaChanAlloc(psdichannel);
    if (iRet != ERROR_NONE) {
        return  (PX_ERROR);
    }

    iRet = __sdiBusRegister(psdichannel);
    if (iRet != ERROR_NONE) {
        return  (PX_ERROR);
    }

    iRet = __sdiSdmHostRegister(psdichannel);
    if (iRet != ERROR_NONE) {
        return  (PX_ERROR);
    }

    iRet = __sdiXferDataInit(psdichannel);
    if (iRet != ERROR_NONE) {
        return  (PX_ERROR);
    }

    __sdiInterInit(psdichannel);
    __sdiHotPlugInit(psdichannel);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiChannelDataInit
** 功能描述: 通道数据初始化
** 输    入: iChannel      通道号
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiChannelDataInit (INT  iChannel,  UINT  uiCdPin, UINT  uiWpPin)
{
    __SDI_CHANNEL       *psdichannel;
    __SDI_GPIO          *psdigpio;
    UINT32               uiClkRate;
    PVOID                pvRegBase;

    if ((iChannel < 0) || (iChannel >= __SDI_CHAN_NUM)) {
        __SDI_DEBUG("unsupport the ssp channel in current version.\r\n");
        return  (PX_ERROR);
    }

    uiClkRate = __sdiClockInit(iChannel);
    if (uiClkRate == 0) {
        return  (PX_ERROR);
    }

    psdichannel = &_G_sdichannelTbl[iChannel];
    psdigpio    = &psdichannel->SDICH_sdigpio;

    switch (iChannel) {
    case 0:
        /*
         * 由于 bspMap.h 里面已经将外设空间映射好了, 这里无需处理
         */
#if 0
        pvRegBase = API_VmmIoRemapNocache((PVOID)__SDI_CHAN0_BASE, LW_CFG_VMM_PAGE_SIZE);
        if (!pvRegBase) {
            __SDI_DEBUG("failed to remap host%d register space!\n",
                        iChannel);
            return  (PX_ERROR);
        }
#else
        pvRegBase = (VOID *)__SDI_CHAN0_BASE;
#endif

        psdichannel->SDICH_iChannel      = 0;
        psdichannel->SDICH_cpcHostName   = "/bus/sd/0";
        psdichannel->SDICH_iCardSta      = 0;
        psdichannel->SDICH_pvRegBase     = pvRegBase;
        psdichannel->SDICH_uiMaxClock    = uiClkRate;

        psdichannel->SDICH_bSdioEnable   = LW_FALSE;
        psdichannel->SDICH_iBusWidth     = 0;
        psdichannel->SDICH_ulInterVector = __SDI_CHAN0_INT;

        psdigpio->SDIGPIO_uiCdPin        = uiCdPin;
        psdigpio->SDIGPIO_uiWpPin        = uiWpPin;

        LW_SPIN_INIT(&psdichannel->SDICH_slLock);
        break;

    case 1:
        psdichannel->SDICH_iChannel      = 1;
        psdichannel->SDICH_cpcHostName   = "/bus/sd/1";
        psdichannel->SDICH_iCardSta      = 0;
        return  (PX_ERROR);

    case 2:
        return  (PX_ERROR);

    case 3:
        return  (PX_ERROR);

    default:
        return  (PX_ERROR);                                             /*  根据需要添加                */
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: sdiClockInit
** 功能描述: 时钟初始化
** 输　入  : iChannel
** 输　出  : 返回时钟频率 (0 表示失败)
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static UINT32  __sdiClockInit (INT  iChannel)
{
    __SDI_CHANNEL     *psdichannel;
    struct clk        *pSdiClock;
    static const char *pcSspClockName[] = {
        "ssp.0", "ssp.1", "ssp.2", "ssp.3"
    };

    psdichannel = &_G_sdichannelTbl[iChannel];

    pSdiClock = clk_get(LW_NULL, LW_NULL, pcSspClockName[iChannel]);
    if (!pSdiClock) {
        __SDI_DEBUG("can not find the clock module of channel %d.\n", iChannel);
        return  (0);
    }

    psdichannel->SDICH_pClockObj  = pSdiClock;
    psdichannel->SDICH_uiMaxClock = (UINT32)clk_get_rate(pSdiClock);

    clk_enable(pSdiClock);

    __SDI_INFO("sdi channel%d max clock = %d.\r\n", iChannel, psdichannel->SDICH_uiMaxClock);

    return  (psdichannel->SDICH_uiMaxClock);
}
/*********************************************************************************************************
** 函数名称: __sdiPortInit
** 功能描述: 通道端口初始化
** 输    入: psdichannel      通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiPortInit (__SDI_CHANNEL  *psdichannel)
{
    API_GpioRequestOne(psdichannel->SDICH_sdigpio.SDIGPIO_uiCdPin,
                       LW_GPIOF_IN,
                       LW_NULL);

    if (psdichannel->SDICH_sdigpio.SDIGPIO_uiWpPin > 0) {
        API_GpioRequestOne(psdichannel->SDICH_sdigpio.SDIGPIO_uiWpPin,
                           LW_GPIOF_IN,
                           LW_NULL);
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiExtRegInit
** 功能描述: 通道扩展寄存器初始化(硬件复位并初始化)
** 输    入: psdichannel      通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT __sdiExtRegInit (__SDI_CHANNEL  *psdichannel)
{
    psdichannel->SDICH_uiVersion = readl(psdichannel->SDICH_pvRegBase + HW_SSP_VERSION) >>
                                         BP_SSP_VERSION_MAJOR;
    __SDI_INFO("sdi version: %d\n", psdichannel->SDICH_uiVersion);
    __sdiHwReset(psdichannel);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiHotPlugInit
** 功能描述: 热插拔支持初始化
** 输    入: psdichannel      通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiHotPlugInit (__SDI_CHANNEL  *psdichannel)
{
    hotplugPollAdd((VOIDFUNCPTR)__sdiCdScan, (PVOID)psdichannel);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiInterInit
** 功能描述: 通道中断初始化
** 输    入: psdichannel      通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiInterInit (__SDI_CHANNEL   *psdichannel)
{
    API_InterVectorConnect(psdichannel->SDICH_ulInterVector,
                           __sdiIntIrq,
                           (VOID *)psdichannel,
                           "sdi_isr");
    API_InterVectorEnable(psdichannel->SDICH_ulInterVector);

    API_InterVectorConnect(psdichannel->SDICH_dmachannel.SDIDMA_uiIntVector,
                           __sdiDmaIrq,
                           (VOID *)psdichannel,
                           "sdidma_isr");
    API_InterVectorEnable(psdichannel->SDICH_dmachannel.SDIDMA_uiIntVector);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiCdScan
** 功能描述: 设备状态扫描
** 输    入: psdichannel      通道对象
** 输    出: NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __sdiCdScan (__SDI_CHANNEL  *psdichannel)
{
    INT  iStaLast;
    INT  iStaCurr;

    iStaLast = psdichannel->SDICH_iCardSta;
    iStaCurr = __sdiCardStaGet(psdichannel);

    if (iStaLast ^ iStaCurr) {                                          /*  插入状态变化                */
        if (iStaCurr) {
            __SDI_INFO("sdcard insert into channel%d.\r\n", psdichannel->SDICH_iChannel);
            API_SdmEventNotify(psdichannel->SDICH_pvSdmHost,
                               SDM_EVENT_DEV_INSERT);
        } else {
            __SDI_INFO("sdcard removed from channel%d.\r\n", psdichannel->SDICH_iChannel);
            __sdiDevStaCheck(psdichannel, SDHOST_DEVSTA_UNEXIST);
            API_SdmEventNotify(psdichannel->SDICH_pvSdmHost,
                               SDM_EVENT_DEV_REMOVE);
        }

        psdichannel->SDICH_iCardSta = iStaCurr;                         /*  保存状态                    */
    }
}
/*********************************************************************************************************
** 函数名称: __sdiCardStaGet
** 功能描述: 查看设备状态
** 输    入: psdichannel      通道对象
** 输    出: 0: 拔出  1: 插入
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiCardStaGet (__SDI_CHANNEL  *psdichannel)
{
    UINT32  uiStatus;
    uiStatus = API_GpioGetValue(psdichannel->SDICH_sdigpio.SDIGPIO_uiCdPin);
    return  (uiStatus == 0);                                            /*  插入时为低电平              */
}
/*********************************************************************************************************
** 函数名称: __sdiCardStaGet
** 功能描述: 查看设备写状态
** 输    入: psdichannel      通道对象
** 输    出: 0: 未写保护  1: 写保护
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiCardWpGet (__SDI_CHANNEL  *psdichannel)
{
    UINT32  uiStatus;
    if (psdichannel->SDICH_sdigpio.SDIGPIO_uiWpPin > 0) {
        uiStatus = API_GpioGetValue(psdichannel->SDICH_sdigpio.SDIGPIO_uiWpPin);
    } else {
        uiStatus = 0;
    }

    __SDI_INFO("sdi channel%d card insert with %s mode.\r\n",
               psdichannel->SDICH_iChannel,
               uiStatus ? "read-only" : "read-write");

    return  (uiStatus);
}
/*********************************************************************************************************
** 函数名称: __sdiHwReset
** 功能描述: 控制器硬件复位
** 输    入: psdichannel      通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiHwReset (__SDI_CHANNEL  *psdichannel)
{
    UINT32    uiCtrl0;
    UINT32    uiCtrl1;
    VOID     *pvRegBase;

    pvRegBase = psdichannel->SDICH_pvRegBase;

    mxs_reset_block(pvRegBase, 0);

    uiCtrl0 = BM_SSP_CTRL0_IGNORE_CRC;
    uiCtrl1 = BF_SSP(0x3, CTRL1_SSP_MODE)
            | BF_SSP(0x7, CTRL1_WORD_LENGTH)
            | BM_SSP_CTRL1_DMA_ENABLE
            | BM_SSP_CTRL1_POLARITY
            | BM_SSP_CTRL1_RECV_TIMEOUT_IRQ_EN
            | BM_SSP_CTRL1_DATA_CRC_IRQ_EN
            | BM_SSP_CTRL1_DATA_TIMEOUT_IRQ_EN
            | BM_SSP_CTRL1_RESP_TIMEOUT_IRQ_EN
            | BM_SSP_CTRL1_RESP_ERR_IRQ_EN;

    writel(BF_SSP(0xffff, TIMING_TIMEOUT) |
           BF_SSP(2, TIMING_CLOCK_DIVIDE) |
           BF_SSP(0, TIMING_CLOCK_RATE),
           pvRegBase + HW_SSP_TIMING);

    if (psdichannel->SDICH_bSdioEnable) {
        uiCtrl0 |= BM_SSP_CTRL0_SDIO_IRQ_CHECK;
        uiCtrl1 |= BM_SSP_CTRL1_SDIO_IRQ_EN;
    }

    writel(uiCtrl0, pvRegBase + HW_SSP_CTRL0);
    writel(uiCtrl1, pvRegBase + HW_SSP_CTRL1);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiDevStaCheck
** 功能描述: 设备状态监测
** 输    入: psdichannel      通道对象
**           iDevSta          设备状态
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiDevStaCheck (__SDI_CHANNEL  *psdichannel, INT iDevSta)
{
    __SDI_SDM_HOST  *psdmhost;

    psdmhost = __TO_SDMHOST(psdichannel);

    if (psdmhost->SDISDMH_callbackChkDev) {
        psdmhost->SDISDMH_callbackChkDev(psdmhost->SDISDMH_pvCallBackArg, iDevSta);
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiBusRegister
** 功能描述: SD 总线注册
** 输    入: psdichannel      通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiBusRegister (__SDI_CHANNEL *psdichannel)
{
    LW_SD_FUNCS  *psdfunc;
    INT           iError;

    psdfunc = &psdichannel->SDICH_sdfunc;
    psdfunc->SDFUNC_pfuncMasterXfer = __sdiTransfer;
    psdfunc->SDFUNC_pfuncMasterCtl  = __sdiIoCtl;
    iError = API_SdAdapterCreate(psdichannel->SDICH_cpcHostName, psdfunc);
    if (iError != ERROR_NONE) {
        __SDI_DEBUG("create sd adapter error.\r\n");
        return  (PX_ERROR);
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiTransfer
** 功能描述: SD 总线传输函数
** 输    入: psdadapter  SD 总线适配器
**           iCmd        控制命令
**           lArg        参数
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiTransfer (PLW_SD_ADAPTER    psdadapter,
                           PLW_SD_DEVICE     psddev,
                           PLW_SD_MESSAGE    psdmsg,
                           INT               iNum)
{
    __SDI_CHANNEL  *psdichannel;
    INT             iError;
    INT             i = 0;

    psdichannel = __CONTAINER_OF(psdadapter->SDADAPTER_psdfunc, __SDI_CHANNEL, SDICH_sdfunc);

    while ((i < iNum) && (psdmsg != LW_NULL)) {
        iError = __sdiXferPrepare(psdichannel, psdmsg);
        if (iError != ERROR_NONE) {
            return  (PX_ERROR);
        }

        iError = __sdiXferStart(psdichannel, SDI_XFER_STA_REQ);
        if (iError != ERROR_NONE) {
            return  (PX_ERROR);
        }

        iError = __sdiXferFinishWait(psdichannel);
        if (iError != ERROR_NONE) {
            return  (PX_ERROR);
        }

        __sdiXferStop(psdichannel);
        if (psdichannel->SDICH_iCmdError != ERROR_NONE) {
            return  (PX_ERROR);
        }

        if (psdichannel->SDICH_iDatError != ERROR_NONE) {
            return  (PX_ERROR);
        }

        i++;
        psdmsg++;
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiIoCtl
** 功能描述: SD 主控器 IO 控制函数
** 输    入: psdadapter  SD 总线适配器
**           iCmd        控制命令
**           lArg        参数
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiIoCtl (PLW_SD_ADAPTER  psdadapter,
                        INT             iCmd,
                        LONG            lArg)
{
    __SDI_CHANNEL   *psdichannel;
    INT              iError = ERROR_NONE;

    psdichannel = __CONTAINER_OF(psdadapter->SDADAPTER_psdfunc, __SDI_CHANNEL, SDICH_sdfunc);

    switch (iCmd) {
    /*
     * 没有电源控制
     */
    case SDBUS_CTRL_POWEROFF:
    case SDBUS_CTRL_POWERUP:
    case SDBUS_CTRL_POWERON:
        break;

    case SDBUS_CTRL_SETBUSWIDTH:
        iError = __sdiBusWidthSet(psdichannel, (UINT32)lArg);
        break;

    case SDBUS_CTRL_SETCLK:
        iError = __sdiClockSet(psdichannel,  (UINT32)lArg);
        break;

    case SDBUS_CTRL_STOPCLK:
        break;
    case SDBUS_CTRL_STARTCLK:
        break;
    case SDBUS_CTRL_DELAYCLK:
        break;

    case SDBUS_CTRL_GETOCR:
        *(UINT32 *)lArg = SD_VDD_32_33 | SD_VDD_33_34;
        break;

    default:
        __SDI_DEBUG("invalidate command.\r\n");
        iError = PX_ERROR;
        break;
    }

    return  (iError);
}
/*********************************************************************************************************
** 函数名称: __sdiClockSet
** 功能描述: 时钟频率设置并使能
** 输    入: psdichannel     通道对象
**           uiSetClk        要设置的时钟频率
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiClockSet (__SDI_CHANNEL  *psdichannel, UINT32 uiSetClk)
{
    UINT    uiSspClk;
    UINT32  uiClkDiv;
    UINT32  uiClkRate;
    UINT32  uiVal;
    VOID   *pvRegBase = psdichannel->SDICH_pvRegBase;

    uiSspClk = psdichannel->SDICH_uiMaxClock;

    for (uiClkDiv = 2; uiClkDiv <= 254; uiClkDiv += 2) {
        uiClkRate = DIV_ROUND_UP(uiSspClk, uiSetClk * uiClkDiv);
        uiClkRate = (uiClkRate > 0) ? uiClkRate - 1 : 0;
        if (uiClkRate <= 255) {
            break;
        }
    }

    if (uiClkDiv > 254) {
        __SDI_DEBUG("can't set clock to %d.\n", uiSetClk);
        return  (PX_ERROR);
    }

    __SDI_INFO("sdi clock set sck=%d set=%d.\r\n", uiSspClk / uiClkDiv / (1 + uiClkRate), uiSetClk);

    uiVal = readl(pvRegBase + HW_SSP_TIMING);
    uiVal &= ~(BM_SSP_TIMING_CLOCK_DIVIDE | BM_SSP_TIMING_CLOCK_RATE);
    uiVal |= BF_SSP(uiClkDiv, TIMING_CLOCK_DIVIDE);
    uiVal |= BF_SSP(uiClkRate, TIMING_CLOCK_RATE);
    writel(uiVal, pvRegBase + HW_SSP_TIMING);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiBusWidthSet
** 功能描述: 主控总线位宽设置
** 输    入: psdichannel     通道对象
**           uiBusWidth      要设置的总线位宽
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiBusWidthSet (__SDI_CHANNEL  *psdichannel, UINT32 uiBusWidth)
{
    if (uiBusWidth == SDARG_SETBUSWIDTH_4) {
        psdichannel->SDICH_iBusWidth = 1;
    } else {
        psdichannel->SDICH_iBusWidth = 0;
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiSdmHostRegister
** 功能描述: 向 SDM 层注册 HOST 信息
** 输    入: psdichannel     通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiSdmHostRegister (__SDI_CHANNEL  *psdichannel)
{
    __SDI_SDM_HOST *psdmhost;
    SD_HOST        *psdhost;
    PVOID           pvSdmHost;
    INT             iCapablity;

    iCapablity = SDHOST_CAP_DATA_4BIT | SDHOST_CAP_HIGHSPEED;
    psdhost    = __TO_SDHOST(psdichannel);
    psdmhost   = __TO_SDMHOST(psdichannel);

    psdhost->SDHOST_cpcName                = psdichannel->SDICH_cpcHostName;
    psdhost->SDHOST_iType                  = SDHOST_TYPE_SD;
    psdhost->SDHOST_iCapbility             = iCapablity;
    psdhost->SDHOST_pfuncCallbackInstall   = __sdiSdmCallBackInstall;
    psdhost->SDHOST_pfuncCallbackUnInstall = __sdiSdmCallBackUnInstall;
    psdhost->SDHOST_pfuncSdioIntEn         = __sdiSdmSdioIntEn;
    psdhost->SDHOST_pfuncIsCardWp          = __sdiSdmIsCardWp;
    psdhost->SDHOST_pfuncDevAttach         = __sdiSdmDevAttach;
    psdhost->SDHOST_pfuncDevDetach         = __sdiSdmDevDetach;

    pvSdmHost = API_SdmHostRegister(psdhost);
    if (!pvSdmHost) {
        __SDI_DEBUG("can't register into sdm modules.\r\n");
        return  (PX_ERROR);
    }

    psdmhost->SDISDMH_psdichannel = psdichannel;
    psdichannel->SDICH_pvSdmHost  = pvSdmHost;

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiSdmCallBackInstall
** 功能描述: 安装回调函数
** 输    入: psdhost          SDM 层规定的 HOST 信息结构
**           iCallbackType    回调函数类型
**           callback         回调函数指针
**           pvCallbackArg    回调函数参数
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiSdmCallBackInstall (SD_HOST      *psdhost,
                                     INT           iCallbackType,
                                     SD_CALLBACK   callback,
                                     PVOID         pvCallbackArg)
{
    __SDI_SDM_HOST  *psdmhost;
    psdmhost = __TO_SDMHOST(psdhost);
    if (!psdmhost) {
        return  (PX_ERROR);
    }

    if (iCallbackType == SDHOST_CALLBACK_CHECK_DEV) {
        psdmhost->SDISDMH_callbackChkDev = callback;
        psdmhost->SDISDMH_pvCallBackArg  = pvCallbackArg;
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiSdmCallBackUnInstall
** 功能描述: 注销安装的回调函数
** 输    入: psdhost          SDM 层规定的 HOST 信息结构
**           iCallbackType    回调函数类型
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiSdmCallBackUnInstall (SD_HOST    *psdhost,
                                       INT         iCallbackType)
{
    __SDI_SDM_HOST  *psdmhost;
    psdmhost = __TO_SDMHOST(psdhost);
    if (!psdmhost) {
        return  (PX_ERROR);
    }

    if (iCallbackType == SDHOST_CALLBACK_CHECK_DEV) {
        psdmhost->SDISDMH_callbackChkDev = LW_NULL;
        psdmhost->SDISDMH_pvCallBackArg  = LW_NULL;
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiSdmSdioIntEn
** 功能描述: 使能 SDIO 中断
** 输    入: psdhost          SDM 层规定的 HOST 信息结构
**           bEnable          是否使能
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __sdiSdmSdioIntEn (SD_HOST *psdhost, BOOL bEnable)
{
    __sdiSdioIntEn(__TO_CHANNEL(psdhost), bEnable);
}
/*********************************************************************************************************
** 函数名称: __sdiSdmIsCardWp
** 功能描述: 判断该 HOST 上对应的卡是否写保护
** 输    入: psdhost          SDM 层规定的 HOST 信息结构
** 输    出: LW_TRUE: 卡写保护    LW_FALSE: 卡未写保护
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static BOOL  __sdiSdmIsCardWp (SD_HOST  *psdhost)
{
    INT iIsWp;
    iIsWp = __sdiCardWpGet(__TO_CHANNEL(psdhost));
    return  ((BOOL)iIsWp);
}
/*********************************************************************************************************
** 函数名称: __sdiSdmDevAttach
** 功能描述: 添加设备
** 输    入: psdhost          SDM 层规定的 HOST 信息结构
**           cpcDevName       设备的名称
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __sdiSdmDevAttach (SD_HOST *psdhost, CPCHAR cpcDevName)
{
    /*
     * needn't do anything
     */
}
/*********************************************************************************************************
** 函数名称: __sdiSdmDevDetach
** 功能描述: 删除设备
** 输    入: psdhost          SDM 层规定的 HOST 信息结构
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __sdiSdmDevDetach (SD_HOST *psdhost)
{
    /*
     * needn't do anything
     */
}
/*********************************************************************************************************
** 函数名称: __sdiXferDataInit
** 功能描述: 传输相关数据初始化
** 输    入: psdichannel     通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiXferDataInit (__SDI_CHANNEL  *psdichannel)
{
    LW_OBJECT_HANDLE     hFinishSync;
    LW_OBJECT_HANDLE     hSdioSem;
    LW_OBJECT_HANDLE     hSdioSvr;
    LW_CLASS_THREADATTR  threadattr;

    hFinishSync = API_SemaphoreBCreate("mxsdi_sync",
                                       LW_FALSE,
                                       LW_OPTION_OBJECT_GLOBAL,
                                       LW_NULL);
    if (hFinishSync == LW_OBJECT_HANDLE_INVALID) {
        return  (PX_ERROR);
    }

    hSdioSem = API_SemaphoreCCreate("mxsdio_sem",
                                    0,
                                    1024,
                                    LW_OPTION_OBJECT_GLOBAL,
                                    LW_NULL);
    if (hSdioSem == LW_OBJECT_HANDLE_INVALID) {
        goto __err0;
    }

    psdichannel->SDICH_hFinishSync = hFinishSync;
    psdichannel->SDICH_hSdioIntSem = hSdioSem;

    threadattr = API_ThreadAttrGetDefault();
    threadattr.THREADATTR_pvArg            = (PVOID)psdichannel;
    threadattr.THREADATTR_ucPriority       = __SDI_SDIOINTSVR_PRIO;
    threadattr.THREADATTR_stStackByteSize  = __SDI_SDIOINTSVR_STKSZ;
    threadattr.THREADATTR_ulOption        |= LW_OPTION_OBJECT_GLOBAL;
    hSdioSvr = API_ThreadCreate("t_mxsdio",
                                __sdiSdioIntSvr,
                                &threadattr,
                                LW_NULL);
    if (hSdioSvr == LW_OBJECT_HANDLE_INVALID) {
        goto    __err1;
    }

    psdichannel->SDICH_hSdioIntSvr = hSdioSvr;

    return  (ERROR_NONE);

__err1:
    API_SemaphoreCDelete(&hSdioSem);

__err0:
    API_SemaphoreBDelete(&hFinishSync);

    return  (PX_ERROR);
}
/*********************************************************************************************************
** 函数名称: __sdiXferPrepare
** 功能描述: 传输准备
** 输    入: psdichannel     通道对象
**           psdmsg          SD 总线传输消息
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiXferPrepare (__SDI_CHANNEL  *psdichannel, LW_SD_MESSAGE *psdmsg)
{
    LW_SD_DATA  *psddata = psdmsg->SDMSG_psddata;
    INT          iSplitCnt;

    if (psddata) {
        UINT uiBlkSize = psddata->SDDAT_uiBlkSize;

        if ((uiBlkSize < 1) || (psddata->SDDAT_uiBlkNum < 1)) {
            __SDI_DEBUG("block size and block number must be both larger than zero.\r\n");
            return  (PX_ERROR);
        }

        if ((uiBlkSize & (uiBlkSize - 1)) && (psddata->SDDAT_uiBlkNum > 1)) {
            __SDI_DEBUG("block size must be multiple of 2 when blocks larger than 1.\r\n");
            return  (PX_ERROR);
        }

        if (uiBlkSize > SSP_BUFFER_SIZE) {
            __SDI_DEBUG("block size is out of range(%d).\r\n", SSP_BUFFER_SIZE);
            return  (PX_ERROR);
        }

        if ((uiBlkSize * psddata->SDDAT_uiBlkNum) > __SDI_BUFFER_MAX) {
            __SDI_DEBUG("data size is out of range(%d).\r\n", __SDI_BUFFER_MAX);
            return  (PX_ERROR);
        }

        if (SD_DAT_IS_READ(psddata)) {
            psdichannel->SDICH_bIsRead = LW_TRUE;
        } else {
            psdichannel->SDICH_bIsRead = LW_FALSE;
        }

        /*
         * 初始化分裂传输描述
         */
        iSplitCnt = __sdiSplitDescInit(psdichannel->SDICH_splitdesc,
                                       psdmsg,
                                       psdichannel->SDICH_dmachannel.SDIDMA_pucDmaBuffer);
        psdichannel->SDICH_iSplitCnt = iSplitCnt;
    } else {
        psdichannel->SDICH_iSplitCnt = 0;
    }

    psdichannel->SDICH_iCmdError  = ERROR_NONE;
    psdichannel->SDICH_iDatError  = ERROR_NONE;
    psdichannel->SDICH_psdmessage = psdmsg;

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiXferStart
** 功能描述: 启动一次传输
** 输    入: psdichannel     通道对象
**           iXferStat       传输状态
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiXferStart (__SDI_CHANNEL  *psdichannel, INT iXferStage)
{
    LW_SD_COMMAND *psdcmd;
    INT            iError = PX_ERROR;

    switch (iXferStage) {
    case SDI_XFER_STA_REQ:
        psdcmd = psdichannel->SDICH_psdmessage->SDMSG_psdcmdCmd;
        break;
    case SDI_XFER_STA_STOP:
        psdcmd = psdichannel->SDICH_psdmessage->SDMSG_psdcmdStop;
        break;
    default:
        return  (PX_ERROR);
    }

    psdichannel->SDICH_psdcmdCurr = psdcmd;
    psdichannel->SDICH_iXferStage = iXferStage;

    switch (psdcmd->SDCMD_uiFlag & SD_CMD_MASK) {
    case SD_CMD_BC:
        iError = __sdiXferBc(psdichannel);
        break;
    case SD_CMD_BCR:
        iError = __sdiXferAc(psdichannel);
        break;
    case SD_CMD_AC:
        iError = __sdiXferAc(psdichannel);
        break;
    case SD_CMD_ADTC:
        iError = __sdiXferAdtc(psdichannel);
        break;
    default:
        __SDI_DEBUG("invalid SD cmd type.\r\n");
        break;
    }

    return  (iError);
}
/*********************************************************************************************************
** 函数名称: __sdiXferBc
** 功能描述: 处理 BC 类型的命令传输请求(详见 sdcard/core/sdstd.h)
** 输    入: psdichannel     通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiXferBc (__SDI_CHANNEL  *psdichannel)
{
    UINT32           uiCtrl0;
    UINT32           uiCmd0;
    UINT32           uiCmd1;
    INT              iRet;
    LW_SD_COMMAND   *psdcmd = psdichannel->SDICH_psdcmdCurr;

    uiCtrl0 = BM_SSP_CTRL0_ENABLE | BM_SSP_CTRL0_IGNORE_CRC;
    uiCmd0  = BF_SSP(psdcmd->SDCMD_uiOpcode, CMD0_CMD) | BM_SSP_CMD0_APPEND_8CYC;
    uiCmd1  = psdcmd->SDCMD_uiArg;

    if (psdichannel->SDICH_bSdioEnable) {
        uiCtrl0 |= BM_SSP_CTRL0_SDIO_IRQ_CHECK;
        uiCmd0 |= BM_SSP_CMD0_CONT_CLKING_EN | BM_SSP_CMD0_SLOW_CLKING_EN;
    }

    psdichannel->SDICH_uiPioWords[0] = uiCtrl0;
    psdichannel->SDICH_uiPioWords[1] = uiCmd0;
    psdichannel->SDICH_uiPioWords[2] = uiCmd1;

    iRet = __sdiDmaStart(psdichannel, LW_FALSE);
    return  (iRet);
}
/*********************************************************************************************************
** 函数名称: __sdiXferAc
** 功能描述: 处理 AC 类型的命令传输请求(详见 sdcard/core/sdstd.h)
** 输    入: psdichannel     通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiXferAc (__SDI_CHANNEL  *psdichannel)
{
    UINT32           uiIgnoreCrc;
    UINT32           uiGetResp;
    UINT32           uiLongResp;
    UINT32           uiCtrl0;
    UINT32           uiCmd0;
    UINT32           uiCmd1;
    INT              iRet;
    LW_SD_COMMAND   *psdcmd = psdichannel->SDICH_psdcmdCurr;

    uiIgnoreCrc = (SD_RESP_TYPE(psdcmd) & SD_RSP_CRC) ? 0 : BM_SSP_CTRL0_IGNORE_CRC;
    uiGetResp   = (SD_RESP_TYPE(psdcmd) & SD_RSP_PRESENT) ? BM_SSP_CTRL0_GET_RESP : 0;
    uiLongResp  = (SD_RESP_TYPE(psdcmd) & SD_RSP_136) ? BM_SSP_CTRL0_LONG_RESP : 0;

    uiCtrl0 = BM_SSP_CTRL0_ENABLE | uiIgnoreCrc | uiGetResp | uiLongResp;
    uiCmd0  = BF_SSP(psdcmd->SDCMD_uiOpcode, CMD0_CMD);
    uiCmd1  = psdcmd->SDCMD_uiArg;

    if (psdichannel->SDICH_bSdioEnable) {
        uiCtrl0 |= BM_SSP_CTRL0_SDIO_IRQ_CHECK;
        uiCmd0  |= BM_SSP_CMD0_CONT_CLKING_EN | BM_SSP_CMD0_SLOW_CLKING_EN;
    }

    psdichannel->SDICH_uiPioWords[0] = uiCtrl0;
    psdichannel->SDICH_uiPioWords[1] = uiCmd0;
    psdichannel->SDICH_uiPioWords[2] = uiCmd1;

    iRet = __sdiDmaStart(psdichannel, LW_FALSE);

    return  (iRet);
}
/*********************************************************************************************************
** 函数名称: __sdiXferAdtc
** 功能描述: 处理 ADTC 类型的命令传输请求(详见 sdcard/core/sdstd.h)
** 输    入: psdichannel     通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiXferAdtc (__SDI_CHANNEL  *psdichannel)
{
    UINT           uiDataSize = 0;
    UINT           uiLog2BlkSz;
    UINT           uiBlockCnt ;

    UINT32         uiRead;
    UINT32         uiIgnoreCrc;
    UINT32         uiGetResp;
    UINT32         uiLongResp;
    UINT32         uiCtrl0;
    UINT32         uiCmd0;
    UINT32         uiCmd1;
    INT            iRet;

    LW_SD_COMMAND *psdcmd    = psdichannel->SDICH_psdcmdCurr;
    LW_SD_DATA    *psddat    = psdichannel->SDICH_psdmessage->SDMSG_psddata;
    VOID          *pvRegBase = psdichannel->SDICH_pvRegBase;

    if (!psddat) {
        __SDI_DEBUG("no data to transfer with cmd type ADTC.\r\n");
        return  (PX_ERROR);
    }

    uiIgnoreCrc = (SD_RESP_TYPE(psdcmd) & SD_RSP_CRC) ? 0 : BM_SSP_CTRL0_IGNORE_CRC;
    uiGetResp   = (SD_RESP_TYPE(psdcmd) & SD_RSP_PRESENT) ? BM_SSP_CTRL0_GET_RESP : 0;
    uiLongResp  = (SD_RESP_TYPE(psdcmd) & SD_RSP_136) ? BM_SSP_CTRL0_LONG_RESP : 0;

    if (!psdichannel->SDICH_bIsRead) {
        uiRead = 0;
    } else {
        uiRead = BM_SSP_CTRL0_READ;
    }

    uiCtrl0 = BF_SSP(psdichannel->SDICH_iBusWidth, CTRL0_BUS_WIDTH)
            | uiIgnoreCrc | uiGetResp | uiLongResp
            | BM_SSP_CTRL0_DATA_XFER  | uiRead
            | BM_SSP_CTRL0_WAIT_FOR_IRQ
            | BM_SSP_CTRL0_ENABLE;

    uiCmd0 = BF_SSP(psdcmd->SDCMD_uiOpcode, CMD0_CMD);

    /*
     * 当传输多个块时, 块大小寄存器的值为 blksz_reg = log2(BlockSize),
     *    此时XFER_SIZE 寄存器中 XFER_COUNT 必须等于  (1 << blksz_reg) * (blk_cnt + 1)
     * 当传输单个块时, 块大小该寄存器的值被忽略
     *    此时  直接使用XFER_SIZE 寄存器中 XFER_COUNT 的值
     */
    uiLog2BlkSz = SSP_BLKSZ_CVT(psddat->SDDAT_uiBlkSize);
    uiDataSize = psddat->SDDAT_uiBlkNum * psddat->SDDAT_uiBlkSize;
    uiBlockCnt = psddat->SDDAT_uiBlkNum - 1;

    if (ssp_is_old()) {
        uiCtrl0 |= BF_SSP(uiDataSize, CTRL0_XFER_COUNT);
        uiCmd0  |= BF_SSP(uiLog2BlkSz, CMD0_BLOCK_SIZE) | BF_SSP(uiBlockCnt, CMD0_BLOCK_COUNT);
    } else {
        writel(uiDataSize, pvRegBase + HW_SSP_XFER_SIZE);
        writel(BF_SSP(uiLog2BlkSz, BLOCK_SIZE_BLOCK_SIZE) |
               BF_SSP(uiBlockCnt, BLOCK_SIZE_BLOCK_COUNT),
               pvRegBase + HW_SSP_BLOCK_SIZE);
    }

    if ((psdcmd->SDCMD_uiOpcode == SD_STOP_TRANSMISSION) ||
        (psdcmd->SDCMD_uiOpcode == SDIO_RW_EXTENDED)) {
        uiCmd0 |= BM_SSP_CMD0_APPEND_8CYC;
    }

    uiCmd1 = psdcmd->SDCMD_uiArg;

    if (psdichannel->SDICH_bSdioEnable) {
        uiCtrl0 |= BM_SSP_CTRL0_SDIO_IRQ_CHECK;
        uiCmd0  |= BM_SSP_CMD0_CONT_CLKING_EN | BM_SSP_CMD0_SLOW_CLKING_EN;
    }

    psdichannel->SDICH_uiPioWords[0] = uiCtrl0;
    psdichannel->SDICH_uiPioWords[1] = uiCmd0;
    psdichannel->SDICH_uiPioWords[2] = uiCmd1;

    iRet = __sdiDmaStart(psdichannel, LW_TRUE);

    return  (iRet);
}
/*********************************************************************************************************
** 函数名称: __sdiXferFinishWait
** 功能描述: 等待传输完成
** 输    入: psdichannel     通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiXferFinishWait (__SDI_CHANNEL   *psdichannel)
{
    INT iRet;
    iRet = API_SemaphoreBPend(psdichannel->SDICH_hFinishSync, LW_OPTION_WAIT_INFINITE);
    return  (iRet);
}
/*********************************************************************************************************
** 函数名称: __sdiXferFinishNotify
** 功能描述: 通知本次传输完成
** 输    入: psdichannel     通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __sdiXferFinishNotify (__SDI_CHANNEL *psdichannel)
{
    API_SemaphoreBPost(psdichannel->SDICH_hFinishSync);
}
/*********************************************************************************************************
** 函数名称: __sdiXferFinishHandle
** 功能描述: 传输完成处理
** 输    入: psdichannel     通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiXferFinishHandle (__SDI_CHANNEL *psdichannel)
{
    LW_SD_MESSAGE   *psdmsg    = psdichannel->SDICH_psdmessage;
    LW_SD_COMMAND   *psdcmd    = psdmsg->SDMSG_psdcmdCmd;
    LW_SD_DATA      *psddat    = psdmsg->SDMSG_psddata;
    VOID            *pvRegBase = psdichannel->SDICH_pvRegBase;

    if (SD_RESP_TYPE(psdcmd) & SD_RSP_PRESENT) {
        if (SD_RESP_TYPE(psdcmd) & SD_RSP_136) {
            psdcmd->SDCMD_uiResp[3] = readl(pvRegBase + HW_SSP_SDRESP0);
            psdcmd->SDCMD_uiResp[2] = readl(pvRegBase + HW_SSP_SDRESP1);
            psdcmd->SDCMD_uiResp[1] = readl(pvRegBase + HW_SSP_SDRESP2);
            psdcmd->SDCMD_uiResp[0] = readl(pvRegBase + HW_SSP_SDRESP3);
        } else {
            psdcmd->SDCMD_uiResp[0] = readl(pvRegBase + HW_SSP_SDRESP0);
        }
    }

    __sdiDmaClean(psdichannel);                                     /*  清理本次 DMA 传输               */

    if (psddat && (psdichannel->SDICH_iXferStage == SDI_XFER_STA_REQ)) {

        if ((psdichannel->SDICH_iCmdError != ERROR_NONE) ||
            (psdichannel->SDICH_iDatError != ERROR_NONE)) {
            /*
             * 如果有任何传输错误
             * 直接返回
             */
            goto __ret;
        }

        if (psdmsg->SDMSG_psdcmdStop) {
            __sdiXferStart(psdichannel, SDI_XFER_STA_STOP);
            return  (ERROR_NONE);
        }
    }

__ret:
    __sdiXferFinishNotify(psdichannel);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiXferStop
** 功能描述: 结束并清理本次传输
** 输    入: psdichannel     通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiXferStop (__SDI_CHANNEL *psdichannel)
{
    /*
     * 数据传输没有任何错误
     */
    if (psdichannel->SDICH_psdmessage->SDMSG_psddata &&
       (psdichannel->SDICH_iCmdError == ERROR_NONE)  &&
       (psdichannel->SDICH_iDatError == ERROR_NONE)) {
        __sdiDmaSync(psdichannel, __SDI_DMA_SYNC_AFTER);
    }

    psdichannel->SDICH_psdmessage = LW_NULL;
    psdichannel->SDICH_iXferStage = SDI_XFER_STA_NONE;

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiSdioIntEn
** 功能描述: 使能 SDIO 中断
** 输    入: psdichannel   通道对象
**           bEnable       是否使能
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID __sdiSdioIntEn (__SDI_CHANNEL  *psdichannel, BOOL bEnable)
{
    VOID *pvRegBase = psdichannel->SDICH_pvRegBase;

    __SDI_LOCK_PREPARE();

    __SDI_CHANNEL_LOCK(psdichannel);
    if (psdichannel->SDICH_bSdioEnable == bEnable) {
        goto __ret;
    }

    psdichannel->SDICH_bSdioEnable = bEnable;
    if (bEnable) {
        writel(BM_SSP_CTRL0_SDIO_IRQ_CHECK,  pvRegBase + HW_SSP_CTRL0 + MXS_SET_ADDR);
        writel(BM_SSP_CTRL1_SDIO_IRQ_EN, pvRegBase + HW_SSP_CTRL1 + MXS_SET_ADDR);

        if (readl(pvRegBase + HW_SSP_STATUS) & BM_SSP_STATUS_SDIO_IRQ) {
            __SDI_SDIO_NOTIFY(psdichannel);
        }

    } else {
        writel(BM_SSP_CTRL0_SDIO_IRQ_CHECK, pvRegBase + HW_SSP_CTRL0 + MXS_CLR_ADDR);
        writel(BM_SSP_CTRL1_SDIO_IRQ_EN, pvRegBase + HW_SSP_CTRL1 + MXS_CLR_ADDR);
    }

__ret:
    __SDI_CHANNEL_UNLOCK(psdichannel);

    return;
}
/*********************************************************************************************************
** 函数名称: __sdiSdioIntSvr
** 功能描述: SDIO 中断服务线程函数
** 输    入: pvArg      线程参数
** 输    出: 线程返回值
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static PVOID  __sdiSdioIntSvr (VOID *pvArg)
{
    __SDI_CHANNEL   *psdichannel = (__SDI_CHANNEL *)pvArg;

    while (1) {
        __SDI_SDIO_WAIT(psdichannel);
        API_SdmEventNotify(psdichannel->SDICH_pvSdmHost,
                           SDM_EVENT_SDIO_INTERRUPT);
    }

    return  (LW_NULL);
}
/*********************************************************************************************************
** 函数名称: __sdiIntIrq
** 功能描述: SDI 中断服务(主要是处理错误中断)
** 输    入: pvArg      中断服务程序参数
**           ulVector   中断向量号
** 输    出: 中断服务返回值
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static irqreturn_t  __sdiIntIrq (VOID *pvArg, ULONG ulVector)
{
    __SDI_CHANNEL   *psdichannel = (__SDI_CHANNEL *)pvArg;
    LW_SD_DATA      *psddat      = psdichannel->SDICH_psdmessage->SDMSG_psddata;
    VOID            *pvRegBase   = psdichannel->SDICH_pvRegBase;
    UINT32           uiStat;

    __SDI_LOCK_PREPARE();

    __SDI_CHANNEL_LOCK(psdichannel);
    uiStat = readl(pvRegBase + HW_SSP_CTRL1);
    writel(uiStat & MXS_MMC_IRQ_BITS,
           pvRegBase + HW_SSP_CTRL1 + MXS_CLR_ADDR);

    if ((uiStat & BM_SSP_CTRL1_SDIO_IRQ) && (uiStat & BM_SSP_CTRL1_SDIO_IRQ_EN)) {
        __SDI_SDIO_NOTIFY(psdichannel);
    }
    __SDI_CHANNEL_UNLOCK(psdichannel);

    if (uiStat & BM_SSP_CTRL1_RESP_TIMEOUT_IRQ) {
        psdichannel->SDICH_iCmdError = -ETIMEDOUT;
    } else if (uiStat & BM_SSP_CTRL1_RESP_ERR_IRQ) {
        psdichannel->SDICH_iCmdError = -EIO;
    }

    if (psddat && (psdichannel->SDICH_iXferStage == SDI_XFER_STA_REQ)) {
        if (uiStat & (BM_SSP_CTRL1_DATA_TIMEOUT_IRQ | BM_SSP_CTRL1_RECV_TIMEOUT_IRQ)) {
            psdichannel->SDICH_iDatError = -ETIMEDOUT;
        } else if (uiStat & BM_SSP_CTRL1_DATA_CRC_IRQ) {
            psdichannel->SDICH_iDatError = -EILSEQ;
        } else if (uiStat & (BM_SSP_CTRL1_FIFO_UNDERRUN_IRQ | BM_SSP_CTRL1_FIFO_OVERRUN_IRQ)) {
            psdichannel->SDICH_iDatError = -EIO;
        }
    }

    return  (LW_IRQ_HANDLED);
}
/*********************************************************************************************************
** 函数名称: __sdiDmaChanAlloc
** 功能描述: 分配 DMA 通道
** 输    入: psdichannel   通道对象
** 输    出: 中断服务返回值
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiDmaChanAlloc (__SDI_CHANNEL *psdichannel)
{
    __SDI_DMA_CHANNEL    *pdmachannel = &psdichannel->SDICH_dmachannel;
    UINT8                *pucDmaBuffer;
    struct mxs_dma_desc  *pdmadesc;
    INT                   iIntVector;
    UINT                  uiDmaChannel;
    INT                   iRet;
    INT                   i;

    pucDmaBuffer = (UINT8 *)cacheDmaMalloc(__SDI_BUFFER_MAX);
    if (!pucDmaBuffer) {
        __SDI_DEBUG("cannot malloc dma buffer for xfer.\r\n");
        return  (PX_ERROR);
    }

    for (i = 0; i < __SDI_SPLIT_MAX; i++) {
        pdmadesc = mxs_dma_alloc_desc();
        if (!pdmadesc) {
            int j;
            for (j = 0; j < i; j++) {
                mxs_dma_free_desc(pdmachannel->SDIDMA_pdmadescTbl[j]);
            }
            __SDI_DEBUG("cannot malloc dma desc for xfer.\r\n");
            cacheDmaFree(pucDmaBuffer);
            return  (PX_ERROR);
        }

        pdmachannel->SDIDMA_pdmadescTbl[i] = pdmadesc;
    }

    switch (psdichannel->SDICH_iChannel) {
    case 0:
        iIntVector = __SDI_DMA0_INT;
        break;
    case 1:
        iIntVector = __SDI_DMA1_INT;
        break;
    case 2:
        iIntVector = __SDI_DMA2_INT;
        break;
    case 3:
        iIntVector = __SDI_DMA3_INT;
        break;
    default:
        cacheDmaFree(pucDmaBuffer);
        return  (PX_ERROR);
    }

    uiDmaChannel = psdichannel->SDICH_iChannel;
    pdmachannel->SDIDMA_pucDmaBuffer = pucDmaBuffer;
    pdmachannel->SDIDMA_uiIntVector  = iIntVector;
    pdmachannel->SDIDMA_psdichannel  = psdichannel;
    pdmachannel->SDIDMA_uiDmaChannel = psdichannel->SDICH_iChannel;

    iRet = mxs_dma_request(uiDmaChannel);
    if (iRet) {
        __SDI_DEBUG("request dma channel failed.\r\n");
        cacheDmaFree(pucDmaBuffer);
        return  (PX_ERROR);
    }

    mxs_dma_reset(uiDmaChannel);
    mxs_dma_ack_irq(uiDmaChannel);
    mxs_dma_enable_irq(uiDmaChannel, 1);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiDmaStart
** 功能描述: 启动 DMA
** 输    入: psdichannel   通道对象
**           bWithData     是否伴随数据传输
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiDmaStart (__SDI_CHANNEL  *psdichannel, BOOL bWithData)
{
    __SDI_DMA_CHANNEL     *pdmachannel  = &psdichannel->SDICH_dmachannel;
    __SDI_SPLIT_DESC      *psplitdesc   = psdichannel->SDICH_splitdesc;
    struct mxs_dma_desc   *pdmadesc     = pdmachannel->SDIDMA_pdmadescTbl[0];
    UINT                   uiDmaChannel = pdmachannel->SDIDMA_uiDmaChannel;
    INT                    iSplitCnt    = psdichannel->SDICH_iSplitCnt;
    UINT                   uiCmd;
    INT                    i;

    if (!bWithData) {
        pdmadesc->cmd.cmd.data           = 0;
        pdmadesc->cmd.cmd.bits.command   = NO_DMA_XFER;
        pdmadesc->cmd.cmd.bits.irq       = 1;
        pdmadesc->cmd.cmd.bits.dec_sem   = 1;
        pdmadesc->cmd.cmd.bits.wait4end  = 1;
        pdmadesc->cmd.cmd.bits.pio_words = 3;
        pdmadesc->cmd.cmd.bits.bytes     = 0;

        pdmadesc->cmd.pio_words[0] = psdichannel->SDICH_uiPioWords[0];
        pdmadesc->cmd.pio_words[1] = psdichannel->SDICH_uiPioWords[1];
        pdmadesc->cmd.pio_words[2] = psdichannel->SDICH_uiPioWords[2];

        mxs_dma_desc_append(uiDmaChannel, pdmadesc);

        goto __finish;
    }


    /*
     * DMA_READ : 表示读取 DMA 的数据到外设
     * DMA_WRITE: 表示将外设的数据写入 DMA
     */
    if (psdichannel->SDICH_bIsRead) {
        uiCmd = DMA_WRITE;
    } else {
        uiCmd = DMA_READ;
    }
    __sdiDmaSync(psdichannel, __SDI_DMA_SYNC_BEFORE);

    for (i = 0; i < iSplitCnt; i++) {
        pdmachannel->SDIDMA_pdmadescTbl[i]->cmd.cmd.data = 0;
    }

    /*
     * 第一个描述符包括命令发送和数据传输
     * 之后的描述符仅仅是数据传输
     */
    pdmadesc->cmd.cmd.bits.pio_words = 3;
    pdmadesc->cmd.pio_words[0] = psdichannel->SDICH_uiPioWords[0];
    pdmadesc->cmd.pio_words[1] = psdichannel->SDICH_uiPioWords[1];
    pdmadesc->cmd.pio_words[2] = psdichannel->SDICH_uiPioWords[2];

    for (i = 0; i < iSplitCnt; i++) {
        pdmadesc = pdmachannel->SDIDMA_pdmadescTbl[i];

        pdmadesc->cmd.cmd.bits.command = uiCmd;
        pdmadesc->cmd.address          = (dma_addr_t)psplitdesc->SDISD_pucDmaBuffer;
        pdmadesc->cmd.cmd.bits.bytes   = psplitdesc->SDISD_uiBufferSize;

        /*
         * 最后一个描述符
         */
        if (i == (iSplitCnt - 1)) {
            pdmadesc->cmd.cmd.bits.irq       = 1;
            pdmadesc->cmd.cmd.bits.dec_sem   = 1;
            pdmadesc->cmd.cmd.bits.wait4end  = 1;
        }
        mxs_dma_desc_append(uiDmaChannel, pdmadesc);

        psplitdesc++;
    }

__finish:
    mxs_dma_reset(uiDmaChannel);
    mxs_dma_enable(uiDmaChannel);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiDmaClean
** 功能描述: 当前的 DMA 传输完成后的清理工作
** 输    入: psdichannel   通道对象
** 输    出: ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __sdiDmaClean (__SDI_CHANNEL  *psdichannel)
{
    __SDI_DMA_CHANNEL   *pdmachannel  = &psdichannel->SDICH_dmachannel;
    UINT                 uiDmaChannel = pdmachannel->SDIDMA_uiDmaChannel;
    LIST_HEAD           (tmpDescList);

    mxs_dma_cooked(uiDmaChannel, &tmpDescList);
    mxs_dma_ack_irq(uiDmaChannel);
    mxs_dma_disable(uiDmaChannel);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __sdiDmaSync
** 功能描述: DMA 数据同步
** 输    入: pvChannel   通道对象
**           iFlag       同步标志
** 输    出: NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID   __sdiDmaSync (__SDI_CHANNEL   *psdichannel, INT iFlag)
{
    __SDI_DMA_CHANNEL *pdmachannel  = &psdichannel->SDICH_dmachannel;
    PLW_SD_DATA        psddat       = psdichannel->SDICH_psdmessage->SDMSG_psddata;
    UINT8             *pucDmaBuf;
    UINT8             *pucUsrBuf;
    UINT               uiSize;

    uiSize    = psddat->SDDAT_uiBlkSize * psddat->SDDAT_uiBlkNum;
    pucDmaBuf = pdmachannel->SDIDMA_pucDmaBuffer;

    if (iFlag == __SDI_DMA_SYNC_BEFORE) {                               /*  DMA 数据写之前              */
        if (!psdichannel->SDICH_bIsRead) {
            pucUsrBuf = psdichannel->SDICH_psdmessage->SDMSG_pucWrtBuffer;
            lib_memcpy(pucDmaBuf, pucUsrBuf, uiSize);
        }
    } else if (iFlag == __SDI_DMA_SYNC_AFTER) {                         /*  DMA 数据读之后              */
        if (psdichannel->SDICH_bIsRead) {
            pucUsrBuf = psdichannel->SDICH_psdmessage->SDMSG_pucRdBuffer;
            lib_memcpy(pucUsrBuf, pucDmaBuf, uiSize);
        }
    }
}
/*********************************************************************************************************
** 函数名称: __sdiDmaIrq
** 功能描述: DMA 中断服务
** 输    入: pvArg     通道对象
**           ulVector  向量号
** 输    出: NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static irqreturn_t  __sdiDmaIrq (VOID *pvArg, ULONG ulVector)
{
    __SDI_CHANNEL *psdichannel = (__SDI_CHANNEL *)pvArg;
    __sdiXferFinishHandle(psdichannel);
    return  (LW_IRQ_HANDLED);
}
/*********************************************************************************************************
** 函数名称: __sdiSplitXferInit
** 功能描述: 初始化分裂传输描述符
** 输    入: psplitdesc         分裂传输处理对象
**           psdmsgRaw          原始的 SD 请求
**           pucDmaBufferStart  DMA 缓冲区首地址
** 输    出: 分裂传输次数
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT __sdiSplitDescInit (__SDI_SPLIT_DESC    *psplitdesc,
                               LW_SD_MESSAGE       *psdmsgRaw,
                               UINT8               *pucDmaBufferStart)
{
    PLW_SD_DATA   psddat     = psdmsgRaw->SDMSG_psddata;
    UINT8        *pucBufCurr = pucDmaBufferStart;

    UINT          uiSplitCnt;
    UINT          uiRemainSize;
    INT           i;

    uiRemainSize = psddat->SDDAT_uiBlkSize * psddat->SDDAT_uiBlkNum;
    uiSplitCnt   = (uiRemainSize + SSP_BUFFER_SIZE - 1) / SSP_BUFFER_SIZE;

    for (i = 0; i < uiSplitCnt - 1; i++) {
        psplitdesc->SDISD_uiBufferSize = SSP_BUFFER_SIZE;
        psplitdesc->SDISD_pucDmaBuffer = pucBufCurr;

        pucBufCurr   += SSP_BUFFER_SIZE;
        uiRemainSize -= SSP_BUFFER_SIZE;

        psplitdesc++;
    }

    psplitdesc->SDISD_uiBufferSize = uiRemainSize;
    psplitdesc->SDISD_pucDmaBuffer = pucBufCurr;

    __SDI_INFO("sdi split xfer >> "
               "total size: %d  split size: %d  split count: %d  last xfer: %d\n",
               psddat->SDDAT_uiBlkSize * psddat->SDDAT_uiBlkNum,
               SSP_BUFFER_SIZE,
               uiSplitCnt,
               uiRemainSize);

    return  (uiSplitCnt);
}
/*********************************************************************************************************
  END
*********************************************************************************************************/

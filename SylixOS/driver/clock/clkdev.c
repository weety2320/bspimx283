/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: lcd.h
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 03 月 06 日
**
** 描        述: IM28 时钟
*********************************************************************************************************/
#define  __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <linux/err.h>

#include "clk.h"
#include "clock.h"

static LW_LIST_LINE_HEADER clocks;
static LW_SPINLOCK_DEFINE(clocks_lock);

static struct clk *clk_find(const char *dev_id, const char *con_id)
{
    PLW_LIST_LINE  q;
    struct clk_lookup  *p;
    struct clk *clk = NULL;
    int match, best = 0;

    for (q = clocks; q != NULL; q = q->LINE_plistNext) {
        p = _LIST_ENTRY(q, struct clk_lookup, node);
        if (!p) {
            return (NULL);
        }

        match = 0;

        if (p->dev_id) {
            if (!dev_id || strcmp(p->dev_id, dev_id))
                continue;
            match += 2;
        }

        if (p->con_id) {
            if (!con_id || strcmp(p->con_id, con_id))
                continue;
            match += 1;
        }

        if (match > best) {
            clk = p->clk;
            if (match != 3)
                best = match;
            else
                break;
        }
    }

    return clk;
}

struct clk *clk_get(void *dev, const char *name, const char *con_id)
{
    const char *dev_id = name;

    struct clk *clk;

    LW_SPIN_LOCK(&clocks_lock);
    clk = clk_find(dev_id, con_id);
    if (clk && !__clk_get(clk)) {
        clk = NULL;
    }
    LW_SPIN_UNLOCK(&clocks_lock);

    return clk ? clk : ERR_PTR(-ENOENT);
}

void clk_put(struct clk *clk)
{
    __clk_put(clk);
}

struct clk_lookup *clkdev_alloc(struct clk *clk, const char *con_id,
    const char *dev_id)
{
    struct clk_lookup_alloc *cla;

    cla = kzalloc(sizeof(*cla), GFP_KERNEL);
    if (!cla) {
        return NULL;
    }

    cla->cl.clk = clk;
    if (con_id) {
        strlcpy(cla->con_id, con_id, sizeof(cla->con_id));
        cla->cl.con_id = cla->con_id;
    }

    if (dev_id) {
        strlcpy(cla->dev_id, dev_id, sizeof(cla->dev_id));
        cla->cl.dev_id = cla->dev_id;
    }

    return &cla->cl;
}

void clkdev_add(struct clk_lookup *cl)
{
    LW_SPIN_LOCK(&clocks_lock);
    _List_Line_Add_Ahead(&cl->node, &clocks);
    LW_SPIN_UNLOCK(&clocks_lock);
}

void clkdev_drop(struct clk_lookup *cl)
{
    LW_SPIN_LOCK(&clocks_lock);
    _List_Line_Del(&cl->node, &clocks);
    LW_SPIN_UNLOCK(&clocks_lock);
    kfree(cl);
}

/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: imx28x_intc.h
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: IMX283 中断控制器.
*********************************************************************************************************/
#include <string.h>
#include <stdint.h>
#include "driver/regs/imx28_regtype.h"
#include "imx28x_intc.h"
/*********************************************************************************************************
  宏定义
*********************************************************************************************************/
#define __INTC_COUNT 128
/*********************************************************************************************************
  结构体定义
*********************************************************************************************************/
/*********************************************************************************************************
  中断寄存器结构体定义
*********************************************************************************************************/
struct imx28_intc_regs {
    volatile struct imx28_reg vector;
    volatile struct imx28_reg levelack;
    volatile struct imx28_reg ctrl;
    volatile struct imx28_reg reserved0[1];
    volatile struct imx28_reg vbase;
    volatile struct imx28_reg reserved1[2];
    volatile struct imx28_reg stat;
    volatile struct imx28_reg reserved2[2];
    volatile struct imx28_reg raw[4];
    volatile struct imx28_reg reserved3[4];
    volatile struct imx28_reg interrupt[128];
    volatile struct imx28_reg reserved4[128];
    volatile struct imx28_reg debug;
    volatile struct imx28_reg debug_read0;
    volatile struct imx28_reg debug_read1;
    volatile struct imx28_reg debug_flag;
    volatile struct imx28_reg debug_request[4];
    volatile struct imx28_reg reserved5[4];
    volatile struct imx28_reg debug_version;
};
/*********************************************************************************************************
  中断服务函数结构体定义
*********************************************************************************************************/
struct imx28_intc_isr {
    void (*func)(void *arg);
    void  *arg;
};
/*********************************************************************************************************
  结构体定义
*********************************************************************************************************/
static struct imx28_intc_isr   g_isr_objs[__INTC_COUNT];
static struct imx28_intc_regs *thiz = (struct imx28_intc_regs*)0x80000000; /*  default iMX28x           */
static int                     g_pitch_value;
/*********************************************************************************************************
** 函数名称: imx28_intc_init
** 功能描述: 中断初始化
** 输  入  : base :  中断控制器基地址
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void imx28_intc_init (void *base)
{
    if (base) {
        thiz = (struct imx28_intc_regs*)base;
    }
    
    lib_bzero(&g_isr_objs, sizeof(g_isr_objs));
    
    /*
     * reset interrupt controller
     */
    thiz->ctrl.set = (1 << 31);

    /*
     * de-reset interrupt controller
     */
    thiz->ctrl.clr = (1 << 31) | (1 << 30);

    thiz->ctrl.clr = (7 << 21);
    thiz->ctrl.set = (2 << 21);
    g_pitch_value  = 8;
    
    thiz->ctrl.clr = (1 << 19);                                         /*  enable interrupt nesting    */
    thiz->ctrl.set = (1 << 18);                                         /*  ARM-style read side effect  */
    thiz->ctrl.clr = (1 << 17);                                         /*  disable FIQ request         */
    thiz->ctrl.set = (1 << 16);                                         /*  ensable IRQ request         */

    thiz->vbase.dat = (uint32_t)&g_isr_objs;                            /*  set vector table            */
}
/*********************************************************************************************************
** 函数名称: imx28_intc_softirq
** 功能描述: 强制发生一次软中断
** 输  入  : intc_no :  中断号
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void imx28_intc_softirq (enum imx28_int_num intc_no)
{
    if ((intc_no>= 0) && (intc_no < __INTC_COUNT)) {
        thiz->interrupt[intc_no].set = (1 << 3);
    }
}
/*********************************************************************************************************
** 函数名称: imx28_intc_get_vector
** 功能描述: 获得中断服务向量号
** 输  入  : NONE
** 输  出  : 中断号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int imx28_intc_get_vector (void)
{
    char *base = (char*)&g_isr_objs;
    int   vector;

    vector = ((char*)thiz->vector.dat - base) / g_pitch_value;

    return vector;
}
/*********************************************************************************************************
** 函数名称: imx28_intc_irq_service
** 功能描述: 中断服务
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void imx28_intc_irq_service (void)
{
    struct imx28_intc_isr *isr_obj;

    isr_obj = (struct imx28_intc_isr*)thiz->vector.dat;
    if (isr_obj) {
        int priority;
        int vector_number;

        if (isr_obj->func) {
            isr_obj->func(isr_obj->arg);
        }

        vector_number = ((char*)isr_obj - (char*)&g_isr_objs) / g_pitch_value;
        priority = thiz->interrupt[vector_number].dat & 0x3;
        thiz->levelack.dat = (1 << priority);
    }
}
/*********************************************************************************************************
** 函数名称: imx28_intc_enable
** 功能描述: 开中断
** 输  入  : intc_no :  中断向量号
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void imx28_intc_enable (enum imx28_int_num intc_no)
{
    if ((intc_no >= 0) && (intc_no < __INTC_COUNT)) {
        thiz->interrupt[intc_no].set = (1 << 2);
    }
}
/*********************************************************************************************************
** 函数名称: imx28_intc_disable
** 功能描述: 关中断
** 输  入  : intc_no :  中断向量号
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void imx28_intc_disable (enum imx28_int_num intc_no)
{
    if ((intc_no >= 0) && (intc_no < __INTC_COUNT)) {
        thiz->interrupt[intc_no].clr = (1 << 2);
    }
}
/*********************************************************************************************************
** 函数名称: imx28_intc_state
** 功能描述: 获得中断状态(使能、禁能)
** 输  入  : intc_no :  中断向量号
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int imx28_intc_state (enum imx28_int_num intc_no)
{
    volatile int interrupt;

    if ((intc_no >= 0) && (intc_no < __INTC_COUNT)) {
        interrupt = thiz->interrupt[intc_no].dat;

        return  (interrupt & (1 << 2));
    }

    return  (0);
}
/*********************************************************************************************************
  END
*********************************************************************************************************/

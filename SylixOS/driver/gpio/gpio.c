/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: gpio.c
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: GPIO 驱动
*********************************************************************************************************/
#define  __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <linux/compat.h>

#include "gpio.h"
#include "driver/regs/mx28.h"
#include "driver/regs/regs_pinctrl.h"
/*********************************************************************************************************
** 函数名称: mx28GpioSetVoltage
** 功能描述: 设置指定 GPIO 电压
** 输  入  : uiOffset    GPIO 针对 BASE 的偏移量
**           vol         电压选项值
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  mx28GpioSetVoltage (UINT             uiOffset,
                          enum pad_voltage vol)
{
    UINT32     uiAddr;
    UINT32     uiBank;
    UINT32     uiPin;

    uiBank = PINID_2_BANK(uiOffset);
    uiPin  = PINID_2_PIN(uiOffset);
    uiAddr = PINCTRL_PHYS_ADDR + 0x40 * uiBank + 0x10 * (uiPin >> 3);
    uiPin &= 0x7;

    if (vol == PAD_3_3V) {
        writel(1 << (uiPin * 4 + 2), uiAddr + HW_PINCTRL_DRIVE0_SET);
    } else {
        writel(1 << (uiPin * 4 + 2), uiAddr + HW_PINCTRL_DRIVE0_CLR);
    }
}
/*********************************************************************************************************
** 函数名称: mx28GpioSetStrength
** 功能描述: 设置指定 GPIO 电流
** 输  入  : uiOffset    GPIO 针对 BASE 的偏移量
**           strength    电流选项值
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  mx28GpioSetStrength (UINT              uiOffset,
                           enum pad_strength strength)
{
    UINT32     uiAddr;
    UINT32     uiBank;
    UINT32     uiPin;

    uiBank = PINID_2_BANK(uiOffset);
    uiPin  = PINID_2_PIN(uiOffset);
    uiAddr = PINCTRL_PHYS_ADDR + 0x40 * uiBank + 0x10 * (uiPin >> 3);
    uiPin &= 0x7;

    writel(0x3 << (uiPin * 4), uiAddr + HW_PINCTRL_DRIVE0_CLR);
    writel(strength << (uiPin * 4), uiAddr + HW_PINCTRL_DRIVE0_SET);
}
/*********************************************************************************************************
** 函数名称: mx28GpioSetType
** 功能描述: 设置指定 GPIO 类型
** 输  入  : uiOffset    GPIO 针对 BASE 的偏移量
**           strength    电流选项值
**           type        类型选项值
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  mx28GpioSetType (UINT              uiOffset,
                       enum pin_fun      type)
{
    UINT32     uiAddr;
    UINT32     uiBank;
    UINT32     uiPin;

    uiBank = PINID_2_BANK(uiOffset);
    uiPin  = PINID_2_PIN(uiOffset);
    uiAddr = PINCTRL_PHYS_ADDR + 0x20 * uiBank + 0x10 * (uiPin >> 4);
    uiPin &= 0xf;

    writel(0x3 << (uiPin * 2), uiAddr + HW_PINCTRL_MUXSEL0_CLR);
    writel(type << (uiPin * 2), uiAddr + HW_PINCTRL_MUXSEL0_SET);
}
/*********************************************************************************************************
** 函数名称: mx28GpioDirectionInput
** 功能描述: 设置指定 GPIO 为输入模式
** 输  入  : pGpioChip   GPIO 芯片
**           uiOffset    GPIO 针对 BASE 的偏移量
** 输  出  : 0: 正确 -1:错误
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  mx28GpioDirectionInput (PLW_GPIO_CHIP  pGpioChip, UINT  uiOffset)
{
    UINT32     uiAddr;
    UINT32     uiBank;
    UINT32     uiPin;

    uiBank = PINID_2_BANK(uiOffset);
    uiPin  = PINID_2_PIN(uiOffset);
    uiAddr = PINCTRL_PHYS_ADDR + 0x10 * uiBank;

    writel(1 << uiPin, uiAddr + HW_PINCTRL_DOE0_CLR);                     /*  HW_PINCTRL_DOEx_CLR       */

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: s3c2440GpioGet
** 功能描述: 获得指定 GPIO 电平
** 输  入  : pGpioChip   GPIO 芯片
**           uiOffset    GPIO 针对 BASE 的偏移量
** 输  出  : 0: 低电平 1:高电平
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  mx28GpioGet (PLW_GPIO_CHIP  pGpioChip, UINT  uiOffset)
{
    UINT32     uiAddr, uiVal;
    UINT32     uiBank;
    UINT32     uiPin;

    uiBank = PINID_2_BANK(uiOffset);
    uiPin  = PINID_2_PIN(uiOffset);
    uiAddr = PINCTRL_PHYS_ADDR + 0x10 * uiBank;

    uiVal = readl(uiAddr + HW_PINCTRL_DIN0);

    return  ((uiVal & (1 << uiPin)) >> uiPin);
}
/*********************************************************************************************************
** 函数名称: mx28GpioSet
** 功能描述: 设置指定 GPIO 电平
** 输  入  : pGpioChip   GPIO 芯片
**           uiOffset    GPIO 针对 BASE 的偏移量
**           iValue      输出电平
** 输  出  : 0: 正确 -1:错误
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  mx28GpioSet (PLW_GPIO_CHIP  pGpioChip, UINT  uiOffset, INT  iValue)
{
    UINT32     uiAddr;
    UINT32     uiBank;
    UINT32     uiPin;

    uiBank = PINID_2_BANK(uiOffset);
    uiPin  = PINID_2_PIN(uiOffset);
    uiAddr = PINCTRL_PHYS_ADDR + 0x10 * uiBank;

    if (iValue) {
        writel(1 << uiPin, uiAddr + HW_PINCTRL_DOUT0_SET);
    } else {
        writel(1 << uiPin, uiAddr + HW_PINCTRL_DOUT0_CLR);
    }
}
/*********************************************************************************************************
** 函数名称: s3c2440GpioDirectionOutput
** 功能描述: 设置指定 GPIO 为输出模式
** 输  入  : pGpioChip   GPIO 芯片
**           uiOffset    GPIO 针对 BASE 的偏移量
**           iValue      输出电平
** 输  出  : 0: 正确 -1:错误
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  mx28GpioDirectionOutput (PLW_GPIO_CHIP  pGpioChip, UINT  uiOffset, INT  iValue)
{
    UINT32     uiAddr;
    UINT32     uiBank;
    UINT32     uiPin;

    uiBank = PINID_2_BANK(uiOffset);
    uiPin  = PINID_2_PIN(uiOffset);
    uiAddr = PINCTRL_PHYS_ADDR + 0x10 * uiBank;

    writel(1 << uiPin, uiAddr + HW_PINCTRL_DOE0_SET);                     /*  HW_PINCTRL_DOEx_SET       */
    mx28GpioSet(pGpioChip, uiOffset, iValue);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: mx28GpioSetupIrq
** 功能描述: 设置指定 GPIO 为外部中断输入管脚
** 输  入  : pGpioChip   GPIO 芯片
**           uiOffset    GPIO 针对 BASE 的偏移量
**           bIsLevel    是否为电平触发
**           uiType      如果为电平触发, 1 表示高电平触发, 0 表示低电平触发
**                       如果为边沿触发, 1 表示上升沿触发, 0 表示下降沿触发, 2 双边沿触发
** 输  出  : IRQ 向量号 -1:错误
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  mx28GpioSetupIrq (PLW_GPIO_CHIP  pGpioChip, UINT  uiOffset, BOOL  bIsLevel, UINT  uiType)
{
    UINT32     uiAddr;
    UINT32     uiBank;
    UINT32     uiPin;

    uiBank = PINID_2_BANK(uiOffset);
    uiPin  = PINID_2_PIN(uiOffset);
    uiAddr = PINCTRL_PHYS_ADDR + 0x10 * uiBank;

    /*
     *  设置为中断
     */
    writel(1 << uiPin, uiAddr + HW_PINCTRL_IRQEN0_SET);

    if (bIsLevel) {
        /*
         *  电平触发
         */
        writel(1 << uiPin, uiAddr + HW_PINCTRL_IRQLEVEL0_SET);
        if (uiType) {
            /*
             *  高电平触发
             */
            writel(1 << uiPin, uiAddr + HW_PINCTRL_IRQPOL0_SET);
        } else {
            /*
             *  低电平触发
             */
            writel(1 << uiPin, uiAddr + HW_PINCTRL_IRQPOL0_CLR);
        }

    } else {
        /*
         *  边沿触发
         */
        writel(1 << uiPin, uiAddr + HW_PINCTRL_IRQLEVEL0_CLR);
        if (uiType) {
            /*
             * 上升沿触发
             */
            writel(1 << uiPin, uiAddr + HW_PINCTRL_IRQPOL0_CLR);
        } else {
            /*
             *  下降沿触发
             */
            writel(1 << uiPin, uiAddr + HW_PINCTRL_IRQPOL0_SET);
        }
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: s3c2440GpioClearIrq
** 功能描述: 清除指定 GPIO 中断标志
** 输  入  : pGpioChip   GPIO 芯片
**           uiOffset    GPIO 针对 BASE 的偏移量
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  mx28GpioClearIrq (PLW_GPIO_CHIP  pGpioChip, UINT  uiOffset)
{
    UINT32     uiAddr;
    UINT32     uiBank;
    UINT32     uiPin;

    uiBank = PINID_2_BANK(uiOffset);
    uiPin  = PINID_2_PIN(uiOffset);
    uiAddr = PINCTRL_PHYS_ADDR + 0x10 * uiBank;

    writel(1 << uiPin, uiAddr + HW_PINCTRL_IRQSTAT0_CLR);
}
/*********************************************************************************************************
** 函数名称: s3c2440GpioSvrIrq
** 功能描述: 判断 GPIO 中断标志
** 输  入  : pGpioChip   GPIO 芯片
**           uiOffset    GPIO 针对 BASE 的偏移量
** 输  出  : 中断返回值
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static irqreturn_t  mx28GpioSvrIrq (PLW_GPIO_CHIP  pGpioChip, UINT  uiOffset)
{
    UINT32     uiAddr;
    UINT32     uiBank;
    UINT32     uiPin;
    UINT32     uiVal;

    uiBank = PINID_2_BANK(uiOffset);
    uiPin  = PINID_2_PIN(uiOffset);
    uiAddr = PINCTRL_PHYS_ADDR + 0x10 * uiBank;

    uiVal = readl(uiAddr + HW_PINCTRL_IRQSTAT0);
    if (!(uiVal & (1 << uiPin))) {
        return  (LW_IRQ_NONE);
    }

    return  (LW_IRQ_HANDLED);
}
/*********************************************************************************************************
** 函数名称: mx28GpioSetPull
** 功能描述: 设置 GPIO 上拉
** 输  入  : pGpioChip   GPIO 芯片
**           uiOffset    GPIO 针对 BASE 的偏移量
**           uiPull      是否上拉
** 输  出  : 中断返回值
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  mx28GpioSetPull (PLW_GPIO_CHIP  pGpioChip, UINT  uiOffset, UINT  uiPull)
{
    UINT32     uiAddr;
    UINT32     uiBank;
    UINT32     uiPin;

    uiBank = PINID_2_BANK(uiOffset);
    uiPin  = PINID_2_PIN(uiOffset);
    uiAddr = PINCTRL_PHYS_ADDR + 0x10 * uiBank;

    if (uiPull == 1) {
        writel(1 << uiPin, uiAddr + HW_PINCTRL_PULL0_SET);                /*  上拉                      */
    } else {
        writel(1 << uiPin, uiAddr + HW_PINCTRL_PULL0_CLR);
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
  GPIO 驱动程序 (BANK0[29]::BANK1[32]::BANK2[28]::BANK3[31]::BANK4[21] = 141)
*********************************************************************************************************/
static LW_GPIO_CHIP  gpipDrv = {
        .GC_pcLabel              = "MX28 GPIO",

        .GC_pfuncRequest         = LW_NULL,
        .GC_pfuncFree            = LW_NULL,

        .GC_pfuncGetDirection    = LW_NULL,
        .GC_pfuncDirectionInput  = mx28GpioDirectionInput,
        .GC_pfuncGet             = mx28GpioGet,
        .GC_pfuncDirectionOutput = mx28GpioDirectionOutput,
        .GC_pfuncSetDebounce     = LW_NULL,
        .GC_pfuncSetPull         = mx28GpioSetPull,
        .GC_pfuncSet             = mx28GpioSet,
        .GC_pfuncSetupIrq        = mx28GpioSetupIrq,
        .GC_pfuncClearIrq        = mx28GpioClearIrq,
        .GC_pfuncSvrIrq          = mx28GpioSvrIrq,

        .GC_uiBase               = 0,
        .GC_uiNGpios             = 160,                                 /*  0 - 4 BANK(5 * 32)          */
};
/*********************************************************************************************************
** 函数名称: gpioDrvInit
** 功能描述: 初始化 GPIO 驱动
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  gpioDrvInit (VOID)
{
    gpioChipAdd(&gpipDrv);
}
/*********************************************************************************************************
  END
*********************************************************************************************************/


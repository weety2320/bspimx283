/*
 * Copyright (C) 2010 Freescale Semiconductor, Inc.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#define  __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <linux/compat.h>

#include "config.h"
#include "regs_pinctrl.h"
#include "pinctrl.h"
#include "regs_clkctrl.h"
#include "regs_ocotp.h"
#include "mx28.h"
#include "driver/clock/clock.h"
#include "driver/gpio/gpio.h"

/*
 * Structure to define a group of pins and their parameters
 */
struct pin_desc {
    char              *name;
    unsigned           id;
    enum pin_fun       fun;
    enum pad_strength  strength;
    enum pad_voltage   voltage;
    unsigned           pullup:1;
    unsigned           drive:1;
    unsigned           pull:1;
    unsigned           output:1;
    unsigned           data:1;
};

struct pin_group {
    struct pin_desc   *pins;
    int                nr_pins;
};

/*
 * sd0 pin desc
 */
static struct pin_desc sd0_pins_desc[] = {
    {
        .name     = "SD0_CMD",
        .id       = PINID_SSP0_CMD,
        .fun      = PIN_FUN1,
        .strength = PAD_8MA,
        .voltage  = PAD_3_3V,
        .pull     = 1,
        .pullup   = 1,
        .drive    = 1,
    }, {
        .name     = "SD0_SCK",
        .id       = PINID_SSP0_SCK,
        .fun      = PIN_FUN1,
        .strength = PAD_12MA,
        .voltage  = PAD_3_3V,
        .pull     = 0,
        .pullup   = 0,
        .drive    = 1,
    }, {
        .name     = "SD0_D0",
        .id       = PINID_SSP0_DATA0,
        .fun      = PIN_FUN1,
        .strength = PAD_8MA,
        .voltage  = PAD_3_3V,
        .pull     = 1,
        .pullup   = 1,
        .drive    = 1,
    }, {
        .name     = "SD0_D1",
        .id       = PINID_SSP0_DATA1,
        .fun      = PIN_FUN1,
        .strength = PAD_8MA,
        .voltage  = PAD_3_3V,
        .pull     = 1,
        .pullup   = 1,
        .drive    = 1,
    },{
        .name     = "SD0_D2",
        .id       = PINID_SSP0_DATA2,
        .fun      = PIN_FUN1,
        .strength = PAD_8MA,
        .voltage  = PAD_3_3V,
        .pull     = 1,
        .pullup   = 1,
        .drive    = 1,
    },{
        .name     = "SD0_D3",
        .id       = PINID_SSP0_DATA3,
        .fun      = PIN_FUN1,
        .strength = PAD_8MA,
        .voltage  = PAD_3_3V,
        .pull     = 1,
        .pullup   = 1,
        .drive    = 1,
    },{
        .name     = "SD0_WP",
        .id       = PINID_GPMI_CE1N,
        .fun      = PIN_FUN2,
        .strength = PAD_8MA,
        .voltage  = PAD_3_3V,
        .pull     = 0,
        .pullup   = 0,
        .drive    = 1,
    },{
        .name     = "SD0_CD",
        .id       = PINID_SSP0_DETECT,
        .fun      = PIN_FUN2,
        .strength = PAD_8MA,
        .voltage  = PAD_3_3V,
        .pull     = 1,
        .pullup   = 1,
        .drive    = 1,
    },
};
#define SD0_CD_PIN  PINID_SSP0_DETECT
#define SD0_WP_PIN  PINID_GPMI_CE1N

/*
 * ENET pins
 */
static struct pin_desc enet_pins_desc[] = {
     {
         .name = "ENET0_MDC",
         .id = PINID_ENET0_MDC,
         .fun = PIN_FUN1,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET0_MDIO",
         .id = PINID_ENET0_MDIO,
         .fun = PIN_FUN1,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET0_RX_EN",
         .id = PINID_ENET0_RX_EN,
         .fun = PIN_FUN1,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET0_RXD0",
         .id = PINID_ENET0_RXD0,
         .fun = PIN_FUN1,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET0_RXD1",
         .id = PINID_ENET0_RXD1,
         .fun = PIN_FUN1,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET0_TX_EN",
         .id = PINID_ENET0_TX_EN,
         .fun = PIN_FUN1,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET0_TXD0",
         .id = PINID_ENET0_TXD0,
         .fun = PIN_FUN1,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET0_TXD1",
         .id = PINID_ENET0_TXD1,
         .fun = PIN_FUN1,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET1_RX_EN",
         .id = PINID_ENET0_CRS,
         .fun = PIN_FUN2,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET1_RXD0",
         .id = PINID_ENET0_RXD2,
         .fun = PIN_FUN2,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET1_RXD1",
         .id = PINID_ENET0_RXD3,
         .fun = PIN_FUN2,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET1_TX_EN",
         .id = PINID_ENET0_COL,
         .fun = PIN_FUN2,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET1_TXD0",
         .id = PINID_ENET0_TXD2,
         .fun = PIN_FUN2,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET1_TXD1",
         .id = PINID_ENET0_TXD3,
         .fun = PIN_FUN2,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     }, {
         .name = "ENET_CLK",
         .id = PINID_ENET_CLK,
         .fun = PIN_FUN1,
         .strength = PAD_8MA,
         .pull = 1,
         .pullup = 1,
         .voltage = PAD_3_3V,
         .drive = 1,
     },
};

/* Gpmi pins */
static struct pin_desc gpmi_pins_desc[] = {
    {
         .name     = "GPMI D0",
         .id       = PINID_GPMI_D00,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
        .name     = "GPMI D1",
        .id       = PINID_GPMI_D01,
        .fun      = PIN_FUN1,
        .strength = PAD_4MA,
        .voltage  = PAD_3_3V,
        .pullup   = 0,
        .drive    = 1
    }, {
         .name     = "GPMI D2",
         .id       = PINID_GPMI_D02,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI D3",
         .id       = PINID_GPMI_D03,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI D4",
         .id       = PINID_GPMI_D04,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI D5",
         .id       = PINID_GPMI_D05,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI D6",
         .id       = PINID_GPMI_D06,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI D7",
         .id       = PINID_GPMI_D07,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI CE0-",
         .id       = PINID_GPMI_CE0N,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI CE1-",
         .id       = PINID_GPMI_CE1N,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI RDY0",
         .id       = PINID_GPMI_RDY0,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI RDY1",
         .id       = PINID_GPMI_RDY1,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI RD-",
         .id       = PINID_GPMI_RDN,
         .fun      = PIN_FUN1,
         .strength = PAD_12MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI WR-",
         .id       = PINID_GPMI_WRN,
         .fun      = PIN_FUN1,
         .strength = PAD_12MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI ALE",
         .id       = PINID_GPMI_ALE,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI CLE",
         .id       = PINID_GPMI_CLE,
         .fun      = PIN_FUN1,
         .strength = PAD_4MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    }, {
         .name     = "GPMI RST-",
         .id       = PINID_GPMI_RESETN,
         .fun      = PIN_FUN1,
         .strength = PAD_12MA,
         .voltage  = PAD_3_3V,
         .pullup   = 0,
         .drive    = 1
    },
};

/*
 *  lcdif pins
 */
static struct pin_desc lcdif_pins_desc[] = {
    {
         .name  = "LCD_D00",
         .id    = PINID_LCD_D00,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
         .name  = "LCD_D01",
         .id    = PINID_LCD_D01,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
         .name  = "LCD_D02",
         .id    = PINID_LCD_D02,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D03",
         .id    = PINID_LCD_D03,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D04",
         .id    = PINID_LCD_D04,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D05",
         .id    = PINID_LCD_D05,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D06",
         .id    = PINID_LCD_D06,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D07",
         .id    = PINID_LCD_D07,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D08",
         .id    = PINID_LCD_D08,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D09",
         .id    = PINID_LCD_D09,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D10",
         .id    = PINID_LCD_D10,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D11",
         .id    = PINID_LCD_D11,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D12",
         .id    = PINID_LCD_D12,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D13",
         .id    = PINID_LCD_D13,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D14",
         .id    = PINID_LCD_D14,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D15",
         .id    = PINID_LCD_D15,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_D22",
         .id    = PINID_LCD_D22,
         .fun   = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage   = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_RESET",
         .id = PINID_LCD_RESET,
         .fun = PIN_FUN1,
         .strength = PAD_8MA,
         .voltage = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_RD_E",
         .id   = PINID_LCD_RD_E,
         .fun  = PIN_FUN2,
         .strength = PAD_8MA,
         .voltage = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_WR_RWN",
         .id = PINID_LCD_WR_RWN,
         .fun = PIN_FUN2,
         .strength = PAD_8MA,
         .voltage = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_CS",
         .id = PINID_LCD_CS,
         .fun = PIN_FUN2,
         .strength = PAD_8MA,
         .voltage = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "LCD_RS",
         .id = PINID_LCD_RS,
         .fun = PIN_FUN2,
         .strength = PAD_8MA,
         .voltage = PAD_3_3V,
         .drive = 1,
    }, {
            .name  = "PWM3",
         .id = PINID_PWM3,
         .fun = PIN_GPIO,
         .strength = PAD_8MA,
         .voltage = PAD_3_3V,
         .drive = 1,
         .output = 1,
         .data = 1,
    },
};

/*
 *  DUART pins
 */
static struct pin_desc duart_pins_desc[] = {
    {
         .name  = "DUART.RX",
         .id    = PINID_AUART0_CTS,
         .fun   = PIN_FUN3,
    }, {
         .name  = "DUART.TX",
         .id    = PINID_AUART0_RTS,
         .fun   = PIN_FUN3,
    }
};

static struct pin_group sd0_pins = {
    .pins       = sd0_pins_desc,
    .nr_pins    = ARRAY_SIZE(sd0_pins_desc)
};

static struct pin_group enet_pins = {
	.pins		= enet_pins_desc,
	.nr_pins	= ARRAY_SIZE(enet_pins_desc)
};
static struct pin_group gpmi_pins = {
	.pins		= gpmi_pins_desc,
	.nr_pins	= ARRAY_SIZE(gpmi_pins_desc)
};

static struct pin_group lcdif_pins = {
    .pins       = lcdif_pins_desc,
    .nr_pins    = ARRAY_SIZE(lcdif_pins_desc)
};

static struct pin_group duart_pins = {
    .pins       = duart_pins_desc,
    .nr_pins    = ARRAY_SIZE(duart_pins_desc)
};

void mx28_init_pin_group(struct pin_group  *group)
{
    int i;
    struct pin_desc *pin;

    for (i = 0; i < group->nr_pins; i++) {
        pin = &group->pins[i];
        mx28GpioSetType(pin->id, pin->fun);

        if (pin->fun == PIN_GPIO) {
            API_GpioRequest(pin->id, pin->name);
        }

        if (pin->drive) {
            mx28GpioSetStrength(pin->id, pin->strength);
            mx28GpioSetVoltage(pin->id, pin->voltage);
        }

        if (pin->pull) {
            mx28GpioSetPull(NULL, pin->id, pin->pullup);
        }

        if (pin->fun == PIN_GPIO) {
            if (pin->output) {
                API_GpioDirectionOutput(pin->id, LW_GPIOF_DIR_OUT |
                                        (pin->data ? LW_GPIOF_INIT_HIGH : LW_GPIOF_INIT_LOW));
            } else {
                API_GpioDirectionInput(pin->id);
            }
        }
    }
}

void sd_board_init(void)
{
    mx28_init_pin_group(&sd0_pins);
}
int sd_pin_get(int channel, int *wp, int *cd)
{
    switch (channel) {
    case 0:
        *cd = SD0_CD_PIN;
        *wp = SD0_WP_PIN;
        break;
        /*
         * 根据硬件情况返回合适的值
         */
    case 1:
        break;
    case 2:
        break;
    default:
        return  (-1);
    }

    return  (0);
}

void enet_board_init(void)
{
	/*
	 * Set up ENET pins
	 */
    mx28_init_pin_group(&enet_pins);
}

void lcdif_port_init(void)
{
    /*
     *  Set up LCDIF pins
     */
    mx28_init_pin_group(&lcdif_pins);
}

void setup_gpmi_nand(void)
{
	/*
	 * Set up GPMI pins
	 */
    mx28_init_pin_group(&gpmi_pins);
}

void duart_port_init(void)
{
    /*
     * Set up DUART pins
     */
    mx28_init_pin_group(&duart_pins);
}

static int __mxs_reset_block(void __iomem *hwreg, int just_enable)
{
    u32 c;
    int timeout;

    /* the process of software reset of IP block is done
       in several steps:

       - clear SFTRST and wait for block is enabled;
       - clear clock gating (CLKGATE bit);
       - set the SFTRST again and wait for block is in reset;
       - clear SFTRST and wait for reset completion.
     */
    c = readl(hwreg);
    c &= ~(1 << 31);    /* clear SFTRST */
    writel(c, hwreg);
    for (timeout = 1000000; timeout > 0; timeout--)
        /* still in SFTRST state ? */
        if ((readl(hwreg) & (1 << 31)) == 0)
            break;
    if (timeout <= 0) {
        printk(KERN_ERR "%s(%p): timeout when enabling\n",
               __func__, hwreg);
        return -ETIME;
    }

    c = readl(hwreg);
    c &= ~(1 << 30);    /* clear CLKGATE */
    writel(c, hwreg);

    if (!just_enable) {
        c = readl(hwreg);
        c |= (1 << 31); /* now again set SFTRST */
        writel(c, hwreg);
        for (timeout = 1000000; timeout > 0; timeout--) {
            /* poll until CLKGATE set */
            if (readl(hwreg) & (1 << 30)) {
                break;
            }
        }
        if (timeout <= 0) {
            printk(KERN_ERR "%s(%p): timeout when resetting\n",
                   __func__, hwreg);
            return -ETIME;
        }

        c = readl(hwreg);
        c &= ~(1 << 31);    /* clear SFTRST */
        writel(c, hwreg);
        for (timeout = 1000000; timeout > 0; timeout--) {
            /* still in SFTRST state ? */
            if ((readl(hwreg) & (1 << 31)) == 0) {
                break;
            }
        }

        if (timeout <= 0) {
            printk(KERN_ERR "%s(%p): timeout when enabling "
                   "after reset\n", __func__, hwreg);
            return -ETIME;
        }

        c = readl(hwreg);
        c &= ~(1 << 30);    /* clear CLKGATE */
        writel(c, hwreg);
    }

    for (timeout = 1000000; timeout > 0; timeout--) {
        /* still in SFTRST state ? */
        if ((readl(hwreg) & (1 << 30)) == 0) {
            break;
        }
    }

    if (timeout <= 0) {
        printk(KERN_ERR "%s(%p): timeout when unclockgating\n",
               __func__, hwreg);
        return -ETIME;
    }

    return 0;
}

int mxs_reset_block(void *hwreg, int just_enable)
{
    int try = 10;
    int r;

    while (try--) {
        r = __mxs_reset_block(hwreg, just_enable);
        if (!r) {
            break;
        }

        pr_debug("%s: try %d failed\n", __func__, 10 - try);
    }
    return r;
}

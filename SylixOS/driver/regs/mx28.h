/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: mx28.h
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述:
*********************************************************************************************************/
/*
 * Copyright (C) 2008 Embedded Alley Solutions Inc.
 *
 * (C) Copyright 2009-2010 Freescale Semiconductor, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __MX28_H
#define __MX28_H

#define MX28_SOC_IO_PHYS_BASE   0x80000000

#define ICOLL_PHYS_ADDR     (MX28_SOC_IO_PHYS_BASE + 0x000000)
#define HSADC_PHYS_ADDR     (MX28_SOC_IO_PHYS_BASE + 0x002000)
#define APBH_DMA_PHYS_ADDR  (MX28_SOC_IO_PHYS_BASE + 0x004000)
#define PERFMON_PHYS_ADDR   (MX28_SOC_IO_PHYS_BASE + 0x006000)
#define BCH_PHYS_ADDR       (MX28_SOC_IO_PHYS_BASE + 0x00A000)
#define GPMI_PHYS_ADDR      (MX28_SOC_IO_PHYS_BASE + 0x00C000)
#define SSP0_PHYS_ADDR      (MX28_SOC_IO_PHYS_BASE + 0x010000)
#define SSP1_PHYS_ADDR      (MX28_SOC_IO_PHYS_BASE + 0x012000)
#define SSP2_PHYS_ADDR      (MX28_SOC_IO_PHYS_BASE + 0x014000)
#define SSP3_PHYS_ADDR      (MX28_SOC_IO_PHYS_BASE + 0x016000)
#define PINCTRL_PHYS_ADDR   (MX28_SOC_IO_PHYS_BASE + 0x018000)
#define DIGCTL_PHYS_ADDR    (MX28_SOC_IO_PHYS_BASE + 0x01C000)
#define ETM_PHYS_ADDR       (MX28_SOC_IO_PHYS_BASE + 0x022000)
#define APBX_DMA_PHYS_ADDR  (MX28_SOC_IO_PHYS_BASE + 0x024000)
#define DCP_PHYS_ADDR       (MX28_SOC_IO_PHYS_BASE + 0x028000)
#define PXP_PHYS_ADDR       (MX28_SOC_IO_PHYS_BASE + 0x02A000)
#define OCOTP_PHYS_ADDR     (MX28_SOC_IO_PHYS_BASE + 0x02C000)
#define AXI_AHB0_PHYS_ADDR  (MX28_SOC_IO_PHYS_BASE + 0x02E000)
#define LCDIF_PHYS_ADDR     (MX28_SOC_IO_PHYS_BASE + 0x030000)
#define CAN0_PHYS_ADDR      (MX28_SOC_IO_PHYS_BASE + 0x032000)
#define CAN1_PHYS_ADDR      (MX28_SOC_IO_PHYS_BASE + 0x034000)
#define SIMDBG_PHYS_ADDR    (MX28_SOC_IO_PHYS_BASE + 0x03C000)
#define SIMGPMISEL_PHYS_ADDR    (MX28_SOC_IO_PHYS_BASE + 0x03C200)
#define SIMSSPSEL_PHYS_ADDR (MX28_SOC_IO_PHYS_BASE + 0x03C300)
#define SIMMEMSEL_PHYS_ADDR (MX28_SOC_IO_PHYS_BASE + 0x03C400)
#define GPIOMON_PHYS_ADDR   (MX28_SOC_IO_PHYS_BASE + 0x03C500)
#define SIMENET_PHYS_ADDR   (MX28_SOC_IO_PHYS_BASE + 0x03C700)
#define ARMJTAG_PHYS_ADDR   (MX28_SOC_IO_PHYS_BASE + 0x03C800)
#define CLKCTRL_PHYS_ADDR   (MX28_SOC_IO_PHYS_BASE + 0x040000)
#define SAIF0_PHYS_ADDR     (MX28_SOC_IO_PHYS_BASE + 0x042000)
#define POWER_PHYS_ADDR     (MX28_SOC_IO_PHYS_BASE + 0x044000)
#define SAIF1_PHYS_ADDR     (MX28_SOC_IO_PHYS_BASE + 0x046000)
#define LRADC_PHYS_ADDR     (MX28_SOC_IO_PHYS_BASE + 0x050000)
#define SPDIF_PHYS_ADDR     (MX28_SOC_IO_PHYS_BASE + 0x054000)
#define RTC_PHYS_ADDR       (MX28_SOC_IO_PHYS_BASE + 0x056000)
#define I2C0_PHYS_ADDR      (MX28_SOC_IO_PHYS_BASE + 0x058000)
#define I2C1_PHYS_ADDR      (MX28_SOC_IO_PHYS_BASE + 0x05A000)
#define PWM_PHYS_ADDR       (MX28_SOC_IO_PHYS_BASE + 0x064000)
#define TIMROT_PHYS_ADDR    (MX28_SOC_IO_PHYS_BASE + 0x068000)
#define AUART0_PHYS_ADDR    (MX28_SOC_IO_PHYS_BASE + 0x06A000)
#define AUART1_PHYS_ADDR    (MX28_SOC_IO_PHYS_BASE + 0x06C000)
#define AUART2_PHYS_ADDR    (MX28_SOC_IO_PHYS_BASE + 0x06E000)
#define AUART3_PHYS_ADDR    (MX28_SOC_IO_PHYS_BASE + 0x070000)
#define AUART4_PHYS_ADDR    (MX28_SOC_IO_PHYS_BASE + 0x072000)
#define DUART_PHYS_ADDR     (MX28_SOC_IO_PHYS_BASE + 0x074000)
#define USBPHY0_PHYS_ADDR   (MX28_SOC_IO_PHYS_BASE + 0x07C000)
#define USBPHY1_PHYS_ADDR   (MX28_SOC_IO_PHYS_BASE + 0x07E000)
#define USBCTRL0_PHYS_ADDR  (MX28_SOC_IO_PHYS_BASE + 0x080000)
#define USBCTRL1_PHYS_ADDR  (MX28_SOC_IO_PHYS_BASE + 0x090000)
#define DFLPT_PHYS_ADDR     (MX28_SOC_IO_PHYS_BASE + 0x0C0000)
#define DRAM_PHYS_ADDR      (MX28_SOC_IO_PHYS_BASE + 0x0E0000)
#define ENET_PHYS_ADDR      (MX28_SOC_IO_PHYS_BASE + 0x0F0000)

#define IO_ADDRESS(x)       (x)

/*
 *  Init imx283 pins
 */
void sd_board_init(void);
int  sd_pin_get(int channel, int *wp, int *cd);

void enet_board_init(void);
void lcdif_port_init(void);
void setup_gpmi_nand(void);
void duart_port_init(void);

/*
 *  Reset block
 */
int mxs_reset_block(void *hwreg, int just_enable);

#endif /* __MX28_H */

/**********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: imx283int_num.h
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: IMX283 中断向量号.
*********************************************************************************************************/

#ifndef __IMX283_INT_H__
#define __IMX283_INT_H__

enum imx28_int_num
{
    INUM_BATT_BROWNOUT_IRQ = 0  ,                                        /* battery brownout detect IRQ */
    INUM_VDDD_BROWNOUT_IRQ     ,                                         /* VDDD brownout detect IRQ    */
    INUM_VDDIO_BROWNOUT_IRQ    ,                                         /* VDDIO brownout detect IRQ   */
    INUM_VDDA_BROWNOUT_IRQ     ,                                         /* VDDA brownout detect IRQ    */
    INUM_VDD5V_DROOP_IRQ       ,                                         /* 5V Droop IRQ,               */
    INUM_DCDC4P2_BROWNOUT_IRQ  ,                                         /* 4.2V regulated supply       */
                                                                         /* brown-out IRQ               */
    INUM_VDD5V_IRQ             ,                                         /* IRQ on 5V connect or        */
                                                                         /* disconnect also OTG 4.2V    */
    INUM_7_RESERVED            ,                                         /* Reserved                    */
    INUM_CAN0_IRQ              ,                                         /* CAN0 IRQ.                   */
    INUM_CAN1_IRQ              ,                                         /* CAN1 IRQ.                   */
    INUM_LRADC_TOUCH_IRQ       ,                                         /* Touch detection IRQ.        */
    INUM_11_RESERVED           ,                                         /* Reserved                    */
    INUM_12_RESERVED           ,                                         /* Reserved                    */
    INUM_HSADC_IRQ             ,                                         /* HSADC IRQ.                  */
    INUM_LRADC_THRESH0_IRQ     ,                                         /* LRADC0 Threshold IRQ.       */
    INUM_LRADC_THRESH1_IRQ     ,                                         /* LRADC1 Threshold IRQ.       */
    INUM_LRADC_CH0_IRQ         ,                                         /* LRADC Channel 0 conversion  */
                                                                         /* complete IRQ.               */
    INUM_LRADC_CH1_IRQ         ,                                         /* LRADC Channel 1 conversion  */
                                                                         /* complete IRQ.               */
    INUM_LRADC_CH2_IRQ         ,                                         /* LRADC Channel 2 conversion  */
                                                                         /* complete IRQ.               */
    INUM_LRADC_CH3_IRQ         ,                                         /* LRADC Channel 3 conversion  */
                                                                         /* complete IRQ.               */
    INUM_LRADC_CH4_IRQ         ,                                         /* LRADC Channel 4 conversion  */
                                                                         /* complete IRQ.               */
    INUM_LRADC_CH5_IRQ         ,                                         /* LRADC Channel 5 conversion  */
                                                                         /* complete IRQ.               */
    INUM_LRADC_CH6_IRQ         ,                                         /* LRADC Channel 6 conversion  */
                                                                         /* complete IRQ.               */
    INUM_LRADC_CH7_IRQ         ,                                         /* LRADC Channel 7 conversion  */
                                                                         /* complete IRQ.               */
    INUM_LRADC_BUTTON0_IRQ     ,                                         /* LRADC Channel 0 button      */
                                                                         /* detection IRQ.              */
    INUM_LRADC_BUTTON1_IRQ     ,                                         /* LRADC Channel 1 button      */
                                                                         /* detection IRQ.              */
    INUM_26_RESERVED           ,                                         /* Reserved                    */
    INUM_PERFMON_IRQ           ,                                         /* Performance monitor IRQ.    */
    INUM_RTC_1MSEC_IRQ         ,                                         /* RTC 1ms event IRQ.          */
    INUM_RTC_ALARM_IRQ         ,                                         /* RTC alarm event IRQ.        */
    INUM_30_RESERVED           ,                                         /* Reserved                    */
    INUM_COMMS_IRQ             ,                                         /* JTAG debug communications   */
                                                                         /* port IRQ.                   */
    INUM_EMI_ERROR_IRQ         ,                                         /* External memory controller  */
                                                                         /* IRQ.                        */
    INUM_33_RESERVED           ,                                         /* Reserved                    */
    INUM_34_RESERVED           ,                                         /* Reserved                    */
    INUM_35_RESERVED           ,                                         /* Reserved                    */
    INUM_36_RESERVED           ,                                         /* Reserved                    */
    INUM_RESERVED              ,                                         /* Reserved                    */
    INUM_LCDIF_IRQ             ,                                         /* LCDIF IRQ.                  */
    INUM_PXP_IRQ               ,                                         /* PXP IRQ.                    */
    INUM_40_RESERVED           ,                                         /* Reserved                    */
    INUM_BCH_IRQ               ,                                         /* BCH consolidated IRQ.       */
    INUM_GPMI_IRQ              ,                                         /* GPMI internal error and     */
                                                                         /* status IRQ.                 */
    INUM_43_RESERVED           ,                                         /* Reserved                    */
    INUM_44_RESERVED           ,                                         /* Reserved                    */
    INUM_SPDIF_ERROR_IRQ       ,                                         /* SPDIF FIFO error IRQ.       */
    INUM_46_RESERVED           ,                                         /* Reserved                    */
    INUM_DUART_IRQ             ,                                         /* Debug UART IRQ.             */
    INUM_TIMER0_IRQ            ,                                         /* Timer0 IRQ, recommend to    */
                                                                         /* set as FIQ.                 */
    INUM_TIMER1_IRQ            ,                                         /* Timer1 IRQ, recommend to    */
                                                                         /* set as FIQ.                 */
    INUM_TIMER2_IRQ            ,                                         /* Timer2 IRQ, recommend to    */
                                                                         /* set as FIQ.                 */
    INUM_TIMER3_IRQ            ,                                         /* Timer3 IRQ, recommend to    */
                                                                         /* set as FIQ.                 */
    INUM_DCP_VMI_IRQ           ,                                         /* DCP Channel 0 virtual       */
                                                                         /* memory page copy IRQ        */
    INUM_DCP_IRQ               ,                                         /* DCP (per channel and CSC)   */
                                                                         /* IRQ.                        */
    INUM_DCP_SECURE_IRQ        ,                                         /* DCP secure IRQ.             */
    INUM_55_RESERVED           ,                                         /* Reserved                    */
    INUM_56_RESERVED           ,                                         /* Reserved                    */
    INUM_57_RESERVED           ,                                         /* Reserved                    */
    INUM_SAIF1_IRQ             ,                                         /* SAIF1 FIFO & Service error  */
                                                                         /* IRQ.                        */
    INUM_SAIF0_IRQ             ,                                         /* SAIF0 FIFO & Service error  */
                                                                         /* IRQ.                        */
    INUM_60_RESERVED           ,                                         /* Reserved                    */
    INUM_61_RESERVED           ,                                         /* Reserved                    */
    INUM_62_RESERVED           ,                                         /* Reserved                    */
    INUM_63_RESERVED           ,                                         /* Reserved                    */
    INUM_64_RESERVED           ,                                         /* Reserved                    */
    INUM_65_RESERVED           ,                                         /* Reserved                    */
    INUM_SPDIF_DMA_IRQ         ,                                         /* SPDIF DMA channel IRQ.      */
    INUM_67_RESERVED           ,                                         /* Reserved                    */
    INUM_I2C0_DMA_IRQ          ,                                         /* I2C0 DMA channel IRQ.       */
    INUM_I2C1_DMA_IRQ          ,                                         /* I2C1 DMA channel IRQ.       */
    INUM_AUART0_RX_DMA_IRQ     ,                                         /* Application UART0 receiver  */
                                                                         /* DMA channel IRQ.            */
    INUM_AUART0_TX_DMA_IRQ     ,                                         /* Application UART0           */
                                                                         /* transmitter DMA channel IRQ.*/
    INUM_AUART1_RX_DMA_IRQ     ,                                         /* Application UART1 receiver  */
                                                                         /* DMA channel IRQ.            */
    INUM_AUART1_TX_DMA_IRQ     ,                                         /* Application UART1           */
                                                                         /* transmitter DMA             */
    INUM_AUART2_RX_DMA_IRQ     ,                                         /* Application UART2 receiver  */
                                                                         /* DMA channel IRQ.            */
    INUM_AUART2_TX_DMA_IRQ     ,                                         /* Application UART2           */
                                                                         /* transmitter DMA channel IRQ.*/
    INUM_AUART3_RX_DMA_IRQ     ,                                         /* Application UART3 receiver  */
                                                                         /* DMA channel IRQ.            */
    INUM_AUART3_TX_DMA_IRQ     ,                                         /* Application UART3           */
                                                                         /* transmitter DMA channel IRQ.*/
    INUM_AUART4_RX_DMA_IRQ     ,                                         /* Application UART4           */
                                                                         /* receiver DMA channel IRQ.   */
    INUM_AUART4_TX_DMA_IRQ     ,                                         /* Application UART4           */
                                                                         /* transmitter DMA channel IRQ.*/
    INUM_SAIF0_DMA_IRQ         ,                                         /* SAIF0 DMA channel IRQ.      */
    INUM_SAIF1_DMA_IRQ         ,                                         /* SAIF1 DMA channel IRQ.      */
    INUM_SSP0_DMA_IRQ          ,                                         /* SSP0 DMA channel IRQ.       */
    INUM_SSP1_DMA_IRQ          ,                                         /* SSP1 DMA channel IRQ.       */
    INUM_SSP2_DMA_IRQ          ,                                         /* SSP2 DMA channel IRQ.       */
    INUM_SSP3_DMA_IRQ          ,                                         /* SSP3 DMA channel IRQ.       */
    INUM_LCDIF_DMA_IRQ         ,                                         /* LCDIF DMA channel IRQ.      */
    INUM_HSADC_DMA_IRQ         ,                                         /* HSADC DMA channel IRQ.      */
    INUM_GPMI_DMA_IRQ          ,                                         /* GPMI DMA channel IRQ.       */
    INUM_DIGCTL_DEBUG_TRAP_IRQ ,                                         /* Layer 0 or Layer 3 AHB      */
                                                                         /* address access trap IRQ.    */
    INUM_90_RESERVED           ,                                         /* Reserved                    */
    INUM_91_RESERVED           ,                                         /* Reserved                    */
    INUM_USB1_IRQ              ,                                         /* USB1 IRQ.                   */
    INUM_USB0_IRQ              ,                                         /* USB0 IRQ.                   */
    INUM_USB1_WAKEUP_IRQ       ,                                         /* UTM1 IRQ.                   */
    INUM_USB0_WAKEUP_IRQ       ,                                         /* UTM0 IRQ.                   */
    INUM_SSP0_ERROR_IRQ        ,                                         /* SSP0 device-level error     */
                                                                         /* and status IRQ.             */
    INUM_SSP1_ERROR_IRQ        ,                                         /* SSP1 device-level error     */
                                                                         /* and status IRQ.             */
    INUM_SSP2_ERROR_IRQ        ,                                         /* SSP2 device-level error     */
                                                                         /* and status IRQ.             */
    INUM_SSP3_ERROR_IRQ        ,                                         /* SSP3 device-level error     */
                                                                         /* and status IRQ.             */
    INUM_ENET_SWI_IRQ          ,                                         /* Switch IRQ.                 */
    INUM_ENET_MAC0_IRQ         ,                                         /* MAC0 IRQ.                   */
    INUM_ENET_MAC1_IRQ         ,                                         /* MAC1 IRQ.                   */
    INUM_ENET_MAC0_1588_IRQ    ,                                         /* 1588 of MAC0 IRQ.           */
    INUM_ENET_MAC1_1588_IRQ    ,                                         /* 1588 of MAC1 IRQ.           */
    INUM_105_RESERVED          ,                                         /* Reserved                    */
    INUM_106_RESERVED          ,                                         /* Reserved                    */
    INUM_107_RESERVED          ,                                         /* Reserved                    */
    INUM_108_RESERVED          ,                                         /* Reserved                    */
    INUM_109_RESERVED          ,                                         /* Reserved                    */
    INUM_I2C1_ERROR_IRQ        ,                                         /* I2C1 device detected errors */
                                                                         /* and line conditionsIRQ.     */
    INUM_I2C0_ERROR_IRQ        ,                                         /* I2C0 device detected errors */
                                                                         /* and line conditions IRQ. */
    INUM_AUART0_IRQ            ,                                         /* Application UART0 internal  */
                                                                         /* error IRQ.                  */
    INUM_AUART1_IRQ            ,                                         /* Application UART1 internal  */
                                                                         /* error IRQ.                  */
    INUM_AUART2_IRQ            ,                                         /* Application UART2 internal  */
                                                                         /* error IRQ.                  */
    INUM_AUART3_IRQ            ,                                         /* Application UART3 internal  */
                                                                         /* error IRQ.                  */
    INUM_AUART4_IRQ            ,                                         /* Application UART4 internal  */
                                                                         /* error IRQ.                  */
    INUM_117_RESERVED          ,                                         /* Reserved                    */
    INUM_118_RESERVED          ,                                         /* Reserved                    */
    INUM_119_RESERVED          ,                                         /* Reserved                    */
    INUM_120_RESERVED          ,                                         /* Reserved                    */
    INUM_121_RESERVED          ,                                         /* Reserved                    */
    INUM_PINCTRL5_IRQ          ,                                         /* GPIO bank 5 interrupt IRQ.  */
    INUM_PINCTRL4_IRQ          ,                                         /* GPIO bank 4 interrupt IRQ.  */
    INUM_PINCTRL3_IRQ          ,                                         /* GPIO bank 3 interrupt IRQ.  */
    INUM_PINCTRL2_IRQ          ,                                         /* GPIO bank 2 interrupt IRQ.  */
    INUM_PINCTRL1_IRQ          ,                                         /* GPIO bank 1 interrupt IRQ.  */
    INUM_PINCTRL0_IRQ          ,                                         /* GPIO bank 0 interrupt IRQ.  */
};

#endif                                                                  /*  __IMX283_INT_H__            */
/*********************************************************************************************************
  END
*********************************************************************************************************/

/**********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: imx28common.h
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: IMX28 控制时钟文件.
*********************************************************************************************************/

#ifndef __IMX28REGTYPE_H__
#define __IMX28REGTYPE_H__

#include <stdint.h>

/*********************************************************************************************************
  结构体定义
*********************************************************************************************************/
/*********************************************************************************************************
  寄存器结构定义
*********************************************************************************************************/
struct imx28_reg {
    volatile uint32_t dat;                                              /*  data register               */
    volatile uint32_t set;                                              /*  set register                */
    volatile uint32_t clr;                                              /*  clear register              */
    volatile uint32_t tog;                                              /*  toggle register             */
};
/*********************************************************************************************************
  时钟控制寄存器定义
*********************************************************************************************************/
union _hw_clkctrl_xtal {
    struct {
        volatile uint32_t DIV_UART           :2;
        volatile uint32_t RSRVD1             :24;
        volatile uint32_t TIMROT_CLK32K_GATE :1;
        volatile uint32_t RSRVD2             :2;
        volatile uint32_t PWM_CLK24M_GATE    :1;
        volatile uint32_t RSRVD3             :1;
        volatile uint32_t UART_CLK_GATE      :1;
    };
    volatile uint32_t     raw;
};
/*********************************************************************************************************
  时钟控制寄存器结构定义
*********************************************************************************************************/
struct _hw_clkctrl_xtal_ {
    volatile union _hw_clkctrl_xtal dat;
    volatile union _hw_clkctrl_xtal set;
    volatile union _hw_clkctrl_xtal clr;
    volatile union _hw_clkctrl_xtal tog;
};

#endif                                                                  /*  __IMX28REGTYPE_H__          */
/*********************************************************************************************************
  END
*********************************************************************************************************/










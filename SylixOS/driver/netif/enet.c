/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: enet.c
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: IM283 以太网控制器驱动
** BUG:
**
2015.04.04  去掉在中断中关中断，添加系统信号量、消息队列
            LW_OPTION_OBJECT_GLOBAL参数.
2015.05.05  重新规划驱动结构，和内存队列结构.
*********************************************************************************************************/
#define  __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <linux/compat.h>
#include <endian.h>

#include "netif/etharp.h"
#include "lwip/ethip6.h"
#include "lwip/netif.h"
#include "lwip/netifapi.h"
#include "lwip/sys.h"
#include "lwip/stats.h"
#include "lwip/snmp.h"
#include "lwip/ip4_addr.h"
#include "lwip/tcpip.h"
#include "net/if.h"

#include "config.h"
#include "enet.h"
#include "driver/regs/mx28.h"
#include "driver/regs/imx283_int.h"
#include "driver/regs/regs_clkctrl.h"
#include "driver/clock/clk.h"
#include "driver/regs/regs_ocotp.h"
/*********************************************************************************************************
  宏定义
*********************************************************************************************************/
/*********************************************************************************************************
  ENET-MAC Programmable Registers (MCIMX28RM.pdf-->26.4) (部分)
*********************************************************************************************************/
#define HW_ENET_MAC_EIR     (0x004)                                       /*  Interrupt Event Register  */
#define HW_ENET_MAC_EIMR    (0x008)                                       /*  Interrupt Mask Register   */
#define HW_ENET_MAC_RDAR    (0x010)                                       /*  Receive Descriptor Active */
                                                                          /*  Register                  */
#define HW_ENET_MAC_TDAR    (0x014)                                       /*  Transmit Descriptor Active*/
                                                                          /*  Register                  */
#define HW_ENET_MAC_ECR     (0x024)                                       /*  Control Register          */
#define HW_ENET_MAC_MMFR    (0x040)                                       /*  MII Management Frame      */
                                                                          /*  Register                  */
#define HW_ENET_MAC_MSCR    (0x044)                                       /*  MII Speed Control Register*/
#define HW_ENET_MAC_MIBC    (0x064)                                       /*  MIB Control/Status Register*/
#define HW_ENET_MAC_RCR     (0x084)                                       /*  Receive Control Register  */
#define HW_ENET_MAC_TCR     (0x0C4)                                       /*  Transmit Control reg      */
#define HW_ENET_MAC_PALR    (0x0E4)                                       /*  Low 32bits MAC address    */
#define HW_ENET_MAC_PAUR    (0x0E8)                                       /*  High 16bits MAC address   */
#define HW_ENET_MAC_OPD     (0x0EC)                                       /*  Opcode + Pause duration   */
#define HW_ENET_MAC_IAUR    (0x118)                                       /*  High 32bits hash table    */
#define HW_ENET_MAC_IALR    (0x11C)                                       /*  Low 32bits hash table     */
#define HW_ENET_MAC_GAUR    (0x120)                                       /*  High 32bits hash table    */
#define HW_ENET_MAC_GALR    (0x124)                                       /*  Low 32bits hash table     */
#define HW_ENET_MAC_TFW_SFCR (0x144)                                      /*  FIFO transmit water mark  */
#define HW_ENET_MAC_FRBR    (0x14C)                                       /*  FIFO receive bound reg    */
#define HW_ENET_MAC_FRSR    (0x150)                                       /*  FIFO receive start reg    */
#define HW_ENET_MAC_ERDSR   (0x180)                                       /*  Receive descriptor ring   */
#define HW_ENET_MAC_ETDSR   (0x184)                                       /*  Transmit descriptor ring  */
#define HW_ENET_MAC_EMRBR   (0x188)                                       /*  Maximum receive buff size */
/*********************************************************************************************************
  int event type (MCIMX28RM.pdf--> 26.4.1)
*********************************************************************************************************/
#define ENET_EIR_HBERR      ((UINT)0x80000000)                            /*  Heartbeat error           */
#define ENET_EIR_BABR       ((UINT)0x40000000)                            /*  Babbling receiver         */
#define ENET_EIR_BABT       ((UINT)0x20000000)                            /*  Babbling transmitter      */
#define ENET_EIR_GRA        ((UINT)0x10000000)                            /*  Graceful stop complete    */
#define ENET_EIR_TXF        ((UINT)0x08000000)                            /*  Full frame transmitted    */
#define ENET_EIR_TXB        ((UINT)0x04000000)                            /*  A buffer was transmitted  */
#define ENET_EIR_RXF        ((UINT)0x02000000)                            /*  Full frame received       */
#define ENET_EIR_RXB        ((UINT)0x01000000)                            /*  A buffer was received     */
#define ENET_EIR_MII        ((UINT)0x00800000)                            /*  MII interrupt             */
#define ENET_EIR_EBERR      ((UINT)0x00400000)                            /*  SDMA bus error            */
#define ENET_EIR_TS_AVAIL   ((UINT)0x00010000)
#define ENET_EIR_TS_TIMER   ((UINT)0x00008000)
#define ENET_DEFAULT_INTE   (ENET_EIR_TXF |                               \
                             ENET_EIR_RXF |                               \
                             ENET_EIR_MII )
#define ENET_EVENT_LINK     (0x00000001)
/*********************************************************************************************************
  ENET MAC Control Register  (MCIMX28RM.pdf--> 26.4.5)
*********************************************************************************************************/
#define ENET_ECR_RESET      (0x00000001)                                  /*  ENET-MAC reset            */
#define ENET_ECR_ETHER_EN   (0x00000002)                                  /*  MAC is enabled            */
#define ENET_ECR_MAGIC_ENA  (0x00000004)                                  /*  Enable Magic Packet Detection */
#define ENET_ECR_SLEEP      (0x00000008)                                  /*  controller in sleep mode  */
#define ENET_ECR_ENA_1588   (0x00000010)                                  /*  IEEE1588 Enable           */
#define ENET_ECR_DBG_EN     (0X00000040)                                  /*  Enables the debug input pin*/
/*********************************************************************************************************
  ENET MAC Physical Address Upper Register (MCIMX28RM.pdf--> 26.4.12)
*********************************************************************************************************/
#define ENET_PAUR_PAUSE_F_T (0x8808)                                      /*  Type field in PAUSE frames*/
/*********************************************************************************************************
  ENET MAC Receive Descriptor Active Register (MCIMX28RM.pdf--> 26.4.3)
*********************************************************************************************************/
#define ENET_RDAR_RX_ACTIVE (0x01000000)
/*********************************************************************************************************
  ENET MAC Transmit Descriptor Active Register (MCIMX28RM.pdf--> 26.4.4)
*********************************************************************************************************/
#define ENET_TDAR_TX_ACTIVE (0x01000000)
/*********************************************************************************************************
  Buffer descriptor control/status  (Receive Buffer Descriptor)
*********************************************************************************************************/
#define ENET_BD_RX_EMPTY    ((UINT16)0x8000)                              /*  Recieve is empty          */
#define ENET_BD_RX_RO1      ((UINT16)0x4000)                              /*  Receive software ownership*/
#define ENET_BD_RX_WRAP     ((UINT16)0x2000)                              /*  Wrap. Written by user     */
#define ENET_BD_RX_INTR     ((UINT16)0x1000)                              /*  Receive software ownership*/
#define ENET_BD_RX_RO2       ENET_BD_RX_INTR                              /*  Receive software ownership*/
#define ENET_BD_RX_LAST     ((UINT16)0x0800)                              /*  Last in frame             */
#define ENET_BD_RX_MISS     ((UINT16)0x0100)                              /*  Miss. Written by the uDMA */
#define ENET_BD_RX_BC       ((UINT16)0x0080)                              /*  Set if the DA is broadcast*/
#define ENET_BD_RX_MC       ((UINT16)0x0040)                              /*  Set if the DA is multicast*/
                                                                          /*  and not BC                */
#define ENET_BD_RX_LG       ((UINT16)0x0020)                              /*  Rx frame length violation */
#define ENET_BD_RX_NO       ((UINT16)0x0010)                              /*  Receive non-octet aligned */
                                                                          /*  frame                     */
#define ENET_BD_RX_CR       ((UINT16)0x0004)                              /*　Rx CRC or Rx Frame error　*/
#define ENET_BD_RX_OV       ((UINT16)0x0002)                              /*  Overrun                   */
#define ENET_BD_RX_TR       ((UINT16)0x0001)                              /*  Will be set if the receive*/
                                                                          /*  frame is truncated        */
#define ENET_BD_RX_STATS    ((UINT16)0x013f)
/*********************************************************************************************************
  Buffer descriptor control/status  (Receive Buffer Descriptor)
*********************************************************************************************************/
#define ENET_BD_TX_READY    ((UINT16)0x8000)                              /*  Transmit is ready         */
#define ENET_BD_TX_TO1      ((UINT16)0x4000)                              /*  Transmit software ownership*/
#define ENET_BD_TX_WRAP     ((UINT16)0x2000)                              /*  Wrap. Written by user     */
#define ENET_BD_TX_TO2      ((UINT16)0x1000)                              /*  Transmit software ownership*/
#define ENET_BD_TX_LAST     ((UINT16)0x0800)                              /*  Last in frame             */
#define ENET_BD_TX_TC       ((UINT16)0x0400)                              /*  Tx CRC                    */
#define ENET_BD_TX_ABC      ((UINT16)0x0200)                              /*  Append bad CRC            */
#define ENET_BD_TX_STATS    ((UINT16)0x03ff)
/*********************************************************************************************************
  Bit definitions and macros for FEC_RCR
*********************************************************************************************************/
#define FEC_RCR_GRS             (0x80000000)
#define FEC_RCR_NO_LGTH_CHECK   (0x40000000)
#define FEC_RCR_MAX_FL(x)       (((x)&0x7FF)<<16)
#define FEC_RCR_CNTL_FRM_ENA    (0x00008000)
#define FEC_RCR_CRC_FWD         (0x00004000)
#define FEC_RCR_PAUSE_FWD       (0x00002000)
#define FEC_RCR_PAD_EN          (0x00001000)
#define FEC_RCR_RMII_ECHO       (0x00000800)
#define FEC_RCR_RMII_LOOP       (0x00000400)
#define FEC_RCR_RMII_10T        (0x00000200)
#define FEC_RCR_RMII_MODE       (0x00000100)
#define FEC_RCR_SGMII_ENA       (0x00000080)
#define FEC_RCR_RGMII_ENA       (0x00000040)
#define FEC_RCR_FCE             (0x00000020)
#define FEC_RCR_BC_REJ          (0x00000010)
#define FEC_RCR_PROM            (0x00000008)
#define FEC_RCR_MII_MODE        (0x00000004)
#define FEC_RCR_DRT             (0x00000002)
#define FEC_RCR_LOOP            (0x00000001)
/*********************************************************************************************************
  Bit definitions and macros for FEC_TCR
*********************************************************************************************************/
#define FEC_TCR_RFC_PAUSE       (0x00000010)
#define FEC_TCR_TFC_PAUSE       (0x00000008)
#define FEC_TCR_FDEN            (0x00000004)
#define FEC_TCR_HBC             (0x00000002)
#define FEC_TCR_GTS             (0x00000001)
/*********************************************************************************************************
  the defins of MII operation
*********************************************************************************************************/
#define FEC_MII_ST      0x40000000
#define FEC_MII_OP_OFF  28
#define FEC_MII_OP_MASK 0x03
#define FEC_MII_OP_RD   0x02
#define FEC_MII_OP_WR   0x01
#define FEC_MII_PA_OFF  23
#define FEC_MII_PA_MASK 0xFF
#define FEC_MII_RA_OFF  18
#define FEC_MII_RA_MASK 0xFF
#define FEC_MII_TA      0x00020000
#define FEC_MII_DATA_OFF 0
#define FEC_MII_DATA_MASK 0x0000FFFF

#define FEC_MII_FRAME   (FEC_MII_ST | FEC_MII_TA)
#define FEC_MII_OP(x)   (((x) & FEC_MII_OP_MASK) << FEC_MII_OP_OFF)
#define FEC_MII_PA(pa)  (((pa) & FEC_MII_PA_MASK) << FEC_MII_PA_OFF)
#define FEC_MII_RA(ra)  (((ra) & FEC_MII_RA_MASK) << FEC_MII_RA_OFF)
#define FEC_MII_SET_DATA(v) (((v) & FEC_MII_DATA_MASK) << FEC_MII_DATA_OFF)
#define FEC_MII_GET_DATA(v) (((v) >> FEC_MII_DATA_OFF) & FEC_MII_DATA_MASK)
#define FEC_MII_READ(pa, ra) ((FEC_MII_FRAME | FEC_MII_OP(FEC_MII_OP_RD)) |\
                    FEC_MII_PA(pa) | FEC_MII_RA(ra))
#define FEC_MII_WRITE(pa, ra, v) (FEC_MII_FRAME | FEC_MII_OP(FEC_MII_OP_WR)|\
            FEC_MII_PA(pa) | FEC_MII_RA(ra) | FEC_MII_SET_DATA(v))
/*********************************************************************************************************
  驱动基本配置宏定义
*********************************************************************************************************/
#define ENET_NAME           "PHY"
#define ENET_RING_SZ        (16)
#define ENET_TX_RING_SZ     (16)                                         /*  2 的倍数                   */
#define ENET_RX_RING_SZ     (16)                                         /*  2 的倍数                   */
#define ENET_RING_MASK      (15)
#define ENET_BUF_SZ         (LW_CFG_VMM_PAGE_SIZE >> 1)                  /*  半页                       */
#define ENET_BUFD_LENGTH    (1520)
#define ENET_PKT_SZ         (1518)
#define FEC_ENET_PHYID      (0x5c002000)
#define ENET_PHYADDR        (5)
#define ENET_SYNCQ_SZ       (512)
#define ENET_BASE(penet)     penet->ENET_atIobase
/*********************************************************************************************************
  网络相关宏定义
*********************************************************************************************************/
#define NETIF_PRIV(pnetif)  (ENET *)pnetif->state
#define NETIF_HWADDR(pnetif) pnetif->hwaddr

#define IFNAME0             'e'
#define IFNAME1             'n'

#if LWIP_NETIF_HOSTNAME
#define HOSTNAME            "lwIP"
#endif

#define LINK_SNMP_STAT      1
/*********************************************************************************************************
  结构定义
*********************************************************************************************************/
/*********************************************************************************************************
  MII 驱动定义
*********************************************************************************************************/
struct enet;
typedef struct mii_driver {
    PHY_DEV          MIID_phydev;
    struct enet     *MIID_enet;
} MII_DRV;
/*********************************************************************************************************
  ENET 缓冲区描述符结构
*********************************************************************************************************/
typedef struct bufferDesc {
    UINT16              BUFD_usDataLen;                                 /*  缓冲描述符中数据长度        */
    UINT16              BUFD_usStatus;                                  /*  缓冲描述符状态              */
    ULONG               BUFD_uiBufAddr;                                 /*  缓冲区地址                  */
} BUFD;
/*********************************************************************************************************
  ENET 私有结构
*********************************************************************************************************/
typedef struct enet {
    addr_t              ENET_atIobase;                                  /*  ENET 基地址　　　　　　　　 */
    UINT32              ENET_uiPhyspeed;                                /*  PHY 速度                    */
    INT                 ENET_iDuplex;                                   /*  是否是全双工模式            */
    INT                 ENET_iOpened;                                   /*  MII 是否已经初始化了        */
    INT                 ENET_iLink;                                     /*  网络是否连接                */
#define                 MIID_NO_LINK (0)
#define                 MIID_LINK    (1)
    INT                 ENET_iLinkSpeed;                                /*  网络连接速度                */
    INT                 ENET_iRestart;                                  /*  网络重新开始                */
    INT                 ENET_iStop;                                     /*  网络停止                    */
    INT                 ENET_iFull;                                     /*  发送队列满                  */

    BUFD               *ENET_pbufdRxbdBase;                             /*  接收缓冲描述符指针          */
    BUFD               *ENET_pbufdTxbdBase;                             /*  发送缓冲描述符指针          */
    BUFD               *ENET_pbufdCurRxbd;                              /*  当前接收描述符              */
    BUFD               *ENET_pbufdCurTxbd;                              /*  当前发送描述符              */
    BUFD               *ENET_pbufdTxDirty;                              /*  已经发送完成的描述符        */
    PUCHAR              ENET_pcTxBuf;                                   /*  发送缓冲区地址              */
    PUCHAR              ENET_pcRxBuf;                                   /*  接收缓冲区地址              */

    LW_HANDLE           ENET_hNetEvtQ;                                  /*  网络事件队列ID              */

    INT                 ENET_iTs;                                       /*  连接时间戳                  */
    MII_DRV            *ENET_miidrv;                                    /*  mii 驱动数据                */
} ENET;
/*********************************************************************************************************
  ENET 中断事件类型，详见 <int event type (MCIMX28RM.pdf--> 26.4.1)>
*********************************************************************************************************/
typedef struct int_event {
    INT        IE_iEventType;                                           /* 中断事件类型                 */
} IEVENT;
/*********************************************************************************************************
  全局变量定义
*********************************************************************************************************/
static  LW_JOB_QUEUE      _G_enetTxNetJobQ;
static  LW_JOB_MSG        _G_enetTxNetJobMsgs[LW_CFG_LWIP_JOBQUEUE_SIZE];
static  LW_OBJECT_HANDLE  _G_hMiiLock  = LW_OBJECT_HANDLE_INVALID;
static  addr_t            _G_atAddrV   = (addr_t)LW_NULL;
static  ENET              _G_enetInfo  = {
        .ENET_atIobase  = ENET_PHYS_ADDR,
        .ENET_iDuplex   = 0,
        .ENET_iOpened   = 0,
        .ENET_iLink     = MIID_NO_LINK,
        .ENET_iRestart  = 0,
        .ENET_iStop     = 0,
        .ENET_iFull     = 0,
};
/*********************************************************************************************************
  函数声明
*********************************************************************************************************/
static VOID enetBuffReset(struct netif *pnetif);
/*********************************************************************************************************
** 函数名称: miiWait
** 功能描述: 等队列
** 输    入: hq    队列 Handler ID
** 输    出: NONE
** 返    回: ERROR CODE
*********************************************************************************************************/
static INT  miiWait (LW_OBJECT_HANDLE  hq)
{
    ULONG       ulRecv;
    size_t      stLen  = 0;

    API_MsgQueueReceive(hq,
                        (PVOID)&ulRecv,
                        sizeof(ULONG),
                        &stLen,
                        LW_OPTION_WAIT_INFINITE);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: miiWakeup
** 功能描述: 唤醒队列
** 输    入: hq    队列 Handler ID
** 输    出: NONE
** 返    回: ERROR CODE
*********************************************************************************************************/
static INT  miiWakeup (LW_OBJECT_HANDLE  hq)
{
    ULONG  ulMsg = 1;

    API_MsgQueueSend(hq, (PVOID)&ulMsg, sizeof(ulMsg));

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: miiPhyRead
** 功能描述: PHY状态监控
** 输　入  : ucAddr :       PHY 设备地址(0 - 31)
**           ucReg  :       PHY 寄存器
**           usVal  :       值
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  miiPhyRead (UCHAR  ucAddr, UCHAR  ucReg, UINT16  *pusVal)
{
    writel(FEC_MII_READ(ucAddr, ucReg), _G_atAddrV + HW_ENET_MAC_MMFR);

    miiWait(_G_hMiiLock);

    *pusVal = FEC_MII_GET_DATA(readl(_G_atAddrV + HW_ENET_MAC_MMFR));

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: miiPhyWrite
** 功能描述: PHY 写
** 输　入  : ucAddr :       PHY 设备地址(0 - 31)
**           ucReg  :       PHY 寄存器
**           usVal  :       值
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  miiPhyWrite (UCHAR  ucAddr, UCHAR  ucReg, UINT16  usVal)
{
    writel(FEC_MII_WRITE(ucAddr, ucReg, usVal), _G_atAddrV + HW_ENET_MAC_MMFR);

    miiWait(_G_hMiiLock);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: miiPrintStatus
** 功能描述: 打印连接状态信息
** 输　入  : pmiidrv   :  MII 驱动
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID  miiPrintStatus (MII_DRV  *pmiidrv)
{
    printk("%s - Link is %s", ENET_NAME,
           pmiidrv->MIID_enet->ENET_iLink ? "UP" : "DOWN");

    if (pmiidrv->MIID_enet->ENET_iLink == MIID_LINK) {
        printk(" - %dM(%s)",
               pmiidrv->MIID_phydev.PHY_uiPhySpeed / (1000 * 1000),
               pmiidrv->MIID_phydev.PHY_pcPhyMode);
    }

    printk("\n");
}
/*********************************************************************************************************
** 函数名称: miiLinkStatus
** 功能描述: MII 连接状态变化
** 输　入  : pmiidrv   :  MII 驱动
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  miiLinkStatus (MII_DRV  *pmiidrv)
{
    IEVENT    ie;
    INT       iStatus = 0;

    if (!pmiidrv) {
        return  (PX_ERROR);
    }

    if (pmiidrv->MIID_phydev.PHY_usPhyStatus & MII_SR_LINK_STATUS) {
        pmiidrv->MIID_enet->ENET_iLinkSpeed =
                pmiidrv->MIID_phydev.PHY_uiPhySpeed;
        if (pmiidrv->MIID_enet->ENET_iLink == MIID_NO_LINK) {
            iStatus = 1;
            pmiidrv->MIID_enet->ENET_iRestart = 1;
            pmiidrv->MIID_enet->ENET_iLink = MIID_LINK;
            if (!lib_strncmp(MII_FDX_STR,
                             pmiidrv->MIID_phydev.PHY_pcPhyMode,
                             MII_FDX_LEN)) {
                pmiidrv->MIID_enet->ENET_iDuplex = 1;
            } else {
                pmiidrv->MIID_enet->ENET_iDuplex = 0;
            }
        } else {
            if (!lib_strncmp(MII_FDX_STR,
                             pmiidrv->MIID_phydev.PHY_pcPhyMode,
                             MII_FDX_LEN)) {
                if (pmiidrv->MIID_enet->ENET_iDuplex) {
                    pmiidrv->MIID_enet->ENET_iRestart = 0;
                } else {
                    iStatus = 1;
                    pmiidrv->MIID_enet->ENET_iDuplex  = 1;
                    pmiidrv->MIID_enet->ENET_iRestart = 1;
                }
            } else {
                if (pmiidrv->MIID_enet->ENET_iDuplex) {
                    pmiidrv->MIID_enet->ENET_iRestart = 1;
                    pmiidrv->MIID_enet->ENET_iDuplex  = 0;
                    iStatus = 1;
                } else {
                    pmiidrv->MIID_enet->ENET_iRestart = 0;
                }
            }
        }
        pmiidrv->MIID_enet->ENET_iStop = 0;

    } else {
        pmiidrv->MIID_enet->ENET_iLinkSpeed = 0;
        if (pmiidrv->MIID_enet->ENET_iLink == MIID_LINK) {
            iStatus = 1;
            pmiidrv->MIID_enet->ENET_iStop = 1;
            pmiidrv->MIID_enet->ENET_iLink = MIID_NO_LINK;
        } else {
            pmiidrv->MIID_enet->ENET_iStop = 0;
        }
        pmiidrv->MIID_enet->ENET_iRestart = 0;
    }

    ie.IE_iEventType    = ENET_EVENT_LINK;

    if (iStatus) {
        miiPrintStatus(pmiidrv);
        API_MsgQueueSend(pmiidrv->MIID_enet->ENET_hNetEvtQ,
                         (PVOID)&ie, sizeof(IEVENT));
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: miiDrvInit
** 功能描述: MII 驱动初始化
** 输　入  : NONE
** 输　出  : MII 驱动
** 全局变量:
** 调用模块:
*********************************************************************************************************/
MII_DRV  *miiDrvInit (VOID)
{
    MII_DRV   *pmiidrv;

    pmiidrv = __SHEAP_ALLOC(sizeof(MII_DRV));
    if (!pmiidrv) {
        _ErrorHandle(ENOMEM);
        return  (LW_NULL);
    }

    pmiidrv->MIID_phydev.PHY_pPhyDrvFunc = __SHEAP_ALLOC(sizeof(PHY_DRV_FUNC));
    if (!pmiidrv->MIID_phydev.PHY_pPhyDrvFunc) {
        __SHEAP_FREE(pmiidrv);
        _ErrorHandle(ENOMEM);
        return  (LW_NULL);
    }

    _G_atAddrV = (addr_t)ENET_PHYS_ADDR;

    _G_hMiiLock = API_MsgQueueCreate("mii_wait",
                                     ENET_SYNCQ_SZ,
                                     sizeof(ULONG),
                                     LW_OPTION_WAIT_FIFO |
                                     LW_OPTION_OBJECT_GLOBAL,
                                     LW_NULL);
    if (_G_hMiiLock == LW_OBJECT_HANDLE_INVALID) {
        return  (LW_NULL);
    }

    pmiidrv->MIID_phydev.PHY_pPhyDrvFunc->PHYF_pfuncWrite = (FUNCPTR)miiPhyWrite;
    pmiidrv->MIID_phydev.PHY_pPhyDrvFunc->PHYF_pfuncRead = (FUNCPTR)miiPhyRead;
    pmiidrv->MIID_phydev.PHY_pPhyDrvFunc->PHYF_pfuncLinkDown = (FUNCPTR)miiLinkStatus;
    pmiidrv->MIID_phydev.PHY_pvMacDrv = pmiidrv;
    pmiidrv->MIID_phydev.PHY_ucPhyAddr = ENET_PHYADDR;
    pmiidrv->MIID_phydev.PHY_uiPhyID = FEC_ENET_PHYID;
    pmiidrv->MIID_phydev.PHY_uiTryMax = 100;
    pmiidrv->MIID_phydev.PHY_uiLinkDelay = 100;                           /*  延时100毫秒自动协商过程   */
    pmiidrv->MIID_phydev.PHY_uiPhyFlags = MII_PHY_AUTO |                  /*  自动协商标志              */
                                          MII_PHY_FD   |                  /*  全双工模式                */
                                          MII_PHY_100  |                  /*  100Mbit                   */
                                          MII_PHY_10   |                  /*  10Mbit                    */
                                          MII_PHY_HD   |                  /*  半双工模式                */
                                          MII_PHY_MONITOR;                /*  启用自动监视功能          */

    return  (pmiidrv);
}
/*********************************************************************************************************
** 函数名称: enetTxNetJobAdd
** 功能描述: 加入网络异步处理作业队列
** 输　入  : pfunc                      函数指针
**           pvArg0                     函数参数
**           pvArg1                     函数参数
**           pvArg2                     函数参数
**           pvArg3                     函数参数
**           pvArg4                     函数参数
**           pvArg5                     函数参数
** 输　出  : 操作是否成功
** 全局变量:
** 调用模块:
                                           API 函数
*********************************************************************************************************/
static INT  enetTxNetJobAdd (VOIDFUNCPTR  pfunc,
                             PVOID        pvArg0,
                             PVOID        pvArg1,
                             PVOID        pvArg2,
                             PVOID        pvArg3,
                             PVOID        pvArg4,
                             PVOID        pvArg5)
{
    if (!pfunc) {
        _ErrorHandle(EINVAL);
        return  (PX_ERROR);
    }

    if (_jobQueueAdd(&_G_enetTxNetJobQ, pfunc, pvArg0, pvArg1, pvArg2, pvArg3, pvArg4, pvArg5)) {
        _ErrorHandle(ERROR_EXCE_LOST);
        return  (PX_ERROR);
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: enetTxNetJobThread
** 功能描述: 网络工作队列处理线程
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  enetTxNetJobThread (VOID)
{
    for (;;) {
        _jobQueueExec(&_G_enetTxNetJobQ, LW_OPTION_WAIT_INFINITE);
    }
}
/*********************************************************************************************************
** 函数名称: enetTxNetJobqueueInit
** 功能描述: 初始化 Net jobqueue 处理 机制
** 输　入  : NONE
** 输　出  : 是否初始化成功
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  enetTxNetJobqueueInit (VOID)
{
    LW_OBJECT_HANDLE    hNetJobThread;
    LW_CLASS_THREADATTR threadattr;

    if (_jobQueueInit(&_G_enetTxNetJobQ,
                      &_G_enetTxNetJobMsgs[0], LW_CFG_LWIP_JOBQUEUE_SIZE, LW_FALSE)) {
        return  (PX_ERROR);
    }

    API_ThreadAttrBuild(&threadattr, LW_CFG_LWIP_STK_SIZE,
                        LW_PRIO_T_NETJOB,
                        (LW_OPTION_THREAD_STK_CHK | LW_OPTION_THREAD_SAFE | LW_OPTION_OBJECT_GLOBAL),
                        LW_NULL);

    hNetJobThread = API_ThreadCreate("t_txnetjob",
                                     (PTHREAD_START_ROUTINE)enetTxNetJobThread,
                                     (PLW_CLASS_THREADATTR)&threadattr,
                                     LW_NULL);                          /*  建立 job 处理线程           */
    if (!hNetJobThread) {
        _jobQueueFinit(&_G_enetTxNetJobQ);
        return  (PX_ERROR);
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: enetSetHwaddr
** 功能描述: 设置硬件地址
** 输　入  : pnetif  :  网络结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  enetSetHwaddr (struct netif  *pnetif)
{
    ENET     *penet;
    addr_t    atBase;
    UINT8    *ucHwaddr;

    penet    = NETIF_PRIV(pnetif);
    atBase   = ENET_BASE(penet);
    ucHwaddr = NETIF_HWADDR(pnetif);

    /*
     *  设置硬件地址
     */
    writel((ucHwaddr[0] << 24) +                                          /*  Bytes 0 (bits 31:24)      */
           (ucHwaddr[1] << 16) +                                          /*  Bytes 1 (bits 23:16)      */
           (ucHwaddr[2] << 8)  +                                          /*  Bytes 2 (bits 15:8)       */
           (ucHwaddr[3]),                                                 /*  Bytes 3 (bits 7:0)        */
           atBase + HW_ENET_MAC_PALR);
    writel((ucHwaddr[4] << 24) +                                          /*  Bytes 4 (bits 31:24)      */
           (ucHwaddr[5] << 16) +                                          /*  Bytes 5 (bits 23:16)      */
           ENET_PAUR_PAUSE_F_T,
           atBase + HW_ENET_MAC_PAUR);
}
/*********************************************************************************************************
** 函数名称: enetSwapPkt
** 功能描述: 字节序变换
** 输　入  : pvPkt  :  数据包指针
**           iLen   :  数据长度
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static  inline VOID  enetSwapPkt (PVOID  pvPkt, INT  iLen)
{
    INT     i;
    UINT32 *uiBuf = (UINT32 *)pvPkt;

    for (i = 0; i < (iLen + 3) / 4; i++, uiBuf++) {
        *uiBuf = bswap32(*uiBuf);
    }
}
/*********************************************************************************************************
** 函数名称: enetCoreRecv
** 功能描述: 网络接收函数
** 输　入  : pnetif  :  网络结构
** 输　出  : 接收的长度
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  enetCoreRecv (struct netif  *pnetif)
{
    ENET         *penet;
    struct pbuf  *pbuf;
    UINT8        *ucFrame;
    BUFD         *pbufd;
    addr_t        atBase;
    UINT16        usStatus;
    UINT16        usLen = 0;

    penet  = NETIF_PRIV(pnetif);
    atBase = ENET_BASE(penet);

    pbufd = penet->ENET_pbufdCurRxbd;
    while (!((usStatus = pbufd->BUFD_usStatus) & ENET_BD_RX_EMPTY)) {
        if (!penet->ENET_iOpened) {
            goto rx_done;
        }

        if (usStatus & ENET_BD_RX_LG) {
#if LINK_STATS
            LINK_STATS_INC(link.lenerr);
#endif
        }

        if (usStatus & ENET_BD_RX_CR) {
#if LINK_STATS
            LINK_STATS_INC(link.chkerr);
#endif
        }

        if (usStatus & ENET_BD_RX_OV) {
#if LINK_STATS
            LINK_STATS_INC(link.memerr);
#endif
        }

        if (usStatus & ENET_BD_RX_TR) {
#if LINK_STATS
            LINK_STATS_INC(link.err);
            LINK_STATS_INC(link.memerr);
#endif
            goto rx_done;
        }

        usLen = pbufd->BUFD_usDataLen;
        /*
         *  获取接收的帧
         */
        ucFrame = (UINT8 *)pbufd->BUFD_uiBufAddr;
        /*
         *  在MX28系列做字节序转换
         */
        enetSwapPkt((PVOID)ucFrame, usLen);

        /*
         *  除去 FCS
         */
        usLen -= 4;

#if ETH_PAD_SIZE
        usLen += ETH_PAD_SIZE;
#endif
        pbuf = pbuf_alloc(PBUF_RAW, (UINT16)usLen, PBUF_POOL);
        if (!pbuf) {
#if LINK_STATS
            LINK_STATS_INC(link.memerr);
            LINK_STATS_INC(link.drop);
#endif
#ifdef LINK_SNMP_STAT
            snmp_inc_ifindiscards(pnetif);
#endif
        } else {
#if ETH_PAD_SIZE
            pbuf_header(pbuf, -ETH_PAD_SIZE);
#endif
            pbuf_take(pbuf, ucFrame, (UINT16)usLen - ETH_PAD_SIZE);
#if ETH_PAD_SIZE
            pbuf_header(pbuf, ETH_PAD_SIZE);
#endif
#if LINK_STATS
            LINK_STATS_INC(link.recv);
#endif
#ifdef LINK_SNMP_STAT
            snmp_add_ifinoctets(pnetif, usLen);
            snmp_inc_ifinucastpkts(pnetif);
#endif
            pnetif->input(pbuf, pnetif);
        }
rx_done:
        usStatus &= ~ENET_BD_RX_STATS;
        usStatus |= ENET_BD_RX_EMPTY;
        pbufd->BUFD_usStatus = usStatus;

        if (usStatus & ENET_BD_RX_WRAP) {
            pbufd = penet->ENET_pbufdRxbdBase;
        } else {
            pbufd++;
        }

        writel(ENET_RDAR_RX_ACTIVE, atBase + HW_ENET_MAC_RDAR);
    }

    penet->ENET_pbufdCurRxbd = pbufd;

    return  (usLen + 4);
}
/*********************************************************************************************************
** 函数名称: enetCoreTxStart
** 功能描述: 网络发送函数
** 输　入  : pnetif  :  网络结构
**           pbuf    :  lwip 核心 buff
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static err_t enetCoreTx (struct netif *pnetif, struct pbuf *pbuf)
{
    ENET     *penet;
    addr_t    atBase;
    UINT16    usStatus;
    UINT16    usLen;
    BUFD     *pbufd;

    penet = NETIF_PRIV(pnetif);
    atBase = ENET_BASE(penet);

    pbufd = penet->ENET_pbufdCurTxbd;
    usStatus = pbufd->BUFD_usStatus;
    if (usStatus & ENET_BD_TX_READY) {
        printf("Send queue full!!\n");
        return  (ERR_VAL);
    }

    usStatus &= ~ENET_BD_TX_STATS;

    usLen = pbuf->tot_len;

#if ETH_PAD_SIZE
    usLen -= ETH_PAD_SIZE;
#endif

#if ETH_PAD_SIZE
    pbuf_header(pbuf, -ETH_PAD_SIZE);
#endif

    usStatus |= ENET_BD_TX_TC | ENET_BD_TX_LAST | ENET_BD_TX_READY | ENET_BD_TX_TO2;

    pbuf_copy_partial(pbuf,
                      (PVOID)pbufd->BUFD_uiBufAddr,
                      usLen, 0);
    enetSwapPkt((PVOID)pbufd->BUFD_uiBufAddr, usLen);
    pbufd->BUFD_usDataLen = usLen;
    pbufd->BUFD_usStatus  = usStatus;

    writel(ENET_TDAR_TX_ACTIVE, atBase + HW_ENET_MAC_TDAR);

    /*
     *  如果这是最后一个发送描述符，返回最开始
     */
    if (usStatus & ENET_BD_TX_WRAP) {
        pbufd = penet->ENET_pbufdTxbdBase;
    } else {
        pbufd++;
    }

    if (pbufd == penet->ENET_pbufdTxDirty) {
        penet->ENET_iFull = 1;
    }

    penet->ENET_pbufdCurTxbd = pbufd;

#if ETH_PAD_SIZE
    pbuf_header(pbuf, ETH_PAD_SIZE);
#endif

#ifdef LINK_SNMP_STAT
    snmp_add_ifoutoctets(pnetif, usLen);
    snmp_inc_ifoutucastpkts(pnetif);
#endif

    return  (ERR_OK);
}
/*********************************************************************************************************
** 函数名称: enetCoreTxStart
** 功能描述: 网络发送函数
** 输　入  : pnetif  :  网络结构
**           pbuf    :  lwip 核心 buff
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static err_t enetCoreTxStart (struct netif *pnetif, struct pbuf *pbuf)
{
    ENET     *penet;

    penet = NETIF_PRIV(pnetif);

    if (!penet->ENET_iLink) {
        return  (ERR_CONN);
    }

    enetTxNetJobAdd((VOIDFUNCPTR)enetCoreTx, pnetif, pbuf, 0, 0, 0, 0);

    return  (ERR_OK);
}
/*********************************************************************************************************
** 函数名称: enetCoreSendComplete
** 功能描述: ENET 发送完成函数
** 输　入  : pnetif  :  网络结构
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  enetCoreSendComplete (struct netif *pnetif)
{
    ENET     *penet;
    UINT16    usStatus;
    BUFD     *pbufd;

    penet = NETIF_PRIV(pnetif);

    pbufd = penet->ENET_pbufdTxDirty;
    while (((usStatus = pbufd->BUFD_usStatus) & ENET_BD_TX_READY) == 0) {
        if (pbufd == penet->ENET_pbufdCurTxbd && penet->ENET_iFull == 0) {
            break;
        }

        if (usStatus & ENET_BD_TX_ABC) {
#if LINK_STATS
            LINK_STATS_INC(link.chkerr);
#endif
        }
#if LINK_STATS
        LINK_STATS_INC(link.xmit);
#endif
        if (usStatus & ENET_BD_TX_WRAP) {
            pbufd = penet->ENET_pbufdTxbdBase;
        } else {
            pbufd++;
        }

        if (penet->ENET_iFull) {
            penet->ENET_iFull = 0;
        }
    }

    penet->ENET_pbufdTxDirty = pbufd;

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: enetChipReset
** 功能描述: ENET 复位
** 输　入  : pnetif  :  网络结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static  VOID  enetChipReset (struct netif  *pnetif)
{
    ENET     *penet;
    addr_t    atBase;

    penet = NETIF_PRIV(pnetif);
    atBase = ENET_BASE(penet);

    writel(ENET_ECR_RESET, atBase + HW_ENET_MAC_ECR);
    API_TimeMSleep(1);
}
/*********************************************************************************************************
** 函数名称: enetCoreStart
** 功能描述: ENET 核心开始
** 输　入  : pnetif  :  网络结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  enetCoreStart (struct netif *pnetif, INT  iDuplex)
{
    ENET     *penet;
    addr_t    atBase;

    penet = NETIF_PRIV(pnetif);
    atBase = ENET_BASE(penet);

    enetChipReset(pnetif);

    enetSetHwaddr(pnetif);

    /*
     *  清除中断事件位
     */
    writel(0xFFFFFFFF, atBase + HW_ENET_MAC_EIR);

    /*
     *  清除所有的单播和多播
     */
    writel(0, atBase + HW_ENET_MAC_IAUR);
    writel(0, atBase + HW_ENET_MAC_IALR);
    writel(0, atBase + HW_ENET_MAC_GAUR);
    writel(0, atBase + HW_ENET_MAC_GALR);

    /*
     *  设置最大缓冲区
     */
    writel(ENET_BUFD_LENGTH, atBase + HW_ENET_MAC_EMRBR);

    /*
     *  设置发送接收描述符基地址寄存器
     */
    writel((addr_t)penet->ENET_pbufdRxbdBase, atBase + HW_ENET_MAC_ERDSR);
    writel((addr_t)penet->ENET_pbufdTxbdBase, atBase + HW_ENET_MAC_ETDSR);

    enetBuffReset(pnetif);
    penet->ENET_pbufdCurRxbd = penet->ENET_pbufdRxbdBase;
    penet->ENET_pbufdCurTxbd = penet->ENET_pbufdTxbdBase;
    penet->ENET_pbufdTxDirty = penet->ENET_pbufdCurTxbd;

    if (iDuplex) {
        /*
         *  全双工模式
         */
        writel(FEC_RCR_MAX_FL(ENET_PKT_SZ) | 0x04, atBase + HW_ENET_MAC_RCR);
        writel(FEC_TCR_FDEN, atBase + HW_ENET_MAC_TCR);
    } else {
        writel(FEC_RCR_MAX_FL(ENET_PKT_SZ) | 0x06, atBase + HW_ENET_MAC_RCR);
        writel(0, atBase + HW_ENET_MAC_TCR);
    }

    /*
     *  使能流控和长度检查
     */
    writel(FEC_RCR_FCE | FEC_RCR_NO_LGTH_CHECK, atBase + HW_ENET_MAC_RCR);

    /*
     *  使能RMII
     */
    writel(FEC_RCR_RMII_MODE, atBase + HW_ENET_MAC_RCR);
    /*
     *  设置MII速度
     */
    writel((readl(atBase + HW_ENET_MAC_MSCR) & (~0x7E)) |
            penet->ENET_uiPhyspeed, atBase + HW_ENET_MAC_MSCR);

    /*
     *  现在使能 ENET 和接收帧使能
     */
    writel(ENET_ECR_ETHER_EN, atBase + HW_ENET_MAC_ECR);
    writel(ENET_RDAR_RX_ACTIVE, atBase + HW_ENET_MAC_RDAR);

    /*
     *  使能我们关心的中断事件
     */
    writel(ENET_DEFAULT_INTE, atBase + HW_ENET_MAC_EIMR);
}
/*********************************************************************************************************
** 函数名称: enetCoreStop
** 功能描述: 释放数据缓冲区
** 输　入  : pnetif  :  网络结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  enetCoreStop (struct netif *pnetif)
{
    ENET       *penet;
    addr_t      atBase;

    penet  = NETIF_PRIV(pnetif);
    atBase = ENET_BASE(penet);

    if (penet->ENET_iLink) {
        writel(1, atBase + HW_ENET_MAC_TCR);
        udelay(10);
        if (!(readl(atBase + HW_ENET_MAC_EIR) & ENET_EIR_GRA)) {
            printf("Graceful failed.\n");
        }
    }

    /*
     *  复位
     */
    enetChipReset(pnetif);

    /*
     *  设置 RMII
     */
    writel(readl(atBase + HW_ENET_MAC_RCR) |
           FEC_RCR_RMII_MODE, atBase + HW_ENET_MAC_RCR);

    /*
     *  设置MII速度
     */
    writel((readl(atBase + HW_ENET_MAC_MSCR) & (~0x7E)) |
            penet->ENET_uiPhyspeed, atBase + HW_ENET_MAC_MSCR);

    /*
     *  清除MII中断
     */
    writel(ENET_EIR_MII, atBase + HW_ENET_MAC_EIR);

    /*
     *  现在使能 ENET
     */
    writel(ENET_ECR_ETHER_EN, atBase + HW_ENET_MAC_ECR);

    /*
     *  使能我们关心的中断事件
     */
    writel(ENET_DEFAULT_INTE, atBase + HW_ENET_MAC_EIMR);

}
/*********************************************************************************************************
** 函数名称: enetSetPhySpeed
** 功能描述: 设置 PHY 速度
** 输　入  : pnetif  :  网络结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  enetSetPhySpeed (struct netif *pnetif)
{
    ENET       *penet;
    UINT32      uiRate;
    struct clk *clk;

    if (!pnetif) {
        return;
    }

    penet  = NETIF_PRIV(pnetif);

    clk = clk_get(NULL, NULL, "fec_clk");
    if (!clk) {
        return;
    }

    clk_enable(clk);

    uiRate = clk_get_rate(clk);

    penet->ENET_uiPhyspeed = ((uiRate + (5000000 - 1)) / 5000000) << 1;
}
/*********************************************************************************************************
** 函数名称: enetCoreOpen
** 功能描述: 网络打开
** 输　入  : pnetif  :  网络结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  enetCoreOpen (struct netif  *pnetif)
{
    ENET       *penet;

    penet  = NETIF_PRIV(pnetif);

    enetCoreStart(pnetif, penet->ENET_iDuplex);
    API_MiiPhyMonitorStart();
    penet->ENET_iOpened = 1;
}
/*********************************************************************************************************
** 函数名称: enetCoreClose
** 功能描述: 网络关闭
** 输　入  : pnetif  :  网络结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  enetCoreClose (struct netif  *pnetif)
{
    ENET       *penet;

    penet  = NETIF_PRIV(pnetif);

    penet->ENET_iOpened = 0;

    API_MiiPhyMonitorStop();
    enetCoreStop(pnetif);
}
/*********************************************************************************************************
** 函数名称: enetCoreInit
** 功能描述: Lwip 网络初始化函数
** 输　入  : pnetif  :  网络结构
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
#if USER_CONFIG_HWADDR < 1

static VOID enetGetHwAddr (UCHAR  *ucMac)
{
    UINT32          uiVal;

    writel(BM_OCOTP_CTRL_RD_BANK_OPEN, REGS_OCOTP_BASE + HW_OCOTP_CTRL_SET);

    /*
     * wait until OTP contents are readable
     */
    while (readl(REGS_OCOTP_BASE +
                 HW_OCOTP_CTRL) &
           BM_OCOTP_CTRL_BUSY) {
        udelay(100);
    }

    ucMac[0] = 0x00;
    ucMac[1] = 0x04;

    uiVal = readl(REGS_OCOTP_BASE + HW_OCOTP_CUSTn(0));

    ucMac[2] = (uiVal >> 24) & 0xFF;
    ucMac[3] = (uiVal >> 16) & 0xFF;
    ucMac[4] = (uiVal >> 8)  & 0xFF;
    ucMac[5] = (uiVal >> 0)  & 0xFF;
}

#endif                                                                    /*  USER_CONFIG_HWADDR < 1    */
/*********************************************************************************************************
** 函数名称: enetThread
** 功能描述: 工作线程
** 输　入  : pvArg  :  线程参数
** 输　出  : NULL
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static PVOID enetThread (PVOID  pvArg)
{
    struct netif  *pnetif = (struct netif *)pvArg;
    ENET          *penet;
    IEVENT         ie;
    size_t         stLen;

    if (!pnetif) {
        return  (LW_NULL);
    }

    penet = NETIF_PRIV(pnetif);
    if (!penet) {
        return  (LW_NULL);
    }

    API_MiiPhyInit(&(penet->ENET_miidrv->MIID_phydev));
    API_MiiPhyMonitorStart();
    if (penet->ENET_miidrv->MIID_phydev.PHY_pPhyDrvFunc->PHYF_pfuncLinkDown) {
        penet->ENET_miidrv->
        MIID_phydev.PHY_pPhyDrvFunc->
        PHYF_pfuncLinkDown(penet->ENET_miidrv);
    }

    for (;;) {
        API_MsgQueueReceive(penet->ENET_hNetEvtQ,
                            (PVOID)&ie,
                            sizeof(IEVENT),
                            &stLen,
                            LW_OPTION_WAIT_INFINITE);

        /*
         *  网线状态改变
         */
        if (ie.IE_iEventType & ENET_EVENT_LINK) {
            if (penet->ENET_iRestart) {
                netif_set_link_up(pnetif);
                enetCoreStart(pnetif, penet->ENET_iDuplex);
                pnetif->link_speed = penet->ENET_iLinkSpeed;
#ifdef LWIP_LINK_STAT
                pnetif->link_type      = snmp_ifType_ethernet_csmacd;
                pnetif->ts             = (UINT)API_TimeGet();
#endif
            }

            if (penet->ENET_iStop) {
                netif_set_link_down(pnetif);
                enetCoreStop(pnetif);
            }
        }
        /*
         *  TODO: 我们可能支持更多的事件
         */
    }

    return  (LW_NULL);
}
/*********************************************************************************************************
** 函数名称: enetIsr
** 功能描述: MII 驱动初始化
** 输　入  : pvArg   :  中断参数
**           uiVector:  中断向量号
** 输　出  : 中断返回值
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static irqreturn_t enetIsr (PVOID  pvArg, UINT32  uiVector)
{
    struct netif  *pnetif = (struct netif *)pvArg;
    ENET          *penet;
    INT            ie = 0;
    addr_t         atBase;

    if (!pnetif) {
        /*
         *  因为这个时候我们不能从我们的结构中获得寄存器地址，所以我们硬性写这个地址
         *  但这不是一个好的方法，不过程序可能不会运行到这里来，这是为了清除事件位。
         */
        writel(ENET_DEFAULT_INTE, ENET_PHYS_ADDR + HW_ENET_MAC_EIR);

        return  (LW_IRQ_HANDLED);
    }

    penet = NETIF_PRIV(pnetif);
    atBase = ENET_BASE(penet);

    /*
     *  写 1 清除事件位
     */
    ie = readl(atBase + HW_ENET_MAC_EIR);
    writel(ie, atBase + HW_ENET_MAC_EIR);

    /*
     *  已经完成了接收
     */
    if (ie & ENET_EIR_RXF) {
        API_NetJobAdd((VOIDFUNCPTR)enetCoreRecv, pnetif, 0, 0, 0, 0, 0);
    }

    /*
     *  已经完成了发送
     */
    if (ie & ENET_EIR_TXF) {
        enetCoreSendComplete(pnetif);
    }

    /*
     *  MII 读写事件
     */
    if (ie & ENET_EIR_MII) {
        miiWakeup(_G_hMiiLock);
    }

    return  (LW_IRQ_HANDLED);
}
/*********************************************************************************************************
** 函数名称: enetIntInit
** 功能描述: ENET 中断初始化
** 输　入  : pnetif  :  网络结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  enetIntInit (struct netif  *pnetif)
{
    API_InterVectorConnect(INUM_ENET_MAC0_IRQ,
                           (PINT_SVR_ROUTINE)enetIsr,
                           (PVOID)pnetif,
                           "enet_isr");

    API_InterVectorEnable(INUM_ENET_MAC0_IRQ);
}
/*********************************************************************************************************
** 函数名称: enetThreadCreate
** 功能描述: 增加 ENET 工作队列
** 输　入  : pnetif  :  网络结构
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  enetThreadCreate (struct netif  *pnetif)
{
    LW_HANDLE            handle;
    LW_CLASS_THREADATTR  threadattr;
    ENET                *penet;

    if (!pnetif) {
        return  (PX_ERROR);
    }

    penet = NETIF_PRIV(pnetif);

    handle = API_MsgQueueCreate("enet_event",
                                ENET_SYNCQ_SZ,
                                sizeof(IEVENT),
                                LW_OPTION_WAIT_FIFO |
                                LW_OPTION_OBJECT_GLOBAL,
                                &penet->ENET_hNetEvtQ);
    if (handle == LW_OBJECT_HANDLE_INVALID) {
        return  (PX_ERROR);
    }

    threadattr = API_ThreadAttrGetDefault();
    threadattr.THREADATTR_pvArg      = (PVOID)pnetif;
    threadattr.THREADATTR_ucPriority = LW_PRIO_T_NETJOB + 10;
    threadattr.THREADATTR_ulOption  |= LW_OPTION_OBJECT_GLOBAL;

    handle = API_ThreadCreate("t_enet",
                              (PTHREAD_START_ROUTINE)enetThread,
                              (PLW_CLASS_THREADATTR)&threadattr,
                              LW_NULL);
    if (handle == LW_OBJECT_HANDLE_INVALID) {
        return  (PX_ERROR);
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: enetBuffReset
** 功能描述: 描述符复位
** 输　入  : pnetif  :  网络结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID enetBuffReset (struct netif *pnetif)
{
    ENET             *penet;
    volatile BUFD    *pbufd;
    volatile INT      i;

    penet  = NETIF_PRIV(pnetif);

    pbufd = penet->ENET_pbufdRxbdBase;
    for (i = 0; i < ENET_RX_RING_SZ; ++i) {
        pbufd->BUFD_usStatus = ENET_BD_RX_EMPTY;
        pbufd++;
    }
    /*
     *  设置接收描述符边界标志
     */
    pbufd--;
    pbufd->BUFD_usStatus |= ENET_BD_RX_WRAP;

    pbufd = penet->ENET_pbufdTxbdBase;
    for (i = 0; i < ENET_TX_RING_SZ; ++i) {
        pbufd->BUFD_usStatus = 0;
        pbufd++;
    }
    /*
     *  设置发送描述符边界标志
     */
    pbufd--;
    pbufd->BUFD_usStatus |= ENET_BD_TX_WRAP;
}
/*********************************************************************************************************
** 函数名称: enetAllocBuff
** 功能描述: 分配描述符缓冲区
** 输　入  : pnetif  :  网络结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  enetAllocBuff (struct netif  *pnetif)
{
    ENET           *penet;
    volatile BUFD  *pbufd;
    volatile INT    i;

    penet  = NETIF_PRIV(pnetif);

    pbufd = penet->ENET_pbufdRxbdBase;
    for (i = 0; i < ENET_RX_RING_SZ; i += 2) {
        penet->ENET_pcRxBuf   = (PUCHAR)API_VmmDmaAlloc(LW_CFG_VMM_PAGE_SIZE);
        pbufd->BUFD_uiBufAddr = (UINT32)penet->ENET_pcRxBuf;
        pbufd++;

        pbufd->BUFD_uiBufAddr = (UINT32)(penet->ENET_pcRxBuf + ENET_BUF_SZ);
        pbufd++;
    }

    pbufd = penet->ENET_pbufdTxbdBase;
    for (i = 0; i < ENET_TX_RING_SZ; i += 2) {
        penet->ENET_pcTxBuf   = (PUCHAR)API_VmmDmaAlloc(LW_CFG_VMM_PAGE_SIZE);
        pbufd->BUFD_uiBufAddr = (UINT32)penet->ENET_pcTxBuf;
        pbufd++;

        pbufd->BUFD_uiBufAddr = (UINT32)(penet->ENET_pcTxBuf + ENET_BUF_SZ);
        pbufd++;
    }
}
/*********************************************************************************************************
** 函数名称: enetBuffDescInit
** 功能描述: 初始化 ENET 缓冲描述符
** 输　入  : penet :   ENET 结构指针
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  enetBuffDescInit (struct netif *pnetif)
{
    BUFD *pbufd;
    ENET *penet;

    penet = NETIF_PRIV(pnetif);

    pbufd = (BUFD *)API_VmmDmaAlloc(LW_CFG_VMM_PAGE_SIZE);
    if (pbufd == LW_NULL) {
        return;
    }

    penet->ENET_pbufdRxbdBase = pbufd;
    penet->ENET_pbufdTxbdBase = penet->ENET_pbufdRxbdBase + ENET_RX_RING_SZ;

    enetAllocBuff(pnetif);
    enetBuffReset(pnetif);
}
/*********************************************************************************************************
** 函数名称: enetCoreInit
** 功能描述: Lwip 网络初始化函数
** 输　入  : pnetif  :  网络结构
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static err_t  enetCoreInit (struct netif  *pnetif)
{
    UINT8     ucHwAddr[6];

    if (!pnetif) {
        return  (ERR_VAL);
    }

    pnetif->output     = etharp_output;
    pnetif->linkoutput = enetCoreTxStart;
    pnetif->output_ip6 = ethip6_output;
    pnetif->up         = enetCoreOpen;
    pnetif->down       = enetCoreClose;

    pnetif->flags = NETIF_FLAG_ETHARP | NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHERNET;
    pnetif->mtu   = 1500;

#if USER_CONFIG_HWADDR
    ucHwAddr[0] = 0x08;
    ucHwAddr[1] = 0x08;
    ucHwAddr[2] = 0x3E;
    ucHwAddr[3] = 0x26;
    ucHwAddr[4] = 0x01;
    ucHwAddr[5] = 0x32;
#else
    enetGetHwAddr(ucHwAddr);                                            /*  从 OTP 中得到硬件地址       */
#endif                                                                  /*  USER_CONFIG_HWADDR          */

    lib_srand((ucHwAddr[0] << 24) |
              (ucHwAddr[1] << 16) |
              (ucHwAddr[2] <<  8) |
              (ucHwAddr[3]));                                           /*  可以用 mac 设置随机数种子   */

    if (!pnetif->hwaddr_len) {
        lib_memcpy(pnetif->hwaddr, ucHwAddr, 6);
        pnetif->hwaddr_len = 6;
    }

    return  (ERR_OK);
}
/*********************************************************************************************************
** 函数名称: enetRegister
** 功能描述: 注册网络接口
** 输　入  : pnetif  :  网络结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  enetRegister (struct netif  *pnetif)
{
    ip_addr_t    ip, submask, gateway;

    IP4_ADDR(&ip,       192, 168,   1,  32);
    IP4_ADDR(&submask,  255, 255, 255,   0);
    IP4_ADDR(&gateway,  192, 168,   1,   1);

#if LWIP_NETIF_HOSTNAME
    pnetif->hostname = HOSTNAME;
#endif

    pnetif->name[0] = IFNAME0;
    pnetif->name[1] = IFNAME1;

    NETIF_INIT_SNMP(pnetif, snmp_ifType_ethernet_csmacd, 100000000);

    netifapi_netif_add(pnetif, &ip, &submask, &gateway,
                       pnetif->state, enetCoreInit,
                       tcpip_input);

    pnetif->ip6_autoconfig_enabled = 1;                                 /*  允许 IPv6 地址自动配置      */

    netif_create_ip6_linklocal_address(pnetif, 1);                      /*  构造 ipv6 link address      */

    netifapi_netif_set_up(pnetif);
    netifapi_netif_set_default(pnetif);
}
/*********************************************************************************************************
** 函数名称: enetInit
** 功能描述: ENET 初始化
** 输　入  : NONE
** 输　出  : 错误码
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID enetHwInit (VOID)
{
    /*
     * Pins init
     */
    enet_board_init();
}
/*********************************************************************************************************
** 函数名称: enetInit
** 功能描述: ENET 初始化
** 输　入  : NONE
** 输　出  : 错误码
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  enetInit (VOID)
{
    static struct netif netif;
    MII_DRV            *pmiidrv;
    INT                 iRet;

    enetHwInit();
    netif.state = &_G_enetInfo;

    pmiidrv = miiDrvInit();
    if (!pmiidrv) {
        return  (PX_ERROR);
    }

    _G_enetInfo.ENET_miidrv = pmiidrv;
    pmiidrv->MIID_enet      = &_G_enetInfo;

    enetTxNetJobqueueInit();

    enetSetPhySpeed(&netif);

    iRet = enetThreadCreate(&netif);
    if (iRet) {
        _ErrorHandle(EINVAL);
        return  (PX_ERROR);
    }

    enetIntInit(&netif);
    enetBuffDescInit(&netif);
    enetRegister(&netif);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
  END
*********************************************************************************************************/

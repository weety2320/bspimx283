/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: uart.h
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 21 日
**
** 描        述: IMX283 串口驱动头文件
*********************************************************************************************************/
#ifndef __UART_H
#define __UART_H

/*********************************************************************************************************
  函数声明
*********************************************************************************************************/
void imx28_debug_uart_init(const char *dev_name, int intnr);
void send_char(unsigned char *ch);
void init_debug_uart(void *base, uint32_t baud);

#endif                                                                    /*  __UART_H                  */
/*********************************************************************************************************
  END
*********************************************************************************************************/


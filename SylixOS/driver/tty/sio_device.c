/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: sio_device.c
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 21 日
**
** 描        述: IMX283 串口驱动头文件
*********************************************************************************************************/
#define  __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <stddef.h>
#include <string.h>

#include "config.h"
#include "sio_device.h"
/*********************************************************************************************************
  宏定义
*********************************************************************************************************/
#define __DEF_THIZ(psio) struct sio_device *thiz = \
                               _LIST_ENTRY(psio, struct sio_device, sio.pDrvFuncs)
/*********************************************************************************************************
  函数声明
*********************************************************************************************************/
static int   __uart_ioctl(SIO_CHAN  *chan, int  cmd, void *arg);
static int   __startup(SIO_CHAN     *chan);
static int   __callback_intsall(SIO_CHAN        *chan,
                                int              callback_type,
                                VX_SIO_CALLBACK  callback,
                                void            *arg);
static irqreturn_t __isr(struct sio_device *thiz);
static int   __poll_rx_char(SIO_CHAN  *chan,
                            char      *out);
static int   __poll_tx_char(SIO_CHAN  *chan, char in);
/*********************************************************************************************************
  全局变量
*********************************************************************************************************/
static SIO_DRV_FUNCS   __sio_funcs = {
    __uart_ioctl,
    __startup,
    __callback_intsall,
    __poll_rx_char,
    __poll_tx_char
};
/*********************************************************************************************************
** 函数名称: sio_driver_init
** 功能描述: SIO 驱动初始化
** 输  入  : thiz  :  驱动结构
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void sio_driver_init (struct sio_driver *drv)
{
    memset(drv, 0x00, sizeof(*drv));
}
/*********************************************************************************************************
** 函数名称: sio_device_init
** 功能描述: 中断系统初始化
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int sio_device_init (struct sio_device *thiz, const char *dev_name, struct sio_driver *drv)
{
    thiz->sio.pDrvFuncs = &__sio_funcs;
    thiz->channel_mode  = SIO_MODE_INT;
    thiz->opened        = 0;
    thiz->pdrv          = drv;
    
    API_InterVectorConnect(thiz->pdrv->interrupt_number,
                           (PINT_SVR_ROUTINE)__isr,
                           (PVOID)thiz,
                           "duart_isr");

    return ttyDevCreate((PCHAR)dev_name, &thiz->sio, 30, 50);
}
/*********************************************************************************************************
** 函数名称: sio_driver_option
** 功能描述: 设置驱动选项
** 输  入  : pdrv   :  驱动结构
**           option :  选项
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static void  __sio_driver_option (struct sio_driver *pdrv, int option)
{
    if ((option & CS8) == CS8) {
        pdrv->bit_length = 8;
    } else if (option & CS7) {
        pdrv->bit_length = 7;
    } else if (option & CS6) {
        pdrv->bit_length = 6;
    } else {
        pdrv->bit_length = 5;
    }
    
    if (option & STOPB) {
        pdrv->stop_bits = 2;
    } else {
        pdrv->stop_bits = 1;
    }

    pdrv->parity = 0;

    if (option & PARENB) {
        if (option & PARODD) {
            pdrv->parity = 1;
        } else {
            pdrv->parity = 2;
        }
    }
}
/*********************************************************************************************************
** 函数名称: __uart_ioctl
** 功能描述: 中断系统初始化
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT __uart_ioctl (SIO_CHAN  *chan, INT  cmd, void  *arg)
{
    int need_init = 0;
    struct sio_driver *pdrv;
    
    __DEF_THIZ(chan);

    pdrv = thiz->pdrv;
    
    switch (cmd) {
    
    case SIO_BAUD_SET:
        thiz->baud = (INT)arg;
        need_init = 1;
        break;
        
    case SIO_BAUD_GET:
        *((LONG *)arg) = thiz->baud;
        break;
    
    case SIO_HW_OPTS_SET:
        thiz->option = (INT)arg;
        __sio_driver_option(thiz->pdrv, thiz->option);
        need_init = 1;
        break;
        
    case SIO_HW_OPTS_GET:
        *((LONG *)arg) = thiz->option;
        break;
    
    case SIO_OPEN:
        need_init = 1;
        API_InterVectorEnable(thiz->pdrv->interrupt_number);
        thiz->pdrv->int_disable(thiz->pdrv, 1, 0);
        thiz->pdrv->int_enable(thiz->pdrv, 0, 1);
        break;
        
    case SIO_HUP:
        thiz->pdrv->int_disable(thiz->pdrv, 1, 1);
        API_InterVectorDisable(thiz->pdrv->interrupt_number);
        break;
        
    default:
        _ErrorHandle(ENOSYS);
	    return ENOSYS;
	}

    if (need_init && pdrv && pdrv->update_setting) {
        pdrv->update_setting(pdrv);
    }
	
	return  0;
}
/*********************************************************************************************************
** 函数名称: __startup
** 功能描述: 发送开始
** 输  入  : chan  : sio 通道
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT   __startup (SIO_CHAN *chan)
{
    struct sio_driver *pdrv;
    INT (*get_tx_func)(void *arg, char *c);
    void *arg;
    int enable_tx_interrupt = 1;

    __DEF_THIZ(chan);

    pdrv = thiz->pdrv;
    
    get_tx_func = thiz->get_tx_func;
    arg = thiz->get_tx_arg;

    if (get_tx_func && pdrv->poll_putc) {
        while (!pdrv->is_full(pdrv)) {
            char c;
            int err;
            err = get_tx_func(arg, &c);

            if (err == ERROR_NONE) {
                pdrv->poll_putc(pdrv, c);
            } else {
                enable_tx_interrupt = 0;
                pdrv->int_disable(pdrv, 1, 0);
                break;
            }
        }
    }
    
    if (thiz->channel_mode == SIO_MODE_INT && enable_tx_interrupt) {
        pdrv->int_enable(pdrv, enable_tx_interrupt, 1);
    }
    
    return 0;
}
/*********************************************************************************************************
** 函数名称: __callback_intsall
** 功能描述: 回调安装
** 输  入  : chan          : SIO 通道
**           callback_type : 回调类型
**           callback      : 回调函数
**           arg           : 回调参数
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT   __callback_intsall(SIO_CHAN          *chan,
                                int                callback_type,
                                VX_SIO_CALLBACK    callback,
                                void              *arg)
{
    __DEF_THIZ(chan);

    switch (callback_type) {
    
    case SIO_CALLBACK_GET_TX_CHAR:
        thiz->get_tx_func = (INT(*)(void*,char*))callback;
        thiz->get_tx_arg  = arg;
        break;
        
    case SIO_CALLBACK_PUT_RCV_CHAR:
        thiz->put_rx_func =	(INT(*)(void*,unsigned char))callback;
		thiz->put_rx_arg  = arg;
        break;
        
    default:
        return  (PX_ERROR);
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __isr
** 功能描述: 中断函数
** 输  入  : thiz  :  SIO 设备
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static irqreturn_t __isr (struct sio_device *thiz)
{
    struct sio_driver *pdrv;
    int need_tx_ack = 0;
    int need_rx_ack = 0;

    pdrv = thiz->pdrv;

    if (!pdrv->is_empty(pdrv)) {
        while (!pdrv->is_empty(pdrv)) {
            char c;
            pdrv->poll_getc(pdrv, &c);
            thiz->put_rx_func(thiz->put_rx_arg, c);
        }
        need_rx_ack = 1;
    }

    if (!pdrv->is_full(pdrv)) {
        while (!pdrv->is_full(pdrv)) {
            char c;
            if (ERROR_NONE == thiz->get_tx_func(thiz->get_tx_arg, &c)) {
                pdrv->poll_putc(pdrv, c);
            } else {
                pdrv->int_disable(pdrv, 1, 0);
                break;
            }
        }

        need_tx_ack = 1;
    }
    
    pdrv->interrupt_ack(pdrv, need_tx_ack, need_rx_ack);

    return  (LW_IRQ_HANDLED);
}
/*********************************************************************************************************
** 函数名称: __poll_rx_char
** 功能描述: 接收字符
** 输  入  : chan   : SIO 通道
**           out    : 接收字符缓冲
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT   __poll_rx_char (SIO_CHAN  *chan,
                             char      *out)
{
    struct sio_driver *pdrv;

    __DEF_THIZ(chan);

    pdrv = thiz->pdrv;
    if (!pdrv->is_empty(pdrv)) {
        pdrv->poll_getc(pdrv, out);
        return ERROR_NONE;
    }

    return PX_ERROR;
}
/*********************************************************************************************************
** 函数名称: __poll_tx_char
** 功能描述: 发送字符
** 输  入  : chan   : SIO 通道
**           c      : 字符
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __poll_tx_char (SIO_CHAN *chan, char c)
{
    struct sio_driver *pdrv;

    __DEF_THIZ(chan);

    pdrv = thiz->pdrv;

    while (pdrv->is_full(pdrv)) {
        ;
    }

    pdrv->poll_putc(pdrv, c);
    
    return ERROR_NONE;
}
/*********************************************************************************************************
  END
*********************************************************************************************************/

/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: sio_device.h
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 21 日
**
** 描        述: IMX283 SIO 驱动头文件
*********************************************************************************************************/

#ifndef __SIO_DEVICE_H__
#define __SIO_DEVICE_H__

#ifdef __cplusplus
extern "C" {
#endif                                                                  /* __cplusplus                  */
/*********************************************************************************************************
  SIO 驱动
*********************************************************************************************************/
struct sio_driver;
struct sio_driver {
    int interrupt_number;

    int  (*update_setting)(struct sio_driver *thiz);
    void (*int_enable)(struct sio_driver  *thiz, int tx, int rx);
    void (*int_disable)(struct sio_driver *thiz, int tx, int rx);
    int  (*is_full)(struct sio_driver *thiz);
    int  (*is_empty)(struct sio_driver *thiz);
    void (*poll_putc)(struct sio_driver *thiz, char  c);
    void (*poll_getc)(struct sio_driver *thiz, char *c);
    int  (*interrupt_ack)(struct sio_driver *thiz, int need_tx_ack, int need_rx_ack);

    int bit_length;                                                      /*  5, 6, 7, 8                 */
    int stop_bits;                                                       /*  1 or 2                     */
    int parity;                                                          /*  0: none 1: odd 2: even     */
    int rate;                                                            /*  9600, 38400, 115200, ...   */
    int enable;                                                          /*  0: disable.  1: enable     */
};

/*********************************************************************************************************
  SIO 设备结构
*********************************************************************************************************/
struct sio_device {
    SIO_CHAN                 sio;
    struct sio_driver       *pdrv;

    INT                     (*get_tx_func)(void *arg, char *c);
    INT                     (*put_rx_func)(void *arg, unsigned char c);
    PVOID                    get_tx_arg;
    PVOID                    put_rx_arg;

    INT                      channel_mode;
    int                      channel_num;
    int                      baud;
    int                      option;
    int                      opened;
};

/*********************************************************************************************************
  系统 USB PLL 主频选择
*********************************************************************************************************/
int  sio_device_init(struct sio_device *thiz,
                     const char *dev_name,
                     struct sio_driver *pdrv);
void sio_driver_init(struct sio_driver *pdrv);

#ifdef __cplusplus
}
#endif                                                                  /*  __cplusplus                 */

#endif                                                                  /*  __SIO_DEVICE_H__            */
/*********************************************************************************************************
  END
*********************************************************************************************************/

/**********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: uart.c
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: IMX28 uart驱动
*********************************************************************************************************/
#define __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <linux/compat.h>
#include <stdint.h>

#include "sio_device.h"
#include "driver/regs/mx28.h"
/*********************************************************************************************************
  宏定义
*********************************************************************************************************/
#define CLKCTRL_XTAL_ADDR             (CLKCTRL_PHYS_ADDR + 0x080)
#define CLKCTRL_XTAL_CLR_ADDR         (CLKCTRL_XTAL_ADDR + 0x008)
/*********************************************************************************************************
  结构体定义
*********************************************************************************************************/
/*********************************************************************************************************
  串口结构体定义
*********************************************************************************************************/
struct stmp_serial {
    volatile uint32_t HW_UARTDBGDR;
    volatile uint32_t HW_UARTDBGRSR;
    volatile uint32_t reserved1[4];
    volatile uint32_t HW_UARTDBGFR;
    volatile uint32_t reserved2;
    volatile uint32_t HW_UARTDBGILPR;
    volatile uint32_t HW_UARTDBGIBRD;
    volatile uint32_t HW_UARTDBGFBRD;
    volatile uint32_t HW_UARTDBGLCR;
    volatile uint32_t HW_UARTDBGCR;
    volatile uint32_t HW_UARTDBGIFLS;
    volatile uint32_t HW_UARTDBGIMSC;
    volatile uint32_t HW_UARTDBGRIS;
    volatile uint32_t HW_UARTDBGMIS;
    volatile uint32_t HW_UARTDBGICR;
    volatile uint32_t HW_UARTDBGMACR;
};
/*********************************************************************************************************
  IMX28 SIO 驱动结构
*********************************************************************************************************/
struct imx28_easy_sio_driver {
    struct sio_driver     sio;
    struct stmp_serial   *reg;
};
/*********************************************************************************************************
  函数声明
*********************************************************************************************************/
static int  __update_setting (struct sio_driver *_this);
static void __int_enable     (struct sio_driver *_this, int tx, int rx);
static void __int_disable    (struct sio_driver *_this, int tx, int rx);
static int  __is_full        (struct sio_driver *_this);
static int  __is_empty       (struct sio_driver *_this);
static void __poll_putc      (struct sio_driver *_this, char  c);
static void __poll_getc      (struct sio_driver *_this, char *c);
static int  __interrupt_ack  (struct sio_driver *_this, int need_tx_ack, int need_rx_ack);
/*********************************************************************************************************
  宏定义
*********************************************************************************************************/
#define __DECL_THIZ(p)  struct imx28_easy_sio_driver *thiz = \
                         (struct imx28_easy_sio_driver*)p;   \
                          struct stmp_serial *reg = thiz->reg
/*********************************************************************************************************
  全局变量定义
*********************************************************************************************************/
static volatile struct stmp_serial *stmp_debug_uart = (struct stmp_serial*)DUART_PHYS_ADDR;
/*********************************************************************************************************
** 函数名称: imx28_easy_sio_driver_init
** 功能描述: 驱动初始化
** 输  入  : thiz  :  驱动结构
**           base  :  Debug uart 基地址
**           intnr :  Debug uart 中断号
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static void imx28_easy_sio_driver_init (struct imx28_easy_sio_driver *thiz,
                                        void *base, int intnr)
{
    sio_driver_init(&thiz->sio);
    thiz->reg                  = (struct stmp_serial*)base;
    thiz->sio.update_setting   = __update_setting;
    thiz->sio.int_enable       = __int_enable;
    thiz->sio.int_disable      = __int_disable;
    thiz->sio.is_full          = __is_full;
    thiz->sio.is_empty         = __is_empty;
    thiz->sio.poll_putc        = __poll_putc;
    thiz->sio.poll_getc        = __poll_getc;
    thiz->sio.interrupt_ack    = __interrupt_ack;
    thiz->sio.interrupt_number = intnr;
}
/*********************************************************************************************************
** 函数名称: imx28_debug_uart_init
** 功能描述: uart 初始化
** 输  入  : dev_name  :  Debug uart 设备名
**           intnr     :  Debug uart 中断号
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void imx28_debug_uart_init (const char *dev_name, int intnr)
{
    static struct imx28_easy_sio_driver easy_driver;
    static struct sio_device siodev;
    static int inited = 0;

    if (inited) {
        return;
    }

    inited = 1;

    imx28_easy_sio_driver_init(&easy_driver,  (void*)stmp_debug_uart, intnr);
    sio_device_init(&siodev, dev_name, &easy_driver.sio);
}
/*********************************************************************************************************
** 函数名称: __update_setting
** 功能描述: 更新 duart 状态
** 输  入  : _this   :  SIO 驱动结构
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static int  __update_setting (struct sio_driver *_this)
{
    __DECL_THIZ(_this);

    reg->HW_UARTDBGCR = (0x3 << 8) | 0x1;                               /*  enable Tx, Rx and UART      */

    return 0;
}
/*********************************************************************************************************
** 函数名称: __int_enable
** 功能描述: 中断使能
** 输  入  : _this   :  SIO 驱动结构
**           tx      :  是否使能发送中断
**           rx      :  是否使能接收中断
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static void __int_enable (struct sio_driver  *_this, int tx, int rx)
{
    __DECL_THIZ(_this);

    if (rx) {
        reg->HW_UARTDBGIMSC |= (1 << 4);
        reg->HW_UARTDBGIMSC |= (1 << 6);
    }

    if (tx) {
        reg->HW_UARTDBGIMSC |= (1 << 5);
    }
}
/*********************************************************************************************************
** 函数名称: __int_disable
** 功能描述: 中断禁能
** 输  入  : _this   :  SIO 驱动结构
**           tx      :  是否禁能发送中断
**           rx      :  是否禁能接收中断
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static void __int_disable (struct sio_driver *_this, int tx, int rx)
{
    __DECL_THIZ(_this);

    if (rx) {
        reg->HW_UARTDBGIMSC &= ~(1 << 4);
    }

    if (tx) {
        reg->HW_UARTDBGIMSC &= ~(1 << 5);
    }
}
/*********************************************************************************************************
** 函数名称: __is_full
** 功能描述: 判断发送 FIFO 是否满
** 输  入  : _this   :  SIO 驱动结构
** 输  出  : 状态值
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static int  __is_full (struct sio_driver *_this)
{
    __DECL_THIZ(_this);

    return reg->HW_UARTDBGFR & (1 << 5);                                 /*  transmit fifo is full      */
}
/*********************************************************************************************************
** 函数名称: __is_empty
** 功能描述: 判断接收 FIFO 是否为空
** 输  入  : _this   :  SIO 驱动结构
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static int  __is_empty (struct sio_driver *_this)
{
    __DECL_THIZ(_this);

    return reg->HW_UARTDBGFR & (1 << 4);                                /*  receive fifo is empty       */
}
/*********************************************************************************************************
** 函数名称: __poll_putc
** 功能描述: 发送一个字符
** 输  入  : _this   :  SIO 驱动结构
**           c       :  发送字符
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static void __poll_putc (struct sio_driver  *_this, char  c)
{
    __DECL_THIZ(_this);

    reg->HW_UARTDBGDR = c;
}
/*********************************************************************************************************
** 函数名称: __poll_getc
** 功能描述: 接收一个字符
** 输  入  : _this   :  SIO 驱动结构
**           c       :  接收字符地址
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static void __poll_getc (struct sio_driver *_this, char *c)
{
    __DECL_THIZ(_this);

    *c = (reg->HW_UARTDBGDR & 0xFF);
}
/*********************************************************************************************************
** 函数名称: __interrupt_ack
** 功能描述: 清中断
** 输  入  : _this   :  SIO 驱动结构
**           tx      :  清发送中断
**           rx      :  清接收中断
** 输  出  : 成功
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static int  __interrupt_ack (struct sio_driver *_this, int tx, int rx)
{
    __DECL_THIZ(_this);

    if (tx) {
        reg->HW_UARTDBGICR = (1 << 5);                                   /*  Transmit Interrupt Clear   */
    }

    if (rx) {
        reg->HW_UARTDBGICR = (1 << 4);                                   /*  Receive Interrupt Clear    */
        reg->HW_UARTDBGICR = (1 << 6);                                   /*  Receive Timeout Int Clear  */
    }

    return 0;
}
/*********************************************************************************************************
** 函数名称: init_debug_uart
** 功能描述: 初始化 Debug uart
** 输  入  : base  :  Debug uart 基地址
**           baud  :  波特率
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void init_debug_uart (void *base, uint32_t baud)
{
    uint32_t val;

    if (base) {
        stmp_debug_uart = (volatile struct stmp_serial *)base;
    }

    duart_port_init();
    
    /*
     * check clocks
     */
    if ((readl(CLKCTRL_XTAL_ADDR) & 0x80000000)) {
        writel(0x80000000, CLKCTRL_XTAL_CLR_ADDR);                      /*  clock gate on               */
    }

    /*
     * make sure divider is 1 (24mhz), i.e. easiest to just set it..
     */
    val  = readl(CLKCTRL_XTAL_ADDR);
    val &= ~0x3;
    val |= 0x1;
    writel(val, CLKCTRL_XTAL_ADDR);

    /*
     * Wait for UART to finish transmitting
     */
    while (!(stmp_debug_uart->HW_UARTDBGFR & (1 << 7))) {
        ;
    }

    /*
     * Now that we have clocks, disable debug UART
     */
    stmp_debug_uart->HW_UARTDBGCR = 0x0;

    /*
     * Baud rate @115200 baud
     */
    stmp_debug_uart->HW_UARTDBGIBRD = 13;
    stmp_debug_uart->HW_UARTDBGFBRD = 1;

    /*
     * NOTE: This must happen AFTER setting the baud rate!
     * Set for 8 bits, 1 stop, no parity, enable fifo
     */
    stmp_debug_uart->HW_UARTDBGLCR = (0x3 << 5) | 0x10;                    /* 8-bit word, Enable FIFOs  */

    /*
     *  Start it up
     */
    stmp_debug_uart->HW_UARTDBGCR = (0x3 << 8) | 0x1;                      /* enable Tx, Rx and UART    */
}
/*********************************************************************************************************
** 函数名称: send_char
** 功能描述: 发送字符
** 输  入  : ch    :  字符
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void send_char (unsigned char *ch)
{
    /*
     * Wait for Tx FIFO not full
     */
    while (stmp_debug_uart->HW_UARTDBGFR & (1 << 5)) {
        ;
    }

    if (*ch == '\n') {
        stmp_debug_uart->HW_UARTDBGDR = '\r';
        while (stmp_debug_uart->HW_UARTDBGFR & (1 << 5)) {
            ;
        }
    }

    stmp_debug_uart->HW_UARTDBGDR = *ch;
}
/*********************************************************************************************************
** 函数名称: receive_char
** 功能描述: 接收字符
** 输  入  : NONE
** 输  出  : 字符
** 全局变量:
** 调用模块:
*********************************************************************************************************/
char receive_char (void)
{
    /*
     *  If receive fifo is empty, return false
     */
    if (stmp_debug_uart->HW_UARTDBGFR & (1 << 4)) {
        return 0xFF;
    }

    return (uint8_t) (stmp_debug_uart->HW_UARTDBGDR & 0xFF);
}
/*********************************************************************************************************
  END
*********************************************************************************************************/

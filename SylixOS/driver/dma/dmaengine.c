/*
 * Copyright (C) 2009-2010 Freescale Semiconductor, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#define __SYLIXOS_KERNEL
#include "SylixOS.h"
#include "linux/err.h"
#include "apbh_dma.h"

#define MAX_DMA_CHANNELS MXS_MAX_DMA_CHANNELS

/*
 * The list of DMA drivers that manage various DMA channels. A DMA device
 * driver registers to manage DMA channels by calling mxs_dma_device_register().
 */

static LIST_HEAD(mxs_dma_devices);

/*
 * The array of struct mxs_dma_chan that represent every DMA channel in the
 * system. The index of the structure in the array indicates the specific DMA
 * hardware it represents (see mach-mx28/include/mach/dma.h).
 */

static struct mxs_dma_chan mxs_dma_channels[MAX_DMA_CHANNELS];

int mxs_dma_request(int channel)
{
	int ret = 0;
	struct mxs_dma_chan *pchan;

	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return -EINVAL;
	}

	pchan = mxs_dma_channels + channel;
	if ((pchan->flags & MXS_DMA_FLAGS_VALID) != MXS_DMA_FLAGS_VALID) {
	    ret = -ENODEV;
		goto err;
	}
	if (pchan->flags & MXS_DMA_FLAGS_ALLOCATED) {
        ret = -EBUSY;
		goto err;
	}
	pchan->flags |= MXS_DMA_FLAGS_ALLOCATED;
	pchan->active_num = 0;
	pchan->pending_num = 0;
	INIT_LIST_HEAD(&pchan->active);
	INIT_LIST_HEAD(&pchan->done);
err:

	return ret;
}

void mxs_dma_release(int channel)
{
	struct mxs_dma_chan *pchan;

	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return;
	}
	pchan = mxs_dma_channels + channel;

	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return;
	}

	if (pchan->flags & MXS_DMA_FLAGS_BUSY) {
		return;
	}

	pchan->active_num = 0;
	pchan->pending_num = 0;
	pchan->flags &= ~MXS_DMA_FLAGS_ALLOCATED;
}

int mxs_dma_enable(int channel)
{
	int ret = 0;
	struct mxs_dma_chan *pchan;
	struct mxs_dma_device *pdma;
	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return -EINVAL;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return -EINVAL;
	}

	/*
	 *  neednot mutex lock, this function will be called in irq context.
	 *  The mutex may cause process schedule.
	 */
	pdma = pchan->dma;
	if (pchan->pending_num && pdma->enable) {
		ret = pdma->enable(pchan, channel - pdma->chan_base);
	}
	pchan->flags |= MXS_DMA_FLAGS_BUSY;

	return ret;
}

void mxs_dma_disable(int channel)
{
	struct mxs_dma_chan *pchan;
	struct mxs_dma_device *pdma;
	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return;
	}
	if (!(pchan->flags & MXS_DMA_FLAGS_BUSY)) {
		return;
	}
	/*
	 *  neednot mutex lock, this function will be called in irq context.
	 *  The mutex may cause process schedule.
	 */
	pdma = pchan->dma;
	if (pdma->disable) {
		pdma->disable(pchan, channel - pdma->chan_base);
	}
	pchan->flags &= ~MXS_DMA_FLAGS_BUSY;
	pchan->active_num = 0;
	pchan->pending_num = 0;
	list_splice_init(&pchan->active, &pchan->done);
}

int mxs_dma_get_info(int channel, struct mxs_dma_info *info)
{
	struct mxs_dma_chan *pchan;
	struct mxs_dma_device *pdma;

	if (!info) {
		return -EINVAL;
	}
	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return -EINVAL;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return -EFAULT;
	}
	pdma = pchan->dma;
	if (pdma->info) {
		pdma->info(pdma, channel - pdma->chan_base, info);
	}

	return 0;
}

int mxs_dma_cooked(int channel, struct list_head *head)
{
	int sem;
	struct mxs_dma_chan *pchan;
	struct list_head *p, *q;
	struct mxs_dma_desc *pdesc;

	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return -EINVAL;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return -EINVAL;
	}

	sem = mxs_dma_read_semaphore(channel);
	if (sem < 0) {
		return sem;
	}
	if (sem == pchan->active_num) {
		return 0;
	}
	list_for_each_safe(p, q, &pchan->active) {
		if ((pchan->active_num) <= sem) {
			break;
		}

		pdesc = list_entry(p, struct mxs_dma_desc, node);
		pdesc->flags &= ~MXS_DMA_DESC_READY;
		if (head) {
			list_move_tail(p, head);
		} else {
			list_move_tail(p, &pchan->done);
		}

		if (pdesc->flags & MXS_DMA_DESC_LAST) {
			pchan->active_num--;
		}
	}
	if (sem == 0) {
		pchan->flags &= ~MXS_DMA_FLAGS_BUSY;
	}

	return 0;
}

void mxs_dma_reset(int channel)
{
	struct mxs_dma_chan *pchan;
	struct mxs_dma_device *pdma;
	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return;
	}
	pdma = pchan->dma;
	if (pdma->reset) {
		pdma->reset(pdma, channel - pdma->chan_base);
	}
}

void mxs_dma_freeze(int channel)
{
	struct mxs_dma_chan *pchan;
	struct mxs_dma_device *pdma;
	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return;
	}
	pdma = pchan->dma;
	if (pdma->freeze) {
		pdma->freeze(pdma, channel - pdma->chan_base);
	}
}

void mxs_dma_unfreeze(int channel)
{
	struct mxs_dma_chan *pchan;
	struct mxs_dma_device *pdma;

	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return;
	}
	pdma = pchan->dma;
	if (pdma->unfreeze) {
		pdma->unfreeze(pdma, channel - pdma->chan_base);
	}
}

int mxs_dma_read_semaphore(int channel)
{
	int ret = -EINVAL;
	struct mxs_dma_chan *pchan;
	struct mxs_dma_device *pdma;

	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return ret;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return ret;
	}
	pdma = pchan->dma;
	if (pdma->read_semaphore) {
		ret = pdma->read_semaphore(pdma, channel - pdma->chan_base);
	}

	return ret;
}

void mxs_dma_enable_irq(int channel, int en)
{
	struct mxs_dma_chan *pchan;
	struct mxs_dma_device *pdma;
	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return;
	}
	pdma = pchan->dma;
	if (pdma->enable_irq) {
		pdma->enable_irq(pdma, channel - pdma->chan_base, en);
	}
}

int mxs_dma_irq_is_pending(int channel)
{
	int ret = 0;
	struct mxs_dma_chan *pchan;
	struct mxs_dma_device *pdma;

	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return ret;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return ret;
	}
	pdma = pchan->dma;
	if (pdma->irq_is_pending) {
		ret = pdma->irq_is_pending(pdma, channel - pdma->chan_base);
	}

	return ret;
}

void mxs_dma_ack_irq(int channel)
{
	struct mxs_dma_chan *pchan;
	struct mxs_dma_device *pdma;

	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return;
	}
	pdma = pchan->dma;
	if (pdma->ack_irq) {
		pdma->ack_irq(pdma, channel - pdma->chan_base);
	}
}

void mxs_dma_set_target(int channel, int target)
{
	struct mxs_dma_chan *pchan;
	struct mxs_dma_device *pdma;

	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return;
	}
	if (pchan->flags & MXS_DMA_FLAGS_BUSY) {
		return;
	}
	pdma = pchan->dma;
	if (pdma->set_target) {
		pdma->set_target(pdma, channel - pdma->chan_base, target);
	}
}

/* mxs dma utility function */
struct mxs_dma_desc *mxs_dma_alloc_desc(void)
{
	struct mxs_dma_desc *pdesc;

    pdesc = (struct mxs_dma_desc *)API_VmmDmaAlloc(LW_CFG_VMM_PAGE_SIZE);
    if (pdesc == NULL) {
        return NULL;
    }
    memset(pdesc, 0, sizeof(*pdesc));
    pdesc->address = (dma_addr_t)pdesc;

	return pdesc;
}

void mxs_dma_free_desc(struct mxs_dma_desc *pdesc)
{
	if (pdesc) {
	    API_VmmDmaFree(pdesc);
	}
}

int mxs_dma_desc_append(int channel, struct mxs_dma_desc *pdesc)
{
	int ret = 0;
	struct mxs_dma_chan *pchan;
	struct mxs_dma_desc *last;

	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return -EINVAL;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return -EINVAL;
	}

	pdesc->cmd.next = mxs_dma_cmd_address(pdesc);
	pdesc->flags |= MXS_DMA_DESC_FIRST | MXS_DMA_DESC_LAST;

	if (!list_empty(&pchan->active)) {
		last = list_entry(pchan->active.prev,
				  struct mxs_dma_desc, node);
        pdesc->flags &= ~MXS_DMA_DESC_FIRST;
        last->flags &= ~MXS_DMA_DESC_LAST;

		last->cmd.next = mxs_dma_cmd_address(pdesc);
		last->cmd.cmd.bits.chain = 1;
	}
	pdesc->flags |= MXS_DMA_DESC_READY;
	if (pdesc->flags & MXS_DMA_DESC_FIRST) {
		pchan->pending_num++;
	}
	list_add_tail(&pdesc->node, &pchan->active);

	return ret;
}

int mxs_dma_desc_add_list(int channel, struct list_head *head)
{
	int ret = 0, size = 0;
	struct mxs_dma_chan *pchan;
	struct list_head *p;
	struct mxs_dma_desc *prev = NULL, *pcur;

	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return -EINVAL;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return -EINVAL;
	}

	if (list_empty(head)) {
		return 0;
	}

	list_for_each(p, head) {
		pcur = list_entry(p, struct mxs_dma_desc, node);
		if (!(pcur->cmd.cmd.bits.dec_sem || pcur->cmd.cmd.bits.chain)) {
			return -EINVAL;
		}
		if (prev) {
			prev->cmd.next = mxs_dma_cmd_address(pcur);
		} else {
			pcur->flags |= MXS_DMA_DESC_FIRST;
		}
		pcur->flags |= MXS_DMA_DESC_READY;
		prev = pcur;
		size++;
	}
	pcur = list_first_entry(head, struct mxs_dma_desc, node);
	prev->cmd.next = mxs_dma_cmd_address(pcur);
	prev->flags |= MXS_DMA_DESC_LAST;

	if (!list_empty(&pchan->active)) {
		pcur = list_entry(pchan->active.next,
				  struct mxs_dma_desc, node);
		if (pcur->cmd.cmd.bits.dec_sem != prev->cmd.cmd.bits.dec_sem) {
			ret = -EFAULT;
			goto out;
		}
		prev->cmd.next = mxs_dma_cmd_address(pcur);
		prev = list_entry(pchan->active.prev,
				  struct mxs_dma_desc, node);
		pcur = list_first_entry(head, struct mxs_dma_desc, node);
		pcur->flags &= ~MXS_DMA_DESC_FIRST;
		prev->flags &= ~MXS_DMA_DESC_LAST;
		prev->cmd.next = mxs_dma_cmd_address(pcur);
	}
	list_splice(head, &pchan->active);
	pchan->pending_num += size;
	if (!(pcur->cmd.cmd.bits.dec_sem) && (pcur->flags & MXS_DMA_DESC_FIRST)) {
		pchan->pending_num += 1;
	} else {
		pchan->pending_num += size;
	}
out:

	return ret;
}

int mxs_dma_get_cooked(int channel, struct list_head *head)
{
	struct mxs_dma_chan *pchan;

	if ((channel < 0) || (channel >= MAX_DMA_CHANNELS)) {
		return -EINVAL;
	}
	pchan = mxs_dma_channels + channel;
	if (!(pchan->flags & MXS_DMA_FLAGS_ALLOCATED)) {
		return -EINVAL;
	}

	if (head == NULL) {
		return 0;
	}

	list_splice(&pchan->done, head);

	return 0;
}

int mxs_dma_device_register(struct mxs_dma_device *pdev)
{
	int i;
	struct mxs_dma_chan *pchan;

	if (pdev == NULL || !pdev->chan_num) {
		return -EINVAL;
	}

	if ((pdev->chan_base >= MAX_DMA_CHANNELS) ||
	    ((pdev->chan_base + pdev->chan_num) > MAX_DMA_CHANNELS)) {
		return -EINVAL;
	}

	pchan = mxs_dma_channels + pdev->chan_base;
	for (i = 0; i < pdev->chan_num; i++, pchan++) {
		pchan->dma = pdev;
		pchan->flags = MXS_DMA_FLAGS_VALID;
	}
	list_add(&pdev->node, &mxs_dma_devices);

	return 0;
}

/* DMA Operation */
int mxs_dma_init(void)
{
    dma_apbh_probe();

    return 0;
}

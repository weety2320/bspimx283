/*
 * Copyright (C) 2009-2010 Freescale Semiconductor, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#define __SYLIXOS_KERNEL
#include "SylixOS.h"
#include "linux/err.h"
#include "driver/regs/mx28.h"
#include "config.h"
#include "apbh_dma.h"
#include "driver/regs/regs_apbh.h"

#ifndef BM_APBH_CTRL0_APB_BURST_EN
#define BM_APBH_CTRL0_APB_BURST_EN BM_APBH_CTRL0_APB_BURST4_EN
#endif

static int mxs_dma_apbh_enable(struct mxs_dma_chan *pchan, unsigned int chan)
{
	unsigned int sem;
	struct mxs_dma_device *pdev = pchan->dma;
	struct mxs_dma_desc *pdesc;

	pdesc = list_first_entry(&pchan->active, struct mxs_dma_desc, node);
	if (pdesc == NULL) {
		return -EFAULT;
	}

	sem = readl(pdev->base + HW_APBH_CHn_SEMA(chan));
	sem = (sem & BM_APBH_CHn_SEMA_PHORE) >> BP_APBH_CHn_SEMA_PHORE;
	if (pchan->flags & MXS_DMA_FLAGS_BUSY) {
		if (pdesc->cmd.cmd.bits.chain == 0) {
			return 0;
		}
		if (sem < 2) {
			if (!sem) {
				return 0;
			}
			pdesc = list_entry(pdesc->node.next,
					   struct mxs_dma_desc, node);
			writel(mxs_dma_cmd_address(pdesc),
				     pdev->base + HW_APBH_CHn_NXTCMDAR(chan));
		}
		sem = pchan->pending_num;
		pchan->pending_num = 0;
		writel(BF_APBH_CHn_SEMA_INCREMENT_SEMA(sem),
			     pdev->base + HW_APBH_CHn_SEMA(chan));
		pchan->active_num += sem;
		return 0;
	}
	pchan->active_num += pchan->pending_num;
	pchan->pending_num = 0;
	writel(mxs_dma_cmd_address(pdesc),
		     pdev->base + HW_APBH_CHn_NXTCMDAR(chan));
	writel(pchan->active_num, pdev->base + HW_APBH_CHn_SEMA(chan));
	writel(1 << chan, pdev->base + HW_APBH_CTRL0_CLR);

	return 0;
}

static void mxs_dma_apbh_disable(struct mxs_dma_chan *pchan, unsigned int chan)
{
	struct mxs_dma_device *pdev = pchan->dma;
	writel(1 << (chan + BP_APBH_CTRL0_CLKGATE_CHANNEL),
		pdev->base + HW_APBH_CTRL0_SET);
}

static void mxs_dma_apbh_reset(struct mxs_dma_device *pdev, unsigned int chan)
{
#ifdef CONFIG_ARCH_MX28
    writel(1 << (chan + BP_APBH_CHANNEL_CTRL_RESET_CHANNEL),
		     pdev->base + HW_APBH_CHANNEL_CTRL_SET);
#endif

#ifdef CONFIG_ARCH_MX23
    writel(1 << (chan + BP_APBH_CTRL0_RESET_CHANNEL),
				pdev->base + HW_APBH_CTRL0_SET);
#endif
}

static void mxs_dma_apbh_freeze(struct mxs_dma_device *pdev, unsigned int chan)
{
#ifdef CONFIG_ARCH_MX28
    writel(1 << chan, pdev->base + HW_APBH_CHANNEL_CTRL_SET);
#endif

#ifdef CONFIG_ARCH_MX23
    writel(1 << (chan + BP_APBH_CTRL0_FREEZE_CHANNEL),
				pdev->base + HW_APBH_CTRL0_SET);
#endif
}

static void
mxs_dma_apbh_unfreeze(struct mxs_dma_device *pdev, unsigned int chan)
{
#ifdef CONFIG_ARCH_MX28
    writel(1 << chan, pdev->base + HW_APBH_CHANNEL_CTRL_CLR);
#endif

#ifdef CONFIG_ARCH_MX23
    writel(1 << (chan + BP_APBH_CTRL0_FREEZE_CHANNEL),
				pdev->base + HW_APBH_CTRL0_CLR);
#endif

}

static void mxs_dma_apbh_info(struct mxs_dma_device *pdev,
		unsigned int chan, struct mxs_dma_info *info)
{
	unsigned int reg;
	reg = readl(pdev->base + HW_APBH_CTRL2);
	info->status = reg >> chan;
	info->buf_addr = readl(pdev->base + HW_APBH_CHn_BAR(chan));
}

static int
mxs_dma_apbh_read_semaphore(struct mxs_dma_device *pdev, unsigned int chan)
{
	unsigned int reg;
	reg = readl(pdev->base + HW_APBH_CHn_SEMA(chan));
	return (reg & BM_APBH_CHn_SEMA_PHORE) >> BP_APBH_CHn_SEMA_PHORE;
}

static void
mxs_dma_apbh_enable_irq(struct mxs_dma_device *pdev,
			unsigned int chan, int enable)
{
	if (enable) {
		writel(1 << (chan + 16), pdev->base + HW_APBH_CTRL1_SET);
	} else {
	    writel(1 << (chan + 16), pdev->base + HW_APBH_CTRL1_CLR);
	}

}

static int
mxs_dma_apbh_irq_is_pending(struct mxs_dma_device *pdev, unsigned int chan)
{
	unsigned int reg;
	reg = readl(pdev->base + HW_APBH_CTRL1);
	reg |= readl(pdev->base + HW_APBH_CTRL2);
	return reg & (1 << chan);
}

static void mxs_dma_apbh_ack_irq(struct mxs_dma_device *pdev, unsigned int chan)
{
    writel(1 << chan, pdev->base + HW_APBH_CTRL1_CLR);
    writel(1 << chan, pdev->base + HW_APBH_CTRL2_CLR);
}

static struct mxs_dma_device mxs_dma_apbh = {
	.name = "mxs-dma-apbh",
	.enable = mxs_dma_apbh_enable,
	.disable = mxs_dma_apbh_disable,
	.reset = mxs_dma_apbh_reset,
	.freeze = mxs_dma_apbh_freeze,
	.unfreeze = mxs_dma_apbh_unfreeze,
	.info = mxs_dma_apbh_info,
	.read_semaphore = mxs_dma_apbh_read_semaphore,
	.enable_irq = mxs_dma_apbh_enable_irq,
	.irq_is_pending = mxs_dma_apbh_irq_is_pending,
	.ack_irq = mxs_dma_apbh_ack_irq,
};

int dma_apbh_probe(void)
{
	int i = 1000000;
	u32 base = APBH_DMA_PHYS_ADDR;

	mxs_dma_apbh.base = (void *)base;

	writel(BM_APBH_CTRL0_SFTRST, base + HW_APBH_CTRL0_CLR);


    for (; i > 0; --i) {
        if (!(readl(base + HW_APBH_CTRL0) &
              BM_APBH_CTRL0_SFTRST))
            break;
        udelay(2);
    }
    if (i <= 0) {
        return -ETIME;
    }

	writel(BM_APBH_CTRL0_CLKGATE, base + HW_APBH_CTRL0_CLR);

#if CONFIG_APBH_DMA_BURST8 > 0
	    writel(BM_APBH_CTRL0_AHB_BURST8_EN, base + HW_APBH_CTRL0_SET);
#else
	    writel(BM_APBH_CTRL0_AHB_BURST8_EN, base + HW_APBH_CTRL0_CLR);
#endif

#if CONFIG_APBH_DMA_BURST > 0
	    writel(BM_APBH_CTRL0_APB_BURST_EN, base + HW_APBH_CTRL0_SET);
#else
	    writel(BM_APBH_CTRL0_APB_BURST_EN, base + HW_APBH_CTRL0_CLR);
#endif

	mxs_dma_apbh.chan_base = MXS_DMA_CHANNEL_AHB_APBH;
	mxs_dma_apbh.chan_num = MXS_MAX_DMA_CHANNELS;

	return mxs_dma_device_register(&mxs_dma_apbh);
}

/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: imx28x_timer.h
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: timer 驱动头文件.
*********************************************************************************************************/
#ifndef __IMX28X_TIMER_H__
#define __IMX28X_TIMER_H__

#include <stdint.h>
#include "driver/regs/imx28_regtype.h"
#include "driver/regs/imx283_int.h"

/*********************************************************************************************************
  TIMER 寄存器结构体定义
*********************************************************************************************************/
struct imx28_timer_reg {
    volatile struct imx28_reg tim_ctrl;                                   /* [00] Control and status    */
    volatile struct imx28_reg running_count;                              /* [10] Running Count         */
    volatile struct imx28_reg fixed_count;                                /* [20] Fixed Count           */
    volatile struct imx28_reg match_count;                                /* [30] Match Count           */
};

/*********************************************************************************************************
  TIMER 结构体定义
*********************************************************************************************************/
struct freescale_timer {
    volatile struct imx28_timer_reg  *reg;                                /*  register base             */
    struct imx28_reg                 *HW_TIMEROT_ROTCTRL;
    enum imx28_int_num                inum;                               /*  interrupt number          */

    uint32_t                          clk_freq;
    uint32_t                          clk_freq_max;
    uint32_t                          clk_freq_min;

    /*
     *  private members
     */
    void (*pfunc_isr) (void *p_arg);
    void  *p_arg;
};

/*********************************************************************************************************
  宏定义
*********************************************************************************************************/
#define IM283_TIMER_DEF(name, base_address, ctrl_address, inum, freq)     \
        struct freescale_timer name = {                                   \
                                           (struct imx28_timer_reg *)base_address,  \
                                           (struct imx28_reg *)ctrl_address,     \
                                           inum,                          \
                                           freq,                          \
                                           0,                             \
                                           0                              \
                                      }

/*********************************************************************************************************
  函数声明
*********************************************************************************************************/
uint32_t timer_running_count(struct freescale_timer *thiz);
void     timer_clock_init(void);
void     timer_delay_us(unsigned long us);
void     timer_init(struct freescale_timer *thiz );
int      timer_enable(struct freescale_timer *thiz, uint32_t running_freq);
void     timer_isr(struct freescale_timer *thiz);
void     timer_destroy(struct freescale_timer *thiz);
void     timer_disable (struct freescale_timer *thiz);
void     timer_tick_high_resolution(struct freescale_timer *timer,
                                    struct timespec        *ptv);

#endif                                                                  /*  __IMX28X_TIMER_H__          */
/*********************************************************************************************************
  END
*********************************************************************************************************/


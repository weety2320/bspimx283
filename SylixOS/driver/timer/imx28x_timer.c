/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: imx28x_timer.c
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: timer 驱动.
*********************************************************************************************************/
#define __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <linux/compat.h>
#include <stdint.h>

#include "imx28x_timer.h"
#include "driver/regs/regs_timrot.h"
#include "driver/regs/imx28_regtype.h"
#include "driver/regs/mx28.h"
/*********************************************************************************************************
 宏定义
*********************************************************************************************************/
#define __BIT(bit)                     (1u << (bit))
#define __SBF(value, field)            ((value) << (field))

#define __CTRL_IRQ                      __BIT(15)
#define __CTRL_IRQ_EN                   __BIT(14)
#define __CTRL_UPDATE                   __BIT(7)
#define __CTRL_RELOAD                   __BIT(6)

#define __CTRL_PRESCALE                 __SBF(0x3, 4)
#define __CTRL_PRESCALE_VAL(val)        __SBF(val, 4)
#define __CTRL_SELECT_VAL(val)          __SBF(val, 0)
#define __CTRL_SELECT_VAL_NEVER_TICK    0x0
#define __CTRL_SELECT_VAL_TICK_ALWAYS   0x3
/*********************************************************************************************************
  精确时间换算参数
*********************************************************************************************************/
static uint32_t   __full_cnt;
static uint64_t   __nsec_per_cnt_7;
/*********************************************************************************************************
** 函数名称: timer_delay_us
** 功能描述: TIMER 延时
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void timer_delay_us (unsigned long us)
{
    if (us == 0) {
        return;
    }

    writel(0, DIGCTRL_MICROSECONDS_ADDR);                                 /*  clear the timer           */

    while (us > readl(DIGCTRL_MICROSECONDS_ADDR)) {
        ;
    }
}
/*********************************************************************************************************
** 函数名称: timer_version
** 功能描述: 获得 TIMER 版本
** 输  入  : major :  MARJOR field of the RTL version
**           minor :  MINOR  field of the RTL version
**           step  :  the stepping of the RTL version
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void timer_version (int *major, int *minor, int *step)
{
    struct {
        volatile unsigned int step:16;
        volatile unsigned int minor:8;
        volatile unsigned int major:8;
    }*v = (void*)HW_TIMROT_VERSION;

    *major = v->major;
    *minor = v->minor;
    *step =  v->step;
}
/*********************************************************************************************************
** 函数名称: timer_init
** 功能描述: 初始化 TIMER
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void timer_init (struct freescale_timer *thiz)
{
    mxs_reset_block(thiz->HW_TIMEROT_ROTCTRL, 1);

    thiz->clk_freq_max      = thiz->clk_freq >> 1;
    thiz->clk_freq_min      = 1;
    thiz->reg->tim_ctrl.clr = __CTRL_IRQ_EN;
    thiz->reg->tim_ctrl.clr = __CTRL_IRQ;                                   /*  clear IRQ flag          */
}
/*********************************************************************************************************
** 函数名称: timer_destroy
** 功能描述: TIMER 资源回收
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void timer_destroy (struct freescale_timer *thiz)
{
    timer_disable(thiz);
}
/*********************************************************************************************************
** 函数名称: timer_disable
** 功能描述: 禁能 TIMER
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void timer_disable (struct freescale_timer *thiz)
{
    /*
     * clear select field to set timer select to NEVER_TICK to stop the timer
     */
    thiz->reg->tim_ctrl.clr = 0x0f | __CTRL_IRQ_EN;
}
/*********************************************************************************************************
** 函数名称: timer_enable
** 功能描述: 使能 TIMER
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int timer_enable (struct freescale_timer *thiz, uint32_t running_freq)
{

    if (running_freq > thiz->clk_freq_max ||
        running_freq < thiz->clk_freq_min) {
        return -1;
    }
    
    __full_cnt = thiz->clk_freq / running_freq;
    __nsec_per_cnt_7 = ((1000 * 1000 * 1000 / running_freq) << 7) / __full_cnt;

    thiz->reg->fixed_count.set = 0;
    thiz->reg->tim_ctrl.clr    = 0;                                        /*  set prescale to 0        */
    thiz->reg->tim_ctrl.set    = __CTRL_IRQ_EN |__CTRL_RELOAD | __CTRL_UPDATE;
    thiz->reg->fixed_count.dat = __full_cnt;                               /*  set fix-count            */
    thiz->reg->tim_ctrl.set    =  0x0F;                                    /*  clock on                 */
    
    return 0;
}
/*********************************************************************************************************
** 函数名称: timer_running_count
** 功能描述: 获得 TIMER 运行数
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
uint32_t timer_running_count (struct freescale_timer *thiz)
{
    return thiz->reg->running_count.dat;
}
/*********************************************************************************************************
** 函数名称: timer_isr
** 功能描述: TIMER 中断
** 输  入  : NONE
** 输  出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void timer_isr (struct freescale_timer *thiz)
{
    /*
     * clear IRQ flag
     */
    thiz->reg->tim_ctrl.clr = __CTRL_IRQ;

    /*
     *  server func
     */
    if (thiz->pfunc_isr) {
        thiz->pfunc_isr(thiz->p_arg);
    }
}
/*********************************************************************************************************
** 函数名称: timer_tick_high_resolution
** 功能描述: 修正从最近一次 tick 到当前的精确时间.
** 输　入  : timer     TIMER
**           ptv       需要修正的时间
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void  timer_tick_high_resolution (struct freescale_timer *timer, struct timespec *ptv)
{
    register uint32_t  curr_cnt, do_cnt;

    /*
     * work out how many ticks have gone since last timer interrupt
     */
    curr_cnt = timer->reg->running_count.dat;
    do_cnt   = __full_cnt - curr_cnt;

    /*
     * check to see if there is an interrupt pending
     */
    if (timer->reg->tim_ctrl.dat & __CTRL_IRQ) {
        /*
         * re-read the timer, and try and fix up for the missed
         * interrupt. Note, the interrupt may go off before the
         * timer has re-loaded from wrapping.
         */
        curr_cnt = timer->reg->running_count.dat;
        do_cnt   = __full_cnt - curr_cnt;

        if (curr_cnt != 0) {
            do_cnt += __full_cnt;
        }
    }

    ptv->tv_nsec += (LONG)((__nsec_per_cnt_7 * do_cnt) >> 7);
    if (ptv->tv_nsec >= 1000000000) {
        ptv->tv_nsec -= 1000000000;
        ptv->tv_sec++;
    }
}
/*********************************************************************************************************
  END
*********************************************************************************************************/

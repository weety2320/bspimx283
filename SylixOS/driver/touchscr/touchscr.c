/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: touchscr.c
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: IM283 触摸屏驱动
** BUG:
**
2015.03.24  当没有点击事件时, 不用报到新的坐标值.
            中断、线程 同步机制使用技术型信号量.
2015.04.04  添加系统信号量LW_OPTION_OBJECT_GLOBAL参数.
*********************************************************************************************************/
#define __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <mouse.h>                                                      /*  鼠标                        */
#include <linux/compat.h>

#include "touchscr.h"
#include "driver/regs/imx283_int.h"
#include "driver/regs/mx28.h"
#include "driver/regs/regs_lradc.h"
#include "driver/lradc/lradc.h"
/*********************************************************************************************************
  宏定义 ADC 设备名
*********************************************************************************************************/
#define  TS_ADC_DEV_NAME        ("/dev/lradc")
/*********************************************************************************************************
  结构体定义 触摸屏设备
*********************************************************************************************************/
typedef struct ts_device {
    LW_DEV_HDR                  TS_devhdr;
    addr_t                      TS_atBase;

    /*
     *  驱动平台数据
     */
    TSDRV_DATA                 *TS_ptsdrvdata;

    /*
     *  坐标值 (转化后)
     */
    UINT16                      TS_usX;
    UINT16                      TS_usY;
    INT                         TS_iSampleCnt;                          /*  采样数                      */
    INT                         TS_iState;

    /*
     *  input 子系统数据
     */
    touchscreen_event_notify    TS_tsData;

    /*
     *  系统相关变量
     */
    INT                         TS_iFd;                                 /*  LRADC 设备文件描述符        */
    LW_SPINLOCK_DEFINE         (TS_slLock);
    ULONG                       TS_ulTouchIrq;
    ULONG                       TS_ulAdcIrq;
    time_t                      TS_timeCreate;                          /*  设备创建时间                */
    LW_SEL_WAKEUPLIST           TS_selwulList;                          /*  select() 等待链             */
    LW_OBJECT_HANDLE            TS_hTsSignal;                           /*  信号量句柄                  */
    LW_OBJECT_HANDLE            TS_hThread;                             /*  线程句柄                    */
    LW_OBJECT_HANDLE            TS_hLock;
} TS_DEV;
/*********************************************************************************************************
  触摸屏操作状态
*********************************************************************************************************/
typedef enum {
    TS_STATE_DISABLED,
    TS_STATE_TOUCH_DETECT,
    TS_STATE_TOUCH_VERIFY,
    TS_STATE_X_PLANE,
    TS_STATE_Y_PLANE,
} TS_STATE;
/*********************************************************************************************************
  定义驱动号
*********************************************************************************************************/
static INT                  _G_iTsDrvNum = 0;
/*********************************************************************************************************
** 函数名称: __tsDetectState
** 功能描述: 触摸屏进入探测状态 (TS_STATE_TOUCH_DETECT)
** 输　入  : ptsdev   触摸屏设备地址
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID __tsDetectState (TS_DEV  *ptsdev)
{
    LRADC_REG      lradcreg;

    lib_bzero(&lradcreg, sizeof(LRADC_REG));

    writel(0xFFFFFFFF,
           ptsdev->TS_atBase +
           HW_LRADC_CHn_CLR(ptsdev->TS_ptsdrvdata->TSDATA_ucXplusChan));
    writel(0xFFFFFFFF,
           ptsdev->TS_atBase +
           HW_LRADC_CHn_CLR(ptsdev->TS_ptsdrvdata->TSDATA_ucYplusChan));
    writel(0xFFFFFFFF,
           ptsdev->TS_atBase +
           HW_LRADC_CHn_CLR(ptsdev->TS_ptsdrvdata->TSDATA_ucXminusChan));
    writel(0xFFFFFFFF,
           ptsdev->TS_atBase +
           HW_LRADC_CHn_CLR(ptsdev->TS_ptsdrvdata->TSDATA_ucYminusChan));

    writel(BM_LRADC_CTRL1_LRADC0_IRQ << ptsdev->TS_ptsdrvdata->TSDATA_ucYminusChan,
           ptsdev->TS_atBase + HW_LRADC_CTRL1_CLR);

    writel(BM_LRADC_CTRL1_TOUCH_DETECT_IRQ,
           ptsdev->TS_atBase + HW_LRADC_CTRL1_CLR);
    /*
     * turn off the yplus and yminus pullup and pulldown, and turn off touch
     * detect (enables yminus, and xplus through a resistor.On a press,
     * xplus is pulled down)
     */
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiYplusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiYminusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiXplusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiXminusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);

    writel(BM_LRADC_CTRL0_TOUCH_DETECT_ENABLE,                            /*  触摸屏探测使能            */
            ptsdev->TS_atBase + HW_LRADC_CTRL0_SET);

    /*
     *  设置触发 KICK
     */
    lradcreg.LRADCREG_iTrigger = LRADC_DELAY_TRIGGER_TOUCHSCREEN;
    lradcreg.LRADCREG_iValue = 0;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_SET_DELAY_KICK, (LONG)&lradcreg);

    ptsdev->TS_iState = TS_STATE_TOUCH_DETECT;
    ptsdev->TS_iSampleCnt = 0;
}
/*********************************************************************************************************
** 函数名称: __tsDisableState
** 功能描述: 触摸屏进入禁能状态 (TS_STATE_DISABLED)
** 输　入  : ptsdev   触摸屏设备地址
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static  VOID  __tsDisableState (TS_DEV  *ptsdev)
{
    LRADC_REG      lradcreg;

    lib_bzero(&lradcreg, sizeof(LRADC_REG));

    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiYplusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiYminusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiXplusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiXminusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);

    writel(BM_LRADC_CTRL0_TOUCH_DETECT_ENABLE,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);

    lradcreg.LRADCREG_iTrigger = LRADC_DELAY_TRIGGER_TOUCHSCREEN;
    lradcreg.LRADCREG_iValue = 0;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_SET_DELAY_KICK, (LONG)&lradcreg);

    ptsdev->TS_iState = TS_STATE_DISABLED;
    ptsdev->TS_iSampleCnt = 0;
}
/*********************************************************************************************************
** 函数名称: __tsXplaneState
** 功能描述: 触摸屏进入获取 X 坐标状态 (TS_STATE_X_PLANE)
** 输　入  : ptsdev   触摸屏设备地址
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __tsXplaneState (TS_DEV  *ptsdev)
{
    LRADC_REG      lradcreg;

    lib_bzero(&lradcreg, sizeof(LRADC_REG));

    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiYplusVal,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_SET);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiYminusVal,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_SET);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiXplusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiXminusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);

    writel(BM_LRADC_CTRL0_TOUCH_DETECT_ENABLE,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);

    lradcreg.LRADCREG_iTrigger = LRADC_DELAY_TRIGGER_TOUCHSCREEN;
    lradcreg.LRADCREG_iValue = 1;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_SET_DELAY_KICK, (LONG)&lradcreg);

    ptsdev->TS_iState = TS_STATE_X_PLANE;
    ptsdev->TS_iSampleCnt = 0;
}
/*********************************************************************************************************
** 函数名称: __tsYplaneState
** 功能描述: 触摸屏进入获得 y 坐标状态 (TS_STATE_Y_PLANE)
** 输　入  : ptsdev   触摸屏设备地址
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __tsYplaneState (TS_DEV  *ptsdev)
{
    LRADC_REG      lradcreg;

    lib_bzero(&lradcreg, sizeof(LRADC_REG));

    writel(BM_LRADC_CTRL0_TOUCH_DETECT_ENABLE,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);

    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiYplusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiYminusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiXplusVal,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_SET);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiXminusVal,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_SET);

    lradcreg.LRADCREG_iTrigger = LRADC_DELAY_TRIGGER_TOUCHSCREEN;
    lradcreg.LRADCREG_iValue = 1;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_SET_DELAY_KICK, (LONG)&lradcreg);

    ptsdev->TS_iState = TS_STATE_Y_PLANE;
    ptsdev->TS_iSampleCnt = 0;
}
/*********************************************************************************************************
** 函数名称: __tsVerifyState
** 功能描述: 触摸屏进入分析状态 (TS_STATE_TOUCH_VERIFY)
** 输　入  : ptsdev   触摸屏设备地址
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __tsVerifyState (TS_DEV  *ptsdev)
{
    LRADC_REG      lradcreg;

    lib_bzero(&lradcreg, sizeof(LRADC_REG));

    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiYplusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiYminusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiXplusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);
    writel(ptsdev->TS_ptsdrvdata->TSDATA_uiXminusMask,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_CLR);

    writel(BM_LRADC_CTRL0_TOUCH_DETECT_ENABLE,
           ptsdev->TS_atBase + HW_LRADC_CTRL0_SET);

    lradcreg.LRADCREG_iTrigger = LRADC_DELAY_TRIGGER_TOUCHSCREEN;
    lradcreg.LRADCREG_iValue = 1;
    ptsdev->TS_iState = TS_STATE_TOUCH_VERIFY;

    ioctl(ptsdev->TS_iFd, LRADC_CMD_SET_DELAY_KICK, (LONG)&lradcreg);

    ptsdev->TS_iSampleCnt = 0;
}
/*********************************************************************************************************
** 函数名称: __tsProcessLradc
** 功能描述: 触摸屏处理中断状态
** 输　入  : ptsdev   触摸屏设备地址
**           usX      x 坐标
**           usY      y 坐标
**           iPress   点击状态
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __tsProcessLradc (TS_DEV  *ptsdev,
                               UINT16   usX,
                               UINT16   usY,
                               INT      iPress)
{
    switch (ptsdev->TS_iState) {

    case TS_STATE_X_PLANE:
        if (ptsdev->TS_iSampleCnt < 2) {
            ptsdev->TS_usX = usX;
            ptsdev->TS_iSampleCnt++;
        } else {
            if (abs(ptsdev->TS_usX - usX) > 100) {
                ptsdev->TS_iSampleCnt = 1;
            } else {
                UINT16  uc = ptsdev->TS_usX * (ptsdev->TS_iSampleCnt - 1);
                ptsdev->TS_usX = (uc + usX) / ptsdev->TS_iSampleCnt;
                ptsdev->TS_iSampleCnt++;
            }
        }

        if (ptsdev->TS_iSampleCnt > 4) {
            __tsYplaneState(ptsdev);

        } else {
            LRADC_REG      lradcreg;

            lib_bzero(&lradcreg, sizeof(LRADC_REG));

            lradcreg.LRADCREG_iTrigger = LRADC_DELAY_TRIGGER_TOUCHSCREEN;
            lradcreg.LRADCREG_iValue = 1;

            ioctl(ptsdev->TS_iFd, LRADC_CMD_SET_DELAY_KICK, (LONG)&lradcreg);
        }
        break;

    case TS_STATE_Y_PLANE:
        if (ptsdev->TS_iSampleCnt < 2) {
            ptsdev->TS_usY = usY;
            ptsdev->TS_iSampleCnt++;

        } else {
            if (abs(ptsdev->TS_usY - usY) > 100) {
                ptsdev->TS_iSampleCnt = 1;

            } else {
                UINT16  uc = ptsdev->TS_usY * (ptsdev->TS_iSampleCnt - 1);
                ptsdev->TS_usY = (uc + usY) / ptsdev->TS_iSampleCnt;
                ptsdev->TS_iSampleCnt++;
            }
        }

        if (ptsdev->TS_iSampleCnt > 4) {
            __tsVerifyState(ptsdev);

        } else {
            LRADC_REG      lradcreg;

            lib_bzero(&lradcreg, sizeof(LRADC_REG));

            lradcreg.LRADCREG_iTrigger = LRADC_DELAY_TRIGGER_TOUCHSCREEN;
            lradcreg.LRADCREG_iValue = 1;

            ioctl(ptsdev->TS_iFd, LRADC_CMD_SET_DELAY_KICK, (LONG)&lradcreg);
        }
        break;

    case TS_STATE_TOUCH_VERIFY:
        if (iPress) {
            ptsdev->TS_tsData.xmovement = ptsdev->TS_usY;
            ptsdev->TS_tsData.ymovement = ptsdev->TS_usX;
            ptsdev->TS_tsData.kstat     |= MOUSE_LEFT;
        } else {
            ptsdev->TS_tsData.kstat     &= ~MOUSE_LEFT;
        }

        SEL_WAKE_UP_ALL(&ptsdev->TS_selwulList,
                        SELREAD);                                       /*  释放所有等待读的线程        */

    case TS_STATE_TOUCH_DETECT:
        if (iPress) {
            __tsXplaneState(ptsdev);

        } else {
            __tsDetectState(ptsdev);
        }

        break;

    default:
        printk(KERN_ERR"Unkown touchscreen state.\n");
    }
}
/*********************************************************************************************************
** 函数名称: __tsThreadHandler
** 功能描述: 驱动线程函数
** 输　入  : pTsDev   触摸屏设备地址
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __tsThreadHandler (TS_DEV  *ptsdev)
{
    UINT16      uiXplus, uiYplus;
    INT         iPress;

    while (1) {
        API_SemaphoreCPend(ptsdev->TS_hTsSignal,
                           LW_OPTION_WAIT_INFINITE);

        /*
         *  触摸状态
         */
        if (readl(ptsdev->TS_atBase + HW_LRADC_STATUS) &
            BM_LRADC_STATUS_TOUCH_DETECT_RAW) {
            iPress = 1;

        } else {
            iPress = 0;
        }

        /*
         *  得到 x, y 坐标值
         */
        uiXplus = readl(ptsdev->TS_atBase +
                        HW_LRADC_CHn(ptsdev->TS_ptsdrvdata->TSDATA_ucXplusChan)) &
                        BM_LRADC_CHn_VALUE;
        uiYplus = readl(ptsdev->TS_atBase +
                        HW_LRADC_CHn(ptsdev->TS_ptsdrvdata->TSDATA_ucYplusChan)) &
                        BM_LRADC_CHn_VALUE;

        API_SemaphoreMPend(ptsdev->TS_hLock, LW_OPTION_WAIT_INFINITE);
        __tsProcessLradc(ptsdev, uiXplus, uiYplus, iPress);
        API_SemaphoreMPost(ptsdev->TS_hLock);
    }
}
/*********************************************************************************************************
** 函数名称: __tsIrq
** 功能描述: 驱动中断函数
** 输　入  : pvArg      中断参数
**           uiVector   中断向量号
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static irqreturn_t __tsIrq (PVOID  pvArg, UINT32  uiVector)
{
    TS_DEV  *ptsdev = (TS_DEV *)pvArg;

    if (uiVector == ptsdev->TS_ulTouchIrq) {
        writel(BM_LRADC_CTRL1_TOUCH_DETECT_IRQ,
               ptsdev->TS_atBase + HW_LRADC_CTRL1_CLR);
    } else if (uiVector == ptsdev->TS_ulAdcIrq) {
        writel(BM_LRADC_CTRL1_LRADC0_IRQ <<
               ptsdev->TS_ptsdrvdata->TSDATA_ucYminusChan,
               ptsdev->TS_atBase + HW_LRADC_CTRL1_CLR);
    }

    API_SemaphoreCPost(ptsdev->TS_hTsSignal);

    return  (LW_IRQ_HANDLED);
}
/*********************************************************************************************************
** 函数名称: __tsSysInit
** 功能描述: 驱动系统层次初始化
** 输　入  : ptsdev   触摸屏设备地址
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __tsSysInit (TS_DEV  *ptsdev)
{
    LW_CLASS_THREADATTR  threadAttr;

    if (!ptsdev) {
        _ErrorHandle(EFAULT);
        return  (PX_ERROR);
    }

    ptsdev->TS_hLock = API_SemaphoreMCreate("ts_lock",
                                            LW_PRIO_DEF_CEILING,
                                            LW_OPTION_INHERIT_PRIORITY |
                                            LW_OPTION_DELETE_SAFE      |
                                            LW_OPTION_OBJECT_GLOBAL,
                                            LW_NULL);
    if (ptsdev->TS_hLock == LW_HANDLE_INVALID) {
        _ErrorHandle(EINVAL);
        return  (PX_ERROR);
    }

    ptsdev->TS_hTsSignal = API_SemaphoreCCreate("ts_cntsem",
                                                0,
                                                4096,
                                                LW_OPTION_WAIT_FIFO |
                                                LW_OPTION_OBJECT_GLOBAL,
                                                LW_NULL);
    if (ptsdev->TS_hTsSignal == LW_OBJECT_HANDLE_INVALID) {
        _ErrorHandle(EINVAL);
        return  (PX_ERROR);
    }

    threadAttr = API_ThreadAttrGetDefault();
    threadAttr.THREADATTR_pvArg       = (PVOID)ptsdev;
    threadAttr.THREADATTR_ucPriority  = LW_PRIO_NORMAL - 1;
    threadAttr.THREADATTR_ulOption   |= LW_OPTION_OBJECT_GLOBAL;

    ptsdev->TS_hThread = API_ThreadCreate("t_touch",
                                          (PTHREAD_START_ROUTINE)__tsThreadHandler,
                                          &threadAttr, LW_NULL);
    if (ptsdev->TS_hThread == LW_OBJECT_HANDLE_INVALID) {
        _DebugHandle(__ERRORMESSAGE_LEVEL, "Thread create failed.\r\n");
        _ErrorHandle(EINVAL);
        return  (PX_ERROR);
    }

    /*
     *  请求触摸屏中断
     */
    API_InterVectorConnect(ptsdev->TS_ulTouchIrq,
                           (PINT_SVR_ROUTINE)__tsIrq,
                           (PVOID)ptsdev,
                           "ts_isr");

    /*
     *  请求 ADC 通道5 转换中断
     */
    API_InterVectorConnect(ptsdev->TS_ulAdcIrq,
                           (PINT_SVR_ROUTINE)__tsIrq,
                           (PVOID)ptsdev,
                           "adc5_isr");

    API_InterVectorEnable(ptsdev->TS_ulTouchIrq);
    API_InterVectorEnable(ptsdev->TS_ulAdcIrq);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __tsDevInit
** 功能描述: 驱动设备初始化
** 输　入  : ptsdev   触摸屏设备地址
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static  VOID  __tsDevInit (TS_DEV   *ptsdev)
{
    LRADC_REG      lradcreg;

    lib_bzero(&lradcreg, sizeof(LRADC_REG));

    __tsDetectState(ptsdev);

    /*
     *  通道选择
     */
    lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucXplusChan;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_USE_CHAN, (LONG)&lradcreg);

    lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucXminusChan;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_USE_CHAN, (LONG)&lradcreg);

    lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucYplusChan;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_USE_CHAN, (LONG)&lradcreg);

    lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucYminusChan;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_USE_CHAN, (LONG)&lradcreg);

    /*
     *  通道配置
     */
    lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucXplusChan;
    lradcreg.LRADCREG_iEnableDiv2 = 0;
    lradcreg.LRADCREG_iEnableAcc = 0;
    lradcreg.LRADCREG_iSamples = 0;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_CFG_CHAN, (LONG)&lradcreg);

    lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucXminusChan;
    lradcreg.LRADCREG_iEnableDiv2 = 0;
    lradcreg.LRADCREG_iEnableAcc = 0;
    lradcreg.LRADCREG_iSamples = 0;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_CFG_CHAN, (LONG)&lradcreg);

    lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucYplusChan;
    lradcreg.LRADCREG_iEnableDiv2 = 0;
    lradcreg.LRADCREG_iEnableAcc = 0;
    lradcreg.LRADCREG_iSamples = 0;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_CFG_CHAN, (LONG)&lradcreg);

    lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucYminusChan;
    lradcreg.LRADCREG_iEnableDiv2 = 0;
    lradcreg.LRADCREG_iEnableAcc = 0;
    lradcreg.LRADCREG_iSamples = 0;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_CFG_CHAN, (LONG)&lradcreg);

    /*
     *  清除累加器数值和采样数
     */
    writel(0xFFFFFFFF,
           ptsdev->TS_atBase +
           HW_LRADC_CHn_CLR(ptsdev->TS_ptsdrvdata->TSDATA_ucXplusChan));
    writel(0xFFFFFFFF,
           ptsdev->TS_atBase +
           HW_LRADC_CHn_CLR(ptsdev->TS_ptsdrvdata->TSDATA_ucXminusChan));
    writel(0xFFFFFFFF,
           ptsdev->TS_atBase +
           HW_LRADC_CHn_CLR(ptsdev->TS_ptsdrvdata->TSDATA_ucYplusChan));
    writel(0xFFFFFFFF,
           ptsdev->TS_atBase +
           HW_LRADC_CHn_CLR(ptsdev->TS_ptsdrvdata->TSDATA_ucYminusChan));

    /*
     *  设置延时触发参数
     */
    lradcreg.LRADCREG_iTrigger = LRADC_DELAY_TRIGGER_TOUCHSCREEN;
    lradcreg.LRADCREG_uiTriggerLradc = 0x3c;
    lradcreg.LRADCREG_uiDelayTriggers = 0;
    lradcreg.LRADCREG_uiLoops = 0;
    lradcreg.LRADCREG_uiDelays = 8;
    ioctl(ptsdev->TS_iFd, LRADC_CMD_SET_DELAY, (LONG)&lradcreg);

    /*
     *  清除中断位
     */
    writel(BM_LRADC_CTRL1_LRADC0_IRQ <<
           ptsdev->TS_ptsdrvdata->TSDATA_ucYminusChan,
           ptsdev->TS_atBase + HW_LRADC_CTRL1_CLR);
    writel(BM_LRADC_CTRL1_TOUCH_DETECT_IRQ,
           ptsdev->TS_atBase + HW_LRADC_CTRL1_CLR);

    /*
     *  使能中断位
     */
    writel(BM_LRADC_CTRL1_LRADC0_IRQ_EN <<
           ptsdev->TS_ptsdrvdata->TSDATA_ucYminusChan,
           ptsdev->TS_atBase + HW_LRADC_CTRL1_SET);

    writel(BM_LRADC_CTRL1_TOUCH_DETECT_IRQ_EN,
           ptsdev->TS_atBase + HW_LRADC_CTRL1_SET);
}
/*********************************************************************************************************
** 函数名称: __tsOpen
** 功能描述: 驱动OPEN函数
** 输　入  : ptsdev   触摸屏设备地址
**           pcName   设备名
**           iFlags   标志
**           iMode    模式
** 输　出  : 错误号 或 触摸屏设备地址
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static LONG  __tsOpen (TS_DEV   *ptsdev,
                       PCHAR     pcName,
                       INT       iFlags,
                       INT       iMode)
{
    INT  iFd;

    if (!ptsdev) {
        _ErrorHandle(EFAULT);
        return  (PX_ERROR);
    }

    if (LW_DEV_INC_USE_COUNT(&ptsdev->TS_devhdr) == 1) {
        iFd = open(TS_ADC_DEV_NAME, O_RDWR);
        if (iFd < 0) {
            printk(KERN_ERR"Open lradc device failed.\n");
            _ErrorHandle(EBADF);
            return  (PX_ERROR);
        }
        ptsdev->TS_iFd = iFd;

        ptsdev->TS_tsData.ctype     = MOUSE_CTYPE_ABS;
        ptsdev->TS_tsData.kstat     = 0;
        ptsdev->TS_tsData.xmovement = 0;
        ptsdev->TS_tsData.ymovement = 0;

        __tsDevInit(ptsdev);
    }

    return  ((LONG)ptsdev);
}
/*********************************************************************************************************
** 函数名称: __tsClose
** 功能描述: 驱动close函数
** 输　入  : ptsdev    触摸屏设备地址
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __tsClose (TS_DEV     *ptsdev)
{
    if (!ptsdev) {
        _ErrorHandle(EFAULT);
        return  (PX_ERROR);
    }

    if (LW_DEV_GET_USE_COUNT(&ptsdev->TS_devhdr)) {
        if (!LW_DEV_DEC_USE_COUNT(&ptsdev->TS_devhdr)) {
            LRADC_REG      lradcreg;

            lib_bzero(&lradcreg, sizeof(LRADC_REG));

            close(ptsdev->TS_iFd);
            SEL_WAKE_UP_ALL(&ptsdev->TS_selwulList,
                             SELEXCEPT);                                   /*  激活异常等待             */
            SEL_WAKE_UP_ALL(&ptsdev->TS_selwulList,
                             SELWRITE);                                    /*  激活写等待               */
            SEL_WAKE_UP_ALL(&ptsdev->TS_selwulList,
                             SELREAD);                                     /*  激活读等待               */
            API_InterVectorDisable(INUM_LRADC_TOUCH_IRQ);
            API_InterVectorDisable(INUM_LRADC_CH5_IRQ);
            __tsDisableState(ptsdev);

            lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucXplusChan;
            ioctl(ptsdev->TS_iFd, LRADC_CMD_UNUSE_CHAN, (LONG)&lradcreg);

            lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucXminusChan;
            ioctl(ptsdev->TS_iFd, LRADC_CMD_UNUSE_CHAN, (LONG)&lradcreg);

            lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucYplusChan;
            ioctl(ptsdev->TS_iFd, LRADC_CMD_UNUSE_CHAN, (LONG)&lradcreg);

            lradcreg.LRADCREG_iChan = ptsdev->TS_ptsdrvdata->TSDATA_ucYminusChan;
            ioctl(ptsdev->TS_iFd, LRADC_CMD_UNUSE_CHAN, (LONG)&lradcreg);
        }
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __tsRead
** 功能描述: 驱动读函数
** 输　入  : pTsDev      触摸屏设备地址
**           pvBuf       触摸数据Buf
**           stNbyte     要读的字节数
** 输　出  : 成功读取的字节数
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static ssize_t  __tsRead (TS_DEV       *ptsdev,
                          PVOID         pvBuf,
                          size_t        stNbyte)
{
    touchscreen_event_notify   *ptouchData = (touchscreen_event_notify *)pvBuf;
    INTREG                      iRegInterLevel;

    if (!ptouchData) {
        _DebugHandle(__ERRORMESSAGE_LEVEL, "Invalid parametor(null).\r\n");
        _ErrorHandle(EINVAL);
        return  (0);
    }

    if (!stNbyte) {
        return  (stNbyte);
    }

    LW_SPIN_LOCK_QUICK(&ptsdev->TS_slLock, &iRegInterLevel);
    ptouchData->ctype = ptsdev->TS_tsData.ctype;
    ptouchData->kstat = ptsdev->TS_tsData.kstat;
    ptouchData->xmovement = ptsdev->TS_tsData.xmovement;
    ptouchData->ymovement = ptsdev->TS_tsData.ymovement;
    LW_SPIN_UNLOCK_QUICK(&ptsdev->TS_slLock, iRegInterLevel);

    return  (sizeof(touchscreen_event_notify));
}
/*********************************************************************************************************
** 函数名称: __tsIoctl
** 功能描述: 驱动IOCTL
** 输　入  : pTsDev     触摸屏设备地址
**           iCmd       命令字
**           iArg       地址
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __tsIoctl (TS_DEV   *ptsdev,
                       INT       iCmd,
                       LONG      lArg)
{
    PLW_SEL_WAKEUPNODE   pselwunNode;
    INT                  iError = ERROR_NONE;
    struct stat         *pstatGet;

    switch (iCmd) {

    case FIOFSTATGET:
        pstatGet = (struct stat *)lArg;
        if (pstatGet) {
            pstatGet->st_dev     = (dev_t)ptsdev;
            pstatGet->st_ino     = (ino_t)0;                            /*  相当于唯一节点              */
            pstatGet->st_mode    = 0444 | S_IFCHR;
            pstatGet->st_nlink   = 1;
            pstatGet->st_uid     = 0;
            pstatGet->st_gid     = 0;
            pstatGet->st_rdev    = 1;
            pstatGet->st_size    = 0;
            pstatGet->st_blksize = 0;
            pstatGet->st_blocks  = 0;
            pstatGet->st_atime   = ptsdev->TS_timeCreate;
            pstatGet->st_mtime   = ptsdev->TS_timeCreate;
            pstatGet->st_ctime   = ptsdev->TS_timeCreate;

        } else {
            errno  = EINVAL;
            iError = PX_ERROR;
        }
        break;

    case FIOSELECT:
        pselwunNode = (PLW_SEL_WAKEUPNODE)lArg;
        SEL_WAKE_NODE_ADD(&ptsdev->TS_selwulList, pselwunNode);
        break;

    case FIOUNSELECT:
        SEL_WAKE_NODE_DELETE(&ptsdev->TS_selwulList, (PLW_SEL_WAKEUPNODE)lArg);
        break;

    default:
        iError = PX_ERROR;
        errno  = ENOSYS;
        break;
    }

    return  (iError);
}
/*********************************************************************************************************
** 函数名称: tsDrv
** 功能描述: 安装触摸屏设备驱动
** 输　入  : NONE
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  tsDrv (VOID)
{
    static struct file_operations fileop;

    if (_G_iTsDrvNum > 0) {
        return  (ERROR_NONE);
    }

    lib_bzero(&fileop, sizeof(struct file_operations));

    fileop.owner      = THIS_MODULE;
    fileop.fo_create  = __tsOpen;
    fileop.fo_release = LW_NULL;
    fileop.fo_open    = __tsOpen;
    fileop.fo_close   = __tsClose;
    fileop.fo_read    = __tsRead;
    fileop.fo_write   = LW_NULL;
    fileop.fo_ioctl   = __tsIoctl;

    _G_iTsDrvNum = iosDrvInstallEx(&fileop);

    DRIVER_LICENSE(_G_iTsDrvNum,     "Dual BSD/GPL->Ver 1.0");
    DRIVER_AUTHOR(_G_iTsDrvNum,      "Lu.Zhenping");
    DRIVER_DESCRIPTION(_G_iTsDrvNum, "imx283 TouchScreen.");

    return  ((_G_iTsDrvNum > 0) ? (ERROR_NONE) : (PX_ERROR));
}
/*********************************************************************************************************
** 函数名称: tsDevCreate
** 功能描述: 创建触摸屏设备
** 输　入  : cpcName  :  设备名
**           ptsdata  :  触摸屏驱动数据
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  tsDevCreate (CPCHAR  cpcName, TSDRV_DATA  *ptsdata)
{
    TS_DEV    *ptsdev;
    INT        iRet;

    if (!cpcName || !ptsdata) {
        _ErrorHandle(EINVAL);
        return (PX_ERROR);
    }

    if (_G_iTsDrvNum <= 0) {
        _ErrorHandle(ERROR_IO_NO_DRIVER);
        return  (PX_ERROR);
    }

    ptsdev = (TS_DEV *)__SHEAP_ALLOC(sizeof(TS_DEV));
    if (!ptsdev) {
        _ErrorHandle(ERROR_SYSTEM_LOW_MEMORY);
        return  (PX_ERROR);
    }

    lib_bzero(ptsdev, sizeof(TS_DEV));

    ptsdev->TS_atBase = LRADC_PHYS_ADDR;
    ptsdev->TS_ulTouchIrq = INUM_LRADC_TOUCH_IRQ;
    ptsdev->TS_ulAdcIrq = INUM_LRADC_CH5_IRQ;
    ptsdev->TS_ptsdrvdata = ptsdata;

    SEL_WAKE_UP_LIST_INIT(&ptsdev->TS_selwulList);                      /*  初始化 select 等待链        */
    LW_SPIN_INIT(&ptsdev->TS_slLock);                                   /*  初始化自旋锁                */

    iRet = iosDevAddEx(&ptsdev->TS_devhdr,
                       cpcName,
                       _G_iTsDrvNum,
                       DT_CHR);
    if (iRet) {
        _DebugHandle(__ERRORMESSAGE_LEVEL, "Add touchscr device to Sylixos error.\r\n");
        _ErrorHandle(ERROR_SYSTEM_LOW_MEMORY);
        __SHEAP_FREE(ptsdev);
        return  (PX_ERROR);
    }

    ptsdev->TS_timeCreate = lib_time(LW_NULL);

    __tsSysInit(ptsdev);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
  END
*********************************************************************************************************/

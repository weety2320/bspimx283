/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: lcd.c
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 03 月 06 日
**
** 描        述: IM283 LCD 驱动
*********************************************************************************************************/
#define __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <linux/err.h>

#include "lcd.h"
#include "driver/regs/regs_lcdif.h"
#include "driver/regs/regs_pwm.h"
#include "driver/regs/pinctrl.h"
#include "driver/clock/clk.h"
/*********************************************************************************************************
  宏定义
*********************************************************************************************************/
#define LCDIF_CLK__XTAL             (1)
#define LCDIF_CLK_REF               (0)
#define LCD_XSIZE                   (480)
#define LCD_YSIZE                   (272)
#define LCD_FREQ                    (8000000)

#define DOTCLK_H_ACTIVE             (480)
#define DOTCLK_H_PULSE_WIDTH        (41)
#define DOTCLK_HF_PORCH             (5)
#define DOTCLK_HB_PORCH             (5)
#define DOTCLK_H_WAIT_CNT           (DOTCLK_H_PULSE_WIDTH + DOTCLK_HB_PORCH)
#define DOTCLK_H_PERIOD             (DOTCLK_H_WAIT_CNT + DOTCLK_HF_PORCH + DOTCLK_H_ACTIVE)

#define DOTCLK_V_ACTIVE             (272)
#define DOTCLK_V_PULSE_WIDTH        (20)
#define DOTCLK_VF_PORCH             (5)
#define DOTCLK_VB_PORCH             (5)
#define DOTCLK_V_WAIT_CNT           (DOTCLK_V_PULSE_WIDTH + DOTCLK_VB_PORCH)
#define DOTCLK_V_PERIOD             (DOTCLK_VF_PORCH + DOTCLK_V_ACTIVE + DOTCLK_V_WAIT_CNT)
/*********************************************************************************************************
  LCD 设备结构
*********************************************************************************************************/
typedef struct lcd_dev {
    LW_GM_DEVICE            LCDD_gmdev;
    LW_GM_FILEOPERATIONS    LCDD_gmfo;
    PVOID                   LCDD_pvFbBase;
    addr_t                  LCDD_atReg;
    INT                     LCDD_iX;
    INT                     LCDD_iY;
    INT                     LCDD_iBpp;
    INT                     LCDD_iLcdMode;
#define                     LCDD_RGB555    (0)
#define                     LCDD_RGB565    (1)
    struct clk             *LCDD_clk;
    struct clk             *LCDD_clkBl;
    INT                     LCDD_iClkFreq;
} LCD_DEV;
/*********************************************************************************************************
  显示信息
*********************************************************************************************************/
static LCD_DEV              _G_lcddev;
static INT                  _G_iLock = 0;
/*********************************************************************************************************
** 函数名称: __lcdOn
** 功能描述: 打开 LCD 控制器
** 输　入  : plcddev  :  LCD 设备
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __lcdOn (LCD_DEV  *plcddev)
{
    writel(BM_LCDIF_CTRL_LCDIF_MASTER,
           plcddev->LCDD_atReg + HW_LCDIF_CTRL_SET);

    writel(BM_LCDIF_CTRL_RUN,
           plcddev->LCDD_atReg + HW_LCDIF_CTRL_SET);
}
/*********************************************************************************************************
** 函数名称: __lcdPinInit
** 功能描述: LCD 引脚初始化
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static  VOID  __lcdPinInit (VOID)
{
    lcdif_port_init();
}
/*********************************************************************************************************
** 函数名称: __lcdBlInit
** 功能描述: 初始化背光设备
** 输　入  : plcddev  :  LCD 设备
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT __lcdBlInit (LCD_DEV  *plcddev)
{
    INT iRet = 0;

    plcddev->LCDD_clkBl = clk_get(NULL, NULL, "pwm");
    if (IS_ERR(plcddev->LCDD_clkBl)) {
        iRet = PTR_ERR(plcddev->LCDD_clkBl);
        return  (iRet);
    }

    clk_enable(plcddev->LCDD_clkBl);

    mxs_reset_block((PVOID)REGS_PWM_BASE, 1);

    writel(BF_PWM_ACTIVEn_INACTIVE(0) |
           BF_PWM_ACTIVEn_ACTIVE(0),
           REGS_PWM_BASE + HW_PWM_ACTIVEn(3));

    writel(BF_PWM_PERIODn_CDIV(5)           |
           BF_PWM_PERIODn_INACTIVE_STATE(2) |
           BF_PWM_PERIODn_ACTIVE_STATE(3)   |
           BF_PWM_PERIODn_PERIOD(399),
           REGS_PWM_BASE + HW_PWM_PERIODn(3));

    writel(BM_PWM_CTRL_PWM3_ENABLE,
           REGS_PWM_BASE + HW_PWM_CTRL_SET);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lcdDotclkRelease
** 功能描述: 释放 dotclk
** 输　入  : plcddev   : LCD 设备
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID __lcdDotclkRelease (LCD_DEV  *plcddev)
{
    writel(BM_LCDIF_CTRL_DOTCLK_MODE,
           plcddev->LCDD_atReg + HW_LCDIF_CTRL_CLR);

    writel(0, plcddev->LCDD_atReg + HW_LCDIF_VDCTRL0);
    writel(0, plcddev->LCDD_atReg + HW_LCDIF_VDCTRL1);
    writel(0, plcddev->LCDD_atReg + HW_LCDIF_VDCTRL2);
    writel(0, plcddev->LCDD_atReg + HW_LCDIF_VDCTRL3);
}
/*********************************************************************************************************
** 函数名称: __lcdDotclkSetup
** 功能描述: 建立 LCD 时钟
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __lcdDotclkSetup (UINT16 v_pulse_width,
                               UINT16 v_period,
                               UINT16 v_wait_cnt,
                               UINT16 v_active,
                               UINT16 h_pulse_width,
                               UINT16 h_period,
                               UINT16 h_wait_cnt,
                               UINT16 h_active,
                               INT    enable_present)
{
    UINT32 val;

    writel(BM_LCDIF_CTRL_DATA_SHIFT_DIR,
           REGS_LCDIF_BASE + HW_LCDIF_CTRL_CLR);

    writel(BM_LCDIF_CTRL_SHIFT_NUM_BITS,
           REGS_LCDIF_BASE + HW_LCDIF_CTRL_CLR);

    writel(BM_LCDIF_CTRL1_BYTE_PACKING_FORMAT,
           REGS_LCDIF_BASE + HW_LCDIF_CTRL1_CLR);
    writel(BF_LCDIF_CTRL1_BYTE_PACKING_FORMAT(0xf) |
           BM_LCDIF_CTRL1_RECOVER_ON_UNDERFLOW,
           REGS_LCDIF_BASE + HW_LCDIF_CTRL1_SET);

    val = readl(REGS_LCDIF_BASE + HW_LCDIF_TRANSFER_COUNT);
    val &= ~(BM_LCDIF_TRANSFER_COUNT_V_COUNT |
             BM_LCDIF_TRANSFER_COUNT_H_COUNT);
    val |= BF_LCDIF_TRANSFER_COUNT_H_COUNT(h_active) |
           BF_LCDIF_TRANSFER_COUNT_V_COUNT(v_active);
    writel(val, REGS_LCDIF_BASE + HW_LCDIF_TRANSFER_COUNT);

    writel(BM_LCDIF_CTRL_VSYNC_MODE,
           REGS_LCDIF_BASE + HW_LCDIF_CTRL_CLR);
    writel(BM_LCDIF_CTRL_WAIT_FOR_VSYNC_EDGE,
           REGS_LCDIF_BASE + HW_LCDIF_CTRL_CLR);
    writel(BM_LCDIF_CTRL_DVI_MODE,
           REGS_LCDIF_BASE + HW_LCDIF_CTRL_CLR);
    writel(BM_LCDIF_CTRL_DOTCLK_MODE,
           REGS_LCDIF_BASE + HW_LCDIF_CTRL_SET);
    writel(BM_LCDIF_CTRL_BYPASS_COUNT,
           REGS_LCDIF_BASE + HW_LCDIF_CTRL_SET);

    writel(BM_LCDIF_CTRL_WORD_LENGTH |
           BM_LCDIF_CTRL_INPUT_DATA_SWIZZLE |
           BM_LCDIF_CTRL_LCD_DATABUS_WIDTH |
           BM_LCDIF_CTRL_DATA_FORMAT_16_BIT,
           REGS_LCDIF_BASE + HW_LCDIF_CTRL_CLR);
    writel(BF_LCDIF_CTRL_WORD_LENGTH(0) |
           BM_LCDIF_CTRL_DATA_SELECT |
           BF_LCDIF_CTRL_INPUT_DATA_SWIZZLE(0) |
           BF_LCDIF_CTRL_LCD_DATABUS_WIDTH(0),
           REGS_LCDIF_BASE + HW_LCDIF_CTRL_SET);

    val = readl(REGS_LCDIF_BASE + HW_LCDIF_VDCTRL0);
    val &= ~(BM_LCDIF_VDCTRL0_VSYNC_POL  |
             BM_LCDIF_VDCTRL0_HSYNC_POL  |
             BM_LCDIF_VDCTRL0_ENABLE_POL |
             BM_LCDIF_VDCTRL0_DOTCLK_POL);
    val |= BM_LCDIF_VDCTRL0_ENABLE_POL |
           BM_LCDIF_VDCTRL0_DOTCLK_POL;
    writel(val, REGS_LCDIF_BASE + HW_LCDIF_VDCTRL0);

    val = readl(REGS_LCDIF_BASE + HW_LCDIF_VDCTRL0);
    val &= ~(BM_LCDIF_VDCTRL0_VSYNC_OEB);
    writel(val, REGS_LCDIF_BASE + HW_LCDIF_VDCTRL0);                      /*  vsync is output           */

    /*
     * need enable sig for true RGB i/f.  Or, if not true RGB, leave it
     * zero.
     */
    val = readl(REGS_LCDIF_BASE + HW_LCDIF_VDCTRL0);
    val |= BM_LCDIF_VDCTRL0_ENABLE_PRESENT;
    writel(val, REGS_LCDIF_BASE + HW_LCDIF_VDCTRL0);

    /*
     * For DOTCLK mode, count VSYNC_PERIOD in terms of complete hz lines
     */
    val = readl(REGS_LCDIF_BASE + HW_LCDIF_VDCTRL0);
    val &= ~(BM_LCDIF_VDCTRL0_VSYNC_PERIOD_UNIT |
             BM_LCDIF_VDCTRL0_VSYNC_PULSE_WIDTH_UNIT);
    val |= BM_LCDIF_VDCTRL0_VSYNC_PERIOD_UNIT |
           BM_LCDIF_VDCTRL0_VSYNC_PULSE_WIDTH_UNIT;
    writel(val, REGS_LCDIF_BASE + HW_LCDIF_VDCTRL0);

    writel(BM_LCDIF_VDCTRL0_VSYNC_PULSE_WIDTH,
           REGS_LCDIF_BASE + HW_LCDIF_VDCTRL0_CLR);
    writel(v_pulse_width, REGS_LCDIF_BASE + HW_LCDIF_VDCTRL0_SET);

    writel(BF_LCDIF_VDCTRL1_VSYNC_PERIOD(v_period),
           REGS_LCDIF_BASE + HW_LCDIF_VDCTRL1);

    writel(BF_LCDIF_VDCTRL2_HSYNC_PULSE_WIDTH(h_pulse_width) |
           BF_LCDIF_VDCTRL2_HSYNC_PERIOD(h_period),
           REGS_LCDIF_BASE + HW_LCDIF_VDCTRL2);

    val = readl(REGS_LCDIF_BASE + HW_LCDIF_VDCTRL4);
    val &= ~BM_LCDIF_VDCTRL4_DOTCLK_H_VALID_DATA_CNT;
    val |= BF_LCDIF_VDCTRL4_DOTCLK_H_VALID_DATA_CNT(h_active);
    writel(val, REGS_LCDIF_BASE + HW_LCDIF_VDCTRL4);

    val = readl(REGS_LCDIF_BASE + HW_LCDIF_VDCTRL3);
    val &= ~(BM_LCDIF_VDCTRL3_HORIZONTAL_WAIT_CNT |
             BM_LCDIF_VDCTRL3_VERTICAL_WAIT_CNT);
    val |= BF_LCDIF_VDCTRL3_HORIZONTAL_WAIT_CNT(h_wait_cnt) |
           BF_LCDIF_VDCTRL3_VERTICAL_WAIT_CNT(v_wait_cnt);
    writel(val, REGS_LCDIF_BASE + HW_LCDIF_VDCTRL3);

    val = readl(REGS_LCDIF_BASE + HW_LCDIF_VDCTRL4);
    val |= BM_LCDIF_VDCTRL4_SYNC_SIGNALS_ON;
    writel(val, REGS_LCDIF_BASE + HW_LCDIF_VDCTRL4);
}
/*********************************************************************************************************
** 函数名称: __lcdTimeing
** 功能描述: LCD timeing 设置
** 输　入  : plcddev  :  LCD 设备
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __lcdTimeing (LCD_DEV  *plcddev)
{
    UINT  uiDataSetup = 1;
    UINT  uiDataHold;
    UINT  uiCmdSetup;
    UINT  uiCmdHold;

    uiDataHold = 1 << 8;
    uiCmdSetup = 1 << 16;
    uiCmdHold  = 1 << 24;

    writel(uiDataSetup |
           uiDataHold  |
           uiCmdSetup  |
           uiCmdHold   ,
           plcddev->LCDD_atReg + HW_LCDIF_TIMING);
}
/*********************************************************************************************************
** 函数名称: __lcdInit
** 功能描述: LCD 初始化
** 输　入  : plcddev  :  LCD 设备
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __lcdInit (LCD_DEV  *plcddev)
{
    /*
     *  打开 LCD 时钟
     */
    writel(BM_LCDIF_CTRL_CLKGATE,
            plcddev->LCDD_atReg + HW_LCDIF_CTRL_CLR);
    /*
     * 复位 LCD 控制器 软复位
     */
    writel(BM_LCDIF_CTRL_SFTRST, plcddev->LCDD_atReg + HW_LCDIF_CTRL_SET);
    udelay(10);

    /*
     * 使能正常的 LCD 操作
     */
    writel(BM_LCDIF_CTRL_SFTRST | BM_LCDIF_CTRL_CLKGATE,
            plcddev->LCDD_atReg + HW_LCDIF_CTRL_CLR);

    /*
     * 选择 LCD 模式(8080) BUSY_DISABLED
     */
    writel(BM_LCDIF_CTRL1_MODE86,
            plcddev->LCDD_atReg + HW_LCDIF_CTRL1_CLR);
    writel(BM_LCDIF_CTRL1_BUSY_ENABLE,
            plcddev->LCDD_atReg + HW_LCDIF_CTRL1_CLR);

    /*
     *  LCD 复位
     */
    writel(BM_LCDIF_CTRL1_RESET,
            plcddev->LCDD_atReg + HW_LCDIF_CTRL1_SET);
    /*
     * VSYNC is an input by default
     */
    writel(BM_LCDIF_VDCTRL0_VSYNC_OEB,
            plcddev->LCDD_atReg + HW_LCDIF_VDCTRL0_SET);

    /*
     * Reset display
     */
    writel(BM_LCDIF_CTRL1_RESET,
            plcddev->LCDD_atReg + HW_LCDIF_CTRL1_CLR);
    udelay(10);

    writel(BM_LCDIF_CTRL1_RESET,
            plcddev->LCDD_atReg + HW_LCDIF_CTRL1_SET);
    udelay(10);
}
/*********************************************************************************************************
** 函数名称: __lcdDmaInit
** 功能描述: LCD DMA 初始化
** 输　入  : plcddev  : LCD 设备
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lcdDmaInit (LCD_DEV  *plcddev)
{
    writel(BM_LCDIF_CTRL_LCDIF_MASTER,
           plcddev->LCDD_atReg + HW_LCDIF_CTRL_SET);

    writel((UINT32)plcddev->LCDD_pvFbBase,
            plcddev->LCDD_atReg + HW_LCDIF_CUR_BUF);
    writel((UINT32)plcddev->LCDD_pvFbBase,
            plcddev->LCDD_atReg + HW_LCDIF_NEXT_BUF);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lcdDmaRelease
** 功能描述: DMA release
** 输　入  : plcddev   :  LCD 设备
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __lcdDmaRelease (LCD_DEV  *plcddev)
{
    writel(BM_LCDIF_CTRL_LCDIF_MASTER,
           plcddev->LCDD_atReg + HW_LCDIF_CTRL_CLR);
}
/*********************************************************************************************************
** 函数名称: __lcdRelease
** 功能描述: 释放 panel
** 输　入  : plcddev   : LCD 设备
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID __lcdRelease (LCD_DEV  *plcddev)
{
    __lcdDotclkRelease(plcddev);
    __lcdDmaRelease(plcddev);

    clk_disable(plcddev->LCDD_clk);
    clk_put(plcddev->LCDD_clk);
}
/*********************************************************************************************************
** 函数名称: __lcdInitPanel
** 功能描述: 初始化 LCD panel
** 输　入  : plcddev   : LCD 设备
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lcdInitPanel (LCD_DEV  *plcddev)
{
    INT  iRet;

    plcddev->LCDD_clk = clk_get(NULL, NULL, "dis_lcdif");
    if (IS_ERR(plcddev->LCDD_clk)) {
        iRet = PTR_ERR(plcddev->LCDD_clk);
        goto __out;
    }

    iRet = clk_enable(plcddev->LCDD_clk);
    if (iRet) {
        clk_put(plcddev->LCDD_clk);
        goto __out;
    }

    iRet = clk_set_rate(plcddev->LCDD_clk, plcddev->LCDD_iClkFreq);
    if (iRet) {
        clk_disable(plcddev->LCDD_clk);
        clk_put(plcddev->LCDD_clk);
        goto __out;
    }

    /*
     * Make sure we do a high-to-low transition to reset the panel.
     * First make it low for 100 msec, hi for 10 msec, low for 10 msec,
     * then hi.
     */
    writel(BM_LCDIF_CTRL1_RESET,
           plcddev->LCDD_atReg + HW_LCDIF_CTRL1_CLR);                     /*  low                       */
    udelay(100 * 1000);
    writel(BM_LCDIF_CTRL1_RESET,
           plcddev->LCDD_atReg + HW_LCDIF_CTRL1_SET);                     /*  high                      */
    udelay(10 * 1000);
    writel(BM_LCDIF_CTRL1_RESET,
           plcddev->LCDD_atReg + HW_LCDIF_CTRL1_CLR);                     /*  low                       */

    /*
     * For the Samsung, Reset must be held low at least 30 uSec
     * Therefore, we'll hold it low for about 10 mSec just to be sure.
     * Then we'll wait 1 mSec afterwards.
     */
    udelay(10 * 1000);
    writel(BM_LCDIF_CTRL1_RESET,
           plcddev->LCDD_atReg + HW_LCDIF_CTRL1_SET);                     /*  high                      */
    udelay(1 * 1000);

    __lcdDotclkSetup(DOTCLK_V_PULSE_WIDTH,
                     DOTCLK_V_PERIOD,
                     DOTCLK_V_WAIT_CNT,
                     DOTCLK_V_ACTIVE,
                     DOTCLK_H_PULSE_WIDTH,
                     DOTCLK_H_PERIOD,
                     DOTCLK_H_WAIT_CNT,
                     DOTCLK_H_ACTIVE, 0);

    __lcdDmaInit(plcddev);

__out:
    return  (iRet);
}
/*********************************************************************************************************
** 函数名称: __lcdOpen
** 功能描述: 打开 FB
** 输　入  : pgmdev    :  fb 设备
**           iFlag     :  打开设备标志
**           iMode     :  打开设备模式
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lcdOpen (PLW_GM_DEVICE  pgmdev, INT  iFlag, INT  iMode)
{
    LCD_DEV     *plcddev;

    if (_G_iLock == 1) {
        return  (ERROR_NONE);
    }

    plcddev = _LIST_ENTRY(pgmdev, LCD_DEV, LCDD_gmdev);
    if (!plcddev) {
        return  (PX_ERROR);
    }

    __lcdInitPanel(plcddev);
    __lcdOn(plcddev);
    __lcdBlInit(plcddev);

    _G_iLock = 1;

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lcdClose
** 功能描述: FB 关闭函数
** 输　入  : pgmdev    :  fb 设备
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lcdClose (PLW_GM_DEVICE  pgmdev)
{
    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lcdGetVarInfo
** 功能描述: 获得 FB 信息
** 输　入  : pgmdev    :  fb 设备
**           pgmsi     :  fb 屏幕信息
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT      __lcdGetVarInfo (PLW_GM_DEVICE  pgmdev, PLW_GM_VARINFO  pgmvi)
{
    LCD_DEV     *plcddev;

    plcddev = _LIST_ENTRY(pgmdev, LCD_DEV, LCDD_gmdev);
    if (!plcddev) {
        return  (PX_ERROR);
    }

    if (pgmvi) {
        pgmvi->GMVI_ulXRes = LCD_XSIZE;
        pgmvi->GMVI_ulYRes = LCD_YSIZE;

        pgmvi->GMVI_ulXResVirtual = LCD_XSIZE;
        pgmvi->GMVI_ulYResVirtual = LCD_YSIZE;

        pgmvi->GMVI_ulXOffset = 0;
        pgmvi->GMVI_ulYOffset = 0;

        pgmvi->GMVI_ulBitsPerPixel  = 16;
        pgmvi->GMVI_ulBytesPerPixel = 2;

        pgmvi->GMVI_ulGrayscale = 65536;

        /*
         *  RGB565 RED 0xF800, GREEN 0x07E0, BLUE 0x001F
         */
        if (plcddev->LCDD_iLcdMode == LCDD_RGB565) {
            pgmvi->GMVI_ulRedMask   = 0xF800;
            pgmvi->GMVI_ulGreenMask = 0x07E0;
            pgmvi->GMVI_ulBlueMask  = 0x001F;
            pgmvi->GMVI_ulTransMask = 0;
        }

        pgmvi->GMVI_bHardwareAccelerate = LW_FALSE;
        pgmvi->GMVI_ulMode              = LW_GM_SET_MODE;
        pgmvi->GMVI_ulStatus            = 0;
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lcdGetScrInfo
** 功能描述: 获得 LCD 屏幕信息
** 输　入  : pgmdev    :  fb 设备
**           pgmsi     :  fb 屏幕信息
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT      __lcdGetScrInfo (PLW_GM_DEVICE  pgmdev, PLW_GM_SCRINFO  pgmsi)
{
    LCD_DEV     *plcddev;

    plcddev = _LIST_ENTRY(pgmdev, LCD_DEV, LCDD_gmdev);
    if (!plcddev) {
        return  (PX_ERROR);
    }

    if (pgmsi) {
        pgmsi->GMSI_pcName    = "/dev/fb0";
        pgmsi->GMSI_ulId      = 0;
        pgmsi->GMSI_stMemSize        = LCD_XSIZE * LCD_YSIZE * 2;
        pgmsi->GMSI_stMemSizePerLine = LCD_XSIZE * 2;
        pgmsi->GMSI_pcMem            = (caddr_t)plcddev->LCDD_pvFbBase;
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: lcdDevCreate
** 功能描述: 创建 LCD 设备 (fb0)
** 输　入  : NONE
** 输　出  : 错误代码.
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  lcdDevCreate (VOID)
{
    static INT  iLock = 0;

    if (iLock) {
        return  (ERROR_NONE);
    }

    if (!_G_lcddev.LCDD_pvFbBase) {
        _G_lcddev.LCDD_pvFbBase = API_VmmDmaAllocAlign(LCD_XSIZE * LCD_YSIZE * 2,
                                                       4 * 1024 * 1024);/*  4 MB 对齐地址               */
        if (_G_lcddev.LCDD_pvFbBase == LW_NULL) {
            return  (PX_ERROR);
        }
    }

    /*
     *  仅支持 framebuffer 模式.
     */
    _G_lcddev.LCDD_gmfo.GMFO_pfuncOpen = __lcdOpen;
    _G_lcddev.LCDD_gmfo.GMFO_pfuncClose = __lcdClose;
    _G_lcddev.LCDD_gmfo.GMFO_pfuncGetVarInfo = (INT (*)(LONG, PLW_GM_VARINFO))__lcdGetVarInfo;
    _G_lcddev.LCDD_gmfo.GMFO_pfuncGetScrInfo = (INT (*)(LONG, PLW_GM_SCRINFO))__lcdGetScrInfo;
    _G_lcddev.LCDD_gmdev.GMDEV_gmfileop = &_G_lcddev.LCDD_gmfo;

    _G_lcddev.LCDD_atReg = (addr_t)REGS_LCDIF_BASE;
    _G_lcddev.LCDD_iClkFreq = LCD_FREQ;
    _G_lcddev.LCDD_iLcdMode = LCDD_RGB565;

    __lcdPinInit();
    __lcdInit(&_G_lcddev);
    __lcdTimeing(&_G_lcddev);

    return  (gmemDevAdd("/dev/fb0", &_G_lcddev.LCDD_gmdev));
}
/*********************************************************************************************************
  END
*********************************************************************************************************/

/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: lradc.c
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: IM283 LRADC 驱动
*********************************************************************************************************/
#define __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <linux/compat.h>

#include "lradc.h"
#include "driver/regs/regs_lradc.h"
#include "driver/regs/mx28.h"
/*********************************************************************************************************
  LRADC 设备定义
*********************************************************************************************************/
typedef struct lradc_device {
    LW_DEV_HDR          LRADC_devhdr;
	addr_t              LRADC_atBase;
	LRADC_DATA         *LRADC_pdata;
	INT                 LRADC_iFreq;
	INT                 LRADC_iOnChipGrp;

    /*
     *  系统相关变量
     */
    LW_SPINLOCK_DEFINE (LRADC_slLock);
    time_t              LRADC_timeCreate;                               /*  设备创建时间                */
    LW_SEL_WAKEUPLIST   LRADC_selwulList;                               /*  select() 等待链             */
} LRADC_DEV;
/*********************************************************************************************************
  定义驱动号全局通道管理
*********************************************************************************************************/
static INT              _G_iLradcDrvNum = 0;
static INT              _G_iChannels[8];
/*********************************************************************************************************
** 函数名称: __lradcUseChan
** 功能描述: 通道使用申请
** 输　入  : plradcdev   LRADC 设备地址
**           iChan       通道号
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lradcUseChan (LRADC_DEV  *plradcdev, INT   iChan)
{
    INTREG    iRegInterLevel;

	if (iChan < 0 || iChan > 7) {
	    _ErrorHandle(EINVAL);
		return  (PX_ERROR);
	}

    LW_SPIN_LOCK_QUICK(&plradcdev->LRADC_slLock, &iRegInterLevel);
	_G_iChannels[iChan]++;
    LW_SPIN_UNLOCK_QUICK(&plradcdev->LRADC_slLock, iRegInterLevel);

	return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lradcUnuseChan
** 功能描述: 通道使用释放
** 输　入  : plradcdev   LRADC 设备地址
**           iChan       通道号
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lradcUnuseChan (LRADC_DEV  *plradcdev, INT   iChan)
{
    INTREG    iRegInterLevel;

	if (iChan < 0 || iChan > 7) {
        _ErrorHandle(EINVAL);
        return  (PX_ERROR);
	}

    LW_SPIN_LOCK_QUICK(&plradcdev->LRADC_slLock, &iRegInterLevel);
	_G_iChannels[iChan]--;
    LW_SPIN_UNLOCK_QUICK(&plradcdev->LRADC_slLock, iRegInterLevel);

	return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lradcPresent
** 功能描述: 判断通道是否存在
** 输　入  : plradcdev       LRADC 设备地址
**           iChan           通道
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lradcPresent (LRADC_DEV  *plradcdev, INT  iChan)
{
    if (iChan < 0 || iChan > 7) {
        return 0;
    }

    return readl(plradcdev->LRADC_atBase + HW_LRADC_STATUS)
        & (1 << (16 + iChan));
}
/*********************************************************************************************************
** 函数名称: __lradcSetDelayTrigger
** 功能描述: 设置延时触发
** 输　入  : plradcdev       LRADC 设备地址
**           iTrigger        触发值
**           uiTriggerLradc  设置 ADC 转换触发
**           uiDelayTriggers 延时触发
**           uiLoops         触发循环
**           uiDelays        延时数
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID __lradcSetDelayTrigger (LRADC_DEV  *plradcdev,
                                    INT         iTrigger,
                                    UINT32      uiTriggerLradc,
                                    UINT32      uiDelayTriggers,
                                    UINT32      uiLoops,
                                    UINT32      uiDelays)
{
    /*
     * set TRIGGER_LRADCS in HW_LRADC_DELAYn
     */
    writel(BF_LRADC_DELAYn_TRIGGER_LRADCS(uiTriggerLradc),
           plradcdev->LRADC_atBase + HW_LRADC_DELAYn_SET(iTrigger));
    writel(BF_LRADC_DELAYn_TRIGGER_DELAYS(uiDelayTriggers),
           plradcdev->LRADC_atBase + HW_LRADC_DELAYn_SET(iTrigger));

    writel(BM_LRADC_DELAYn_LOOP_COUNT | BM_LRADC_DELAYn_DELAY,
           plradcdev->LRADC_atBase + HW_LRADC_DELAYn_CLR(iTrigger));
    writel(BF_LRADC_DELAYn_LOOP_COUNT(uiLoops),
           plradcdev->LRADC_atBase  + HW_LRADC_DELAYn_SET(iTrigger));
    writel(BF_LRADC_DELAYn_DELAY(uiDelays),
           plradcdev->LRADC_atBase + HW_LRADC_DELAYn_SET(iTrigger));
}
/*********************************************************************************************************
** 函数名称: __lradcClearDelayTrigger
** 功能描述: 清除延时触发
** 输　入  : plradcdev       LRADC 设备地址
**           iTrigger        触发值
**           uiTriggerLradc  设置 ADC 转换触发
**           uiDelayTriggers 延时触发
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __lradcClearDelayTrigger (LRADC_DEV  *plradcdev,
                                       INT         iTrigger,
                                       UINT32      uiTriggerLradc,
                                       UINT32      uiDelayTriggers)
{
    writel(BF_LRADC_DELAYn_TRIGGER_LRADCS(uiTriggerLradc),
           plradcdev->LRADC_atBase + HW_LRADC_DELAYn_CLR(iTrigger));
    writel(BF_LRADC_DELAYn_TRIGGER_DELAYS(uiDelayTriggers),
           plradcdev->LRADC_atBase + HW_LRADC_DELAYn_CLR(iTrigger));
}
/*********************************************************************************************************
** 函数名称: __lradcCfgChannel
** 功能描述: 配置通道
** 输　入  : plradcdev   LRADC 设备地址
**           iChan       通道号
**           iEnableDiv2 使能2分频
**           iEnableAcc  使能累计器
**           iSamples    采样设置
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lradcCfgChannel (LRADC_DEV  *plradcdev,
                                INT         iChan,
                                INT         iEnableDiv2,
                                INT         iEnableAcc,
                                INT         iSamples)
{
    if (_G_iChannels[iChan] <= 0) {
        return  (PX_ERROR);
    }

    if (iEnableDiv2) {
        writel(BF_LRADC_CTRL2_DIVIDE_BY_TWO(1 << iChan),
               plradcdev->LRADC_atBase + HW_LRADC_CTRL2_SET);
    } else {
        writel(BF_LRADC_CTRL2_DIVIDE_BY_TWO(1 << iChan),
               plradcdev->LRADC_atBase + HW_LRADC_CTRL2_CLR);
    }

    /*
     * Clear the accumulator & NUM_SAMPLES
     */
    writel(0xFFFFFFFF,
           plradcdev->LRADC_atBase +
           HW_LRADC_CHn_CLR(iChan));

    /*
     * Sets NUM_SAMPLES bitfield of HW_LRADC_CHn register.
     */
    writel(BM_LRADC_CHn_NUM_SAMPLES,
           plradcdev->LRADC_atBase + HW_LRADC_CHn_CLR(iChan));
    writel(BF_LRADC_CHn_NUM_SAMPLES(iSamples),
           plradcdev->LRADC_atBase + HW_LRADC_CHn_SET(iChan));

    if (iEnableAcc) {
        writel(BM_LRADC_CHn_ACCUMULATE,
               plradcdev->LRADC_atBase + HW_LRADC_CHn_SET(iChan));
    } else {
        writel(BM_LRADC_CHn_ACCUMULATE,
               plradcdev->LRADC_atBase + HW_LRADC_CHn_CLR(iChan));
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lradcInitLadder
** 功能描述: 驱动系统层次初始化
** 输　入  : plradcdev   LRADC 设备地址
**           iChan       通道号
**           iTrigger    触发值
**           iSampling   采样设置
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lradcInitLadder (LRADC_DEV  *plradcdev,
                               INT         iChan,
                               INT         iTrigger,
                               UINT        iSampling)
{
    if (_G_iChannels[iChan] <= 0) {
        return  (PX_ERROR);
    }

	/*
	 * check if the lradc channel is present in this product
	 */
	if (!__lradcPresent(plradcdev, iChan)) {
        _ErrorHandle(EINVAL);
        return  (PX_ERROR);
	}

	__lradcCfgChannel(plradcdev, iChan, !0, 0, 0);

	/*
	 * Setup the trigger loop forever
	 */
	__lradcSetDelayTrigger(plradcdev,
	                       iTrigger,
	                       1 << iChan,
	                       1 << iTrigger,
	                       0, iSampling);

	/*
	 * Clear the accumulator & NUM_SAMPLES
	 */
	writel(0xFFFFFFFF,
	       plradcdev->LRADC_atBase +
	       HW_LRADC_CHn_CLR(iChan));

	return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lradcStopLadder
** 功能描述: 驱动系统层次初始化
** 输　入  : plradcdev   LRADC 设备地址
**           iChan       通道号
**           iTrigger    触发值
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lradcStopLadder (LRADC_DEV  *plradcdev,
                               INT         iChan,
                               INT         iTrigger)
{
    if (_G_iChannels[iChan] <= 0) {
        return  (PX_ERROR);
    }

	/*
	 * check if the lradc channel is present in this product
	 */
	if (!__lradcPresent(plradcdev, iChan)) {
        _ErrorHandle(EINVAL);
        return  (PX_ERROR);
	}

	__lradcClearDelayTrigger(plradcdev, iTrigger, 1 << iChan, 1 << iTrigger);

	return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lradcSetDelayTriggerKick
** 功能描述: 延时触发设置
** 输　入  : plradcdev   LRADC 设备地址
**           iTrigger    触发设置值
**           iValue      值
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void __lradcSetDelayTriggerKick (LRADC_DEV  *plradcdev,
                                 INT         iTrigger,
                                 INT         iValue)
{
	if (iValue) {
		writel(BM_LRADC_DELAYn_KICK,
		       plradcdev->LRADC_atBase + HW_LRADC_DELAYn_SET(iTrigger));
	} else {
	    writel(BM_LRADC_DELAYn_KICK,
	           plradcdev->LRADC_atBase + HW_LRADC_DELAYn_CLR(iTrigger));
	}
}
/*********************************************************************************************************
** 函数名称: __lradcVddio
** 功能描述: 驱动系统层次初始化
** 输　入  : plradcdev   LRADC 设备地址
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static UINT32  __lradcVddio (LRADC_DEV  *plradcdev)
{
	/*
	 * Clear the Soft Reset and Clock Gate for normal operation
	 */
    writel(BM_LRADC_CTRL0_SFTRST | BM_LRADC_CTRL0_CLKGATE,
           plradcdev->LRADC_atBase + HW_LRADC_CTRL0_CLR);

	/*
	 * Clear the divide by two for channel 6 since
	 * it has a HW divide-by-two built in.
	 */
    writel(BF_LRADC_CTRL2_DIVIDE_BY_TWO(1 << VDDIO_VOLTAGE_CH),
           plradcdev->LRADC_atBase + HW_LRADC_CTRL2_CLR);

	/*
	 * Clear the accumulator & NUM_SAMPLES
	 */
    writel(0xFFFFFFFF,
           plradcdev->LRADC_atBase + HW_LRADC_CHn_CLR(VDDIO_VOLTAGE_CH));

	/*
	 * Clear the interrupt flag
	 */
    writel(BM_LRADC_CTRL1_LRADC6_IRQ,
           plradcdev->LRADC_atBase + HW_LRADC_CTRL1_CLR);

	/*
	 * Get VddIO; this is the max scale value for the button resistor
	 * ladder.
	 * schedule ch 6:
	 */
    writel(BF_LRADC_CTRL0_SCHEDULE(1 << VDDIO_VOLTAGE_CH),
           plradcdev->LRADC_atBase + HW_LRADC_CTRL0_SET);

	/*
	 * wait for completion
	 */
	while ((readl(plradcdev->LRADC_atBase + HW_LRADC_CTRL1)
	        & BM_LRADC_CTRL1_LRADC6_IRQ) != BM_LRADC_CTRL1_LRADC6_IRQ) {
		API_TimeMSleep(1);
	}

	/*
	 * Clear the interrupt flag
	 */
	writel(BM_LRADC_CTRL1_LRADC6_IRQ,
	       plradcdev->LRADC_atBase + HW_LRADC_CTRL1_CLR);

	/*
	 * read ch 6 value.
	 */
	return  (readl(plradcdev->LRADC_atBase + HW_LRADC_CHn(VDDIO_VOLTAGE_CH)) &
			       BM_LRADC_CHn_VALUE);
}
/*********************************************************************************************************
** 函数名称: __lradcFreqSetup
** 功能描述: 重新设置频率
** 输　入  : plradcdev   LRADC 设备地址
**           iFreq       新频率
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT __lradcFreqSetup (LRADC_DEV  *plradcdev, INT  iFreq)
{
	if (iFreq < 0) {
		return  (PX_ERROR);
	}

	if (iFreq >= 6) {
	    plradcdev->LRADC_iFreq = LRADC_CLOCK_6MHZ;
	} else if (iFreq >= 4) {
	    plradcdev->LRADC_iFreq = LRADC_CLOCK_4MHZ;
	} else if (iFreq >= 3) {
	    plradcdev->LRADC_iFreq = LRADC_CLOCK_3MHZ;
	} else if (iFreq >= 2) {
	    plradcdev->LRADC_iFreq = LRADC_CLOCK_2MHZ;
	} else {
		return  (ERROR_NONE);
	}

	return  (PX_ERROR);
}

/*********************************************************************************************************
** 函数名称: __tsSysInit
** 功能描述: 驱动系统层次初始化
** 输　入  : plradcdev   LRADC 设备地址
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lradcSysInit (LRADC_DEV    *plradcdev)
{
    writel(BM_LRADC_CTRL0_SFTRST,
           plradcdev->LRADC_atBase + HW_LRADC_CTRL0_SET);
    udelay(100);
    writel(BM_LRADC_CTRL0_SFTRST,
           plradcdev->LRADC_atBase + HW_LRADC_CTRL0_CLR);

    /*
     * Clear the Clock Gate for normal operation
     */
    writel(BM_LRADC_CTRL0_CLKGATE,
           plradcdev->LRADC_atBase + HW_LRADC_CTRL0_CLR);

    if (plradcdev->LRADC_iOnChipGrp) {
        writel(BM_LRADC_CTRL0_ONCHIP_GROUNDREF,
               plradcdev->LRADC_atBase + HW_LRADC_CTRL0_SET);
    } else {
        writel(BM_LRADC_CTRL0_ONCHIP_GROUNDREF,
               plradcdev->LRADC_atBase + HW_LRADC_CTRL0_CLR);
    }

    writel(BM_LRADC_CTRL3_CYCLE_TIME,
           plradcdev->LRADC_atBase + HW_LRADC_CTRL3_CLR);
    writel(BF_LRADC_CTRL3_CYCLE_TIME(plradcdev->LRADC_iFreq),
           plradcdev->LRADC_atBase + HW_LRADC_CTRL3_SET);

    writel(BM_LRADC_CTRL4_LRADC6SELECT | BM_LRADC_CTRL4_LRADC7SELECT,
           plradcdev->LRADC_atBase + HW_LRADC_CTRL4_CLR);
    writel(BF_LRADC_CTRL4_LRADC6SELECT(plradcdev->LRADC_pdata->LRADCDT_VddioVoltage),
           plradcdev->LRADC_atBase + HW_LRADC_CTRL4_SET);
    writel(BF_LRADC_CTRL4_LRADC7SELECT(plradcdev->LRADC_pdata->LRADCDT_BatteryVoltage),
           plradcdev->LRADC_atBase + HW_LRADC_CTRL4_SET);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lradcOpen
** 功能描述: 驱动OPEN函数
** 输　入  : plradcdev   LRADC 设备地址
**           pcName      设备名
**           iFlags      标志
**           iMode       模式
** 输　出  : 错误号 或 触摸屏设备地址
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static LONG  __lradcOpen (LRADC_DEV    *plradcdev,
                          PCHAR         pcName,
                          INT           iFlags,
                          INT           iMode)
{
    if (!plradcdev) {
        _ErrorHandle(EFAULT);
        return  (PX_ERROR);
    }

    if (LW_DEV_INC_USE_COUNT(&plradcdev->LRADC_devhdr) == 1) {
        __lradcSysInit(plradcdev);
    }

    return  ((LONG)plradcdev);
}
/*********************************************************************************************************
** 函数名称: __lradcClose
** 功能描述: 驱动close函数
** 输　入  : plradcdev    LRADC 设备地址
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lradcClose (LRADC_DEV    *plradcdev)
{
    if (!plradcdev) {
        _ErrorHandle(EFAULT);
        return  (PX_ERROR);
    }

    if (LW_DEV_GET_USE_COUNT(&plradcdev->LRADC_devhdr)) {
        LW_DEV_DEC_USE_COUNT(&plradcdev->LRADC_devhdr);
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lradcRead
** 功能描述: 驱动读函数
** 输　入  : plradcdev      LRADC 设备地址
**           pvBuf          数据Buf
**           stNbyte        要读的字节数
** 输　出  : 成功读取的字节数
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static ssize_t  __lradcRead (LRADC_DEV    *plradcdev,
                             PVOID         pvBuf,
                             size_t        stNbyte)
{
    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: __lradcIoctl
** 功能描述: 驱动IOCTL
** 输　入  : plradcdev     LRADC 设备地址
**           iCmd          命令字
**           iArg          地址
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __lradcIoctl (LRADC_DEV    *plradcdev,
                          INT           iCmd,
                          LONG          lArg)
{
    PLW_SEL_WAKEUPNODE   pselwunNode;
    INT                  iError = ERROR_NONE;
    struct stat         *pstatGet;
    LRADC_REG           *plradcreg;

    if (!plradcdev) {
        _ErrorHandle(EFAULT);
        return  (PX_ERROR);
    }

    plradcreg = (LRADC_REG *)lArg;

    switch (iCmd) {

    case LRADC_CMD_USE_CHAN:
        if (!plradcreg) {
            iError = PX_ERROR;
        }

        __lradcUseChan(plradcdev, plradcreg->LRADCREG_iChan);
        break;

    case LRADC_CMD_UNUSE_CHAN:
        if (!plradcreg) {
            iError = PX_ERROR;
        }

        __lradcUnuseChan(plradcdev, plradcreg->LRADCREG_iChan);
        break;

    case LRADC_CMD_INIT_LADDR:
        if (!plradcreg) {
            iError = PX_ERROR;
        }

        iError = __lradcInitLadder(plradcdev,
                                   plradcreg->LRADCREG_iChan,
                                   plradcreg->LRADCREG_iTrigger,
                                   plradcreg->LRADCREG_iSamples);
        break;

    case LRADC_CMD_STOP_LADDR:
        if (!plradcreg) {
            iError = PX_ERROR;
        }

        iError = __lradcStopLadder(plradcdev,
                                   plradcreg->LRADCREG_iChan,
                                   plradcreg->LRADCREG_iTrigger);
        break;

    case LRADC_CMD_CFG_CHAN:
        if (!plradcreg) {
            iError = PX_ERROR;
        }

        iError = __lradcCfgChannel(plradcdev,
                                   plradcreg->LRADCREG_iChan,
                                   plradcreg->LRADCREG_iEnableDiv2,
                                   plradcreg->LRADCREG_iEnableAcc,
                                   plradcreg->LRADCREG_iSamples);
        break;

    case LRADC_CMD_SET_DELAY:
        if (!plradcreg) {
            iError = PX_ERROR;
        }

        __lradcSetDelayTrigger(plradcdev,
                               plradcreg->LRADCREG_iTrigger,
                               plradcreg->LRADCREG_uiTriggerLradc,
                               plradcreg->LRADCREG_uiDelayTriggers,
                               plradcreg->LRADCREG_uiLoops,
                               plradcreg->LRADCREG_uiDelays);
        break;

    case LRADC_CMD_CLR_DELAY:
        if (!plradcreg) {
            iError = PX_ERROR;
        }

        __lradcClearDelayTrigger(plradcdev,
                                 plradcreg->LRADCREG_iTrigger,
                                 plradcreg->LRADCREG_uiTriggerLradc,
                                 plradcreg->LRADCREG_uiDelayTriggers);
        break;

    case LRADC_CMD_SET_DELAY_KICK:
        if (!plradcreg) {
            iError = PX_ERROR;
        }

        __lradcSetDelayTriggerKick(plradcdev,
                                   plradcreg->LRADCREG_iTrigger,
                                   plradcreg->LRADCREG_iValue);
        break;

    case LRADC_CMD_VDDIO:
        __lradcVddio(plradcdev);
        break;

    case LRADC_CMD_SET_FREQ:
        if (!plradcreg) {
            iError = PX_ERROR;
        }

        iError = __lradcFreqSetup(plradcdev, plradcreg->LRADCREG_iFreq);
        break;

    case FIOFSTATGET:
        pstatGet = (struct stat *)lArg;
        if (pstatGet) {
            pstatGet->st_dev     = (dev_t)plradcdev;
            pstatGet->st_ino     = (ino_t)0;                            /*  相当于唯一节点              */
            pstatGet->st_mode    = 0444 | S_IFCHR;
            pstatGet->st_nlink   = 1;
            pstatGet->st_uid     = 0;
            pstatGet->st_gid     = 0;
            pstatGet->st_rdev    = 1;
            pstatGet->st_size    = 0;
            pstatGet->st_blksize = 0;
            pstatGet->st_blocks  = 0;
            pstatGet->st_atime   = plradcdev->LRADC_timeCreate;
            pstatGet->st_mtime   = plradcdev->LRADC_timeCreate;
            pstatGet->st_ctime   = plradcdev->LRADC_timeCreate;

        } else {
            errno  = EINVAL;
            iError = (PX_ERROR);
        }
        break;

    case FIOSELECT:
        pselwunNode = (PLW_SEL_WAKEUPNODE)lArg;
        SEL_WAKE_NODE_ADD(&plradcdev->LRADC_selwulList, pselwunNode);
        break;

    case FIOUNSELECT:
        SEL_WAKE_NODE_DELETE(&plradcdev->LRADC_selwulList, (PLW_SEL_WAKEUPNODE)lArg);
        break;

    default:
        errno  = ENOSYS;
        iError = PX_ERROR;
    }

    return  (iError);
}
/*********************************************************************************************************
** 函数名称: lradcDrv
** 功能描述: 安装设备驱动
** 输　入  : NONE
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  lradcDrv (VOID)
{
    static struct file_operations fileop;

    if (_G_iLradcDrvNum > 0) {
        return  (ERROR_NONE);
    }

    lib_bzero(&fileop, sizeof(struct file_operations));

    fileop.owner      = THIS_MODULE;
    fileop.fo_create  = __lradcOpen;
    fileop.fo_release = LW_NULL;
    fileop.fo_open    = __lradcOpen;
    fileop.fo_close   = __lradcClose;
    fileop.fo_read    = __lradcRead;
    fileop.fo_write   = LW_NULL;
    fileop.fo_ioctl   = __lradcIoctl;

    _G_iLradcDrvNum = iosDrvInstallEx(&fileop);

    DRIVER_LICENSE(_G_iLradcDrvNum,     "Dual BSD/GPL->Ver 1.0");
    DRIVER_AUTHOR(_G_iLradcDrvNum,      "Lu.Zhenping");
    DRIVER_DESCRIPTION(_G_iLradcDrvNum, "imx283 LRADC driver.");

    return  ((_G_iLradcDrvNum > 0) ? (ERROR_NONE) : (PX_ERROR));
}
/*********************************************************************************************************
** 函数名称: lradcDevCreate
** 功能描述: 创建 LRADC 设备
** 输　入  : cpcName  :  设备名
**           ptsdata  :  驱动数据
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  lradcDevCreate (CPCHAR  cpcName, LRADC_DATA  *plradcdata)
{
    LRADC_DEV    *plradcdev;
    INT           iRet;

    if (!cpcName || !plradcdata) {
        _ErrorHandle(EINVAL);
        return (PX_ERROR);
    }

    if (_G_iLradcDrvNum <= 0) {
        _ErrorHandle(ERROR_IO_NO_DRIVER);
        return  (PX_ERROR);
    }

    plradcdev = (LRADC_DEV *)__SHEAP_ALLOC(sizeof(LRADC_DEV));
    if (!plradcdev) {
        _ErrorHandle(ERROR_SYSTEM_LOW_MEMORY);
        return  (PX_ERROR);
    }

    lib_bzero(plradcdev, sizeof(LRADC_DEV));

    plradcdev->LRADC_atBase = LRADC_PHYS_ADDR;
    plradcdev->LRADC_pdata = plradcdata;
    plradcdev->LRADC_iFreq = LRADC_CLOCK_2MHZ;
    plradcdev->LRADC_iOnChipGrp = 0;                                    /*  默认关闭 on-chip ground     */

    SEL_WAKE_UP_LIST_INIT(&plradcdev->LRADC_selwulList);                /*  初始化 select 等待链        */
    LW_SPIN_INIT(&plradcdev->LRADC_slLock);                             /*  初始化自旋锁                */

    iRet = iosDevAddEx(&plradcdev->LRADC_devhdr,
                       cpcName,
                       _G_iLradcDrvNum,
                       DT_CHR);
    if (iRet) {
        _DebugHandle(__ERRORMESSAGE_LEVEL, "Add LRADC device to Sylixos error.\r\n");
        _ErrorHandle(ERROR_SYSTEM_LOW_MEMORY);
        __SHEAP_FREE(plradcdev);
        return  (PX_ERROR);
    }

    plradcdev->LRADC_timeCreate = lib_time(LW_NULL);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
  END
*********************************************************************************************************/

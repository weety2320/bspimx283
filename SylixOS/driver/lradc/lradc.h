/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: lradc.h
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 22 日
**
** 描        述: IM283 LRADC 驱动
*********************************************************************************************************/
#ifndef __LRADC_H
#define __LRADC_H

#include <sys/ioccom.h>
/*********************************************************************************************************
  LRADC 命令
*********************************************************************************************************/
#define  LRADC_CMD_USE_CHAN              _IOW('L', 0, INT)
#define  LRADC_CMD_UNUSE_CHAN            _IOW('L', 1, INT)
#define  LRADC_CMD_INIT_LADDR            _IOW('L', 2, INT)
#define  LRADC_CMD_STOP_LADDR            _IOW('L', 3, INT)
#define  LRADC_CMD_CFG_CHAN              _IOW('L', 4, INT)
#define  LRADC_CMD_SET_DELAY             _IOW('L', 5, INT)
#define  LRADC_CMD_CLR_DELAY             _IOW('L', 6, INT)
#define  LRADC_CMD_SET_DELAY_KICK        _IOW('L', 7, INT)
#define  LRADC_CMD_VDDIO                 _IOW('L', 8, INT)
#define  LRADC_CMD_SET_FREQ              _IOW('L', 9, INT)

/*********************************************************************************************************
  LRADC 通道号
*********************************************************************************************************/
#define LRADC_CH0                        (0)
#define LRADC_CH1                        (1)
#define LRADC_CH2                        (2)
#define LRADC_CH3                        (3)
#define LRADC_CH4                        (4)
#define LRADC_CH5                        (5)
#define LRADC_CH6                        (6)
#define LRADC_CH7                        (7)

/*********************************************************************************************************
  设备的通道定义
*********************************************************************************************************/
#define LRADC_TOUCH_X_PLUS               (LRADC_CH2)
#define LRADC_TOUCH_Y_PLUS               (LRADC_CH3)
#define LRADC_TOUCH_X_MINUS              (LRADC_CH4)
#define LRADC_TOUCH_Y_MINUS              (LRADC_CH5)
#define VDDIO_VOLTAGE_CH                 (LRADC_CH6)
#define BATTERY_VOLTAGE_CH               (LRADC_CH7)

/*********************************************************************************************************
  LRADC 时钟选择宏
*********************************************************************************************************/
#define LRADC_CLOCK_6MHZ                 (0)
#define LRADC_CLOCK_4MHZ                 (1)
#define LRADC_CLOCK_3MHZ                 (2)
#define LRADC_CLOCK_2MHZ                 (3)

/*********************************************************************************************************
  宏定义
*********************************************************************************************************/
#define LRADC_DELAY_TRIGGER_BUTTON       (0)
#define LRADC_DELAY_TRIGGER_BATTERY      (1)
#define LRADC_DELAY_TRIGGER_TOUCHSCREEN  (2)
#define LRADC_DELAY_TRIGGER_DIE          (3)

/*********************************************************************************************************
  定义驱动数据
*********************************************************************************************************/
typedef struct lradc_drv_data {
    UINT            LRADCDT_VddioVoltage;
    UINT            LRADCDT_BatteryVoltage;
} LRADC_DATA;

typedef struct lradc_reg_data {
    INT         LRADCREG_iChan;                                           /*  LRADC 通道号              */
    INT         LRADCREG_iTrigger;                                        /*  触发的延时号              */
    INT         LRADCREG_iEnableDiv2;                                     /*  是否使能 2 分频           */
    INT         LRADCREG_iEnableAcc;                                      /*  是否使能连续采样数到累加器*/
    INT         LRADCREG_iSamples;                                        /*  采样数                    */
    INT         LRADCREG_iValue;                                          /*  是否延时触发 KICK         */
    INT         LRADCREG_iFreq;                                           /*  频率选项                  */
    UINT32      LRADCREG_uiTriggerLradc;                                  /*  延时触发 LRADC 相应通道   */
    UINT32      LRADCREG_uiDelayTriggers;                                 /*  延时触发 DELAY 相应通道   */
    UINT32      LRADCREG_uiLoops;                                         /*  循环延时次数              */
    UINT32      LRADCREG_uiDelays;                                        /*  延时数                    */
} LRADC_REG;
/*********************************************************************************************************
** 函数名称: lradcDrv
** 功能描述: 安装设备驱动
** 输　入  : NONE
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  lradcDrv(VOID);

/*********************************************************************************************************
** 函数名称: lradcDevCreate
** 功能描述: 创建 LRADC 设备
** 输　入  : cpcName  :  设备名
**           ptsdata  :  驱动数据
** 输　出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  lradcDevCreate(CPCHAR  cpcName, LRADC_DATA  *plradcdata);


#endif                                                                    /*  __LRADC_H                 */
/*********************************************************************************************************
  END
*********************************************************************************************************/


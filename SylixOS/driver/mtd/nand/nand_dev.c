/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: nand_dev.c
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 21 日
**
** 描        述: nand 驱动
*********************************************************************************************************/
#define  __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/bbm.h>
#include <errno.h>

#include "fs/yaffs2/yaffs_guts.h"
#include "fs/yaffs2/yaffs_mtdif.h"
#include "nand_dev.h"
#include "driver/regs/mx28.h"
/*********************************************************************************************************
  全局变量
*********************************************************************************************************/
static struct yaffs_dev boot_dev;
static struct yaffs_dev comm_dev;
static char boot_dev_name[64];
static char comm_dev_name[64];
int         nand_curr_device = -1;
struct mtd_info nand_info[CONFIG_SYS_MAX_NAND_DEVICE];
static struct nand_chip nand_chip[CONFIG_SYS_MAX_NAND_DEVICE];
static char dev_name[CONFIG_SYS_MAX_NAND_DEVICE][8];
static unsigned long total_nand_size;                                     /* in kiB                     */
/*********************************************************************************************************
  函数声明
*********************************************************************************************************/
extern int board_nand_init(struct nand_chip *nand);
extern void setup_gpmi_nand(void);
/*********************************************************************************************************
** 函数名称: nandRegister
** 功能描述: nand 注册
** 输  入  : devnum  :  设备号
** 输  出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int nandRegister (int devnum)
{
    struct mtd_info *mtd;

    if (devnum >= CONFIG_SYS_MAX_NAND_DEVICE)
        return -EINVAL;

    mtd = &nand_info[devnum];

    sprintf(dev_name[devnum], "nand%d", devnum);
    mtd->name = dev_name[devnum];

#ifdef CONFIG_MTD_DEVICE
    /*
     * Add MTD device so that we can reference it later
     * via the mtdcore infrastructure (e.g. ubi).
     */
    add_mtd_device(mtd);
#endif

    total_nand_size += mtd->size / 1024;

    if (nand_curr_device == -1)
        nand_curr_device = devnum;

    return 0;
}
/*********************************************************************************************************
** 函数名称: nandInitChip
** 功能描述: nandChip 驱动初始化
** 输  入  : i  :  nand 设备号
** 输  出  : 无
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID nandInitChip (INT  i)
{
    struct mtd_info *mtd = &nand_info[i];
    struct nand_chip *nand = &nand_chip[i];
    int maxchips = CONFIG_SYS_NAND_MAX_CHIPS;
    int ret;

    if (maxchips < 1) {
        maxchips = 1;
    }

    setup_gpmi_nand();

    mtd->priv = nand;

    ret = board_nand_init(nand);
    if (ret) {
        return;
    }

    ret = nand_scan(mtd, maxchips);
    if (ret) {
        return;
    }

    nandRegister(i);
}
/*********************************************************************************************************
** 函数名称: nandInit
** 功能描述: nand 驱动初始化
** 输  入  : 无
** 输  出  : 无
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID nandInit (VOID)
{
    int i;

    for (i = 0; i < CONFIG_SYS_MAX_NAND_DEVICE; i++) {
        nandInitChip(i);
    }

    printf("%lu MiB\n", total_nand_size / 1024);
}
/*********************************************************************************************************
** 函数名称: mtdDevCreateEx
** 功能描述: mtd 设备创建
** 输  入  : pcDevName  :  设备名
** 输  出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  mtdDevCreateEx (PCHAR  pcDevName)
{
    struct mtd_info            *pmtd = &nand_info[0];

    lib_bzero(&boot_dev, sizeof(boot_dev));
    lib_bzero(&comm_dev, sizeof(comm_dev));

    snprintf(boot_dev_name, 64, "%s%d", pcDevName, 0);
    snprintf(comm_dev_name, 64, "%s%d", pcDevName, 1);

    /*
     *  Set up devices Boot
     */
    boot_dev.param.name                  = boot_dev_name;
    boot_dev.param.total_bytes_per_chunk = pmtd->writesize;
    boot_dev.param.chunks_per_block      = pmtd->erasesize / pmtd->writesize;
    boot_dev.param.n_reserved_blocks     = 10;
    boot_dev.param.start_block           = 512;
    boot_dev.param.end_block             = boot_dev.param.start_block + 128;
    boot_dev.param.spare_bytes_per_chunk = pmtd->oobsize;
    boot_dev.param.is_yaffs2             = 1;
    boot_dev.param.use_nand_ecc          = 1;
    boot_dev.param.no_tags_ecc           = 0;
    boot_dev.param.n_caches              = 10;
    boot_dev.driver_context              = (void *)pmtd;                /* mtd device for later use     */
    boot_dev.os_context                  = (void *)&boot_dev;

    /*
     *  Set up devices Comm
     */
    comm_dev.param.name                  = comm_dev_name;
    comm_dev.param.total_bytes_per_chunk = pmtd->writesize;
    comm_dev.param.chunks_per_block      = pmtd->erasesize / pmtd->writesize;
    comm_dev.param.n_reserved_blocks     = 16;
    comm_dev.param.start_block           = boot_dev.param.end_block + 1;
    comm_dev.param.end_block             = 2047;
    comm_dev.param.spare_bytes_per_chunk = pmtd->oobsize;
    comm_dev.param.is_yaffs2             = 1;
    comm_dev.param.use_nand_ecc          = 1;
    comm_dev.param.no_tags_ecc           = 0;
    comm_dev.param.n_caches              = 50;
    comm_dev.driver_context              = (void *)pmtd;                /* mtd device for later use     */
    comm_dev.os_context                  = (void *)&comm_dev;

    yaffs_mtd_drv_install(&boot_dev);
    yaffs_mtd_drv_install(&comm_dev);

    yaffs_add_device(&boot_dev);                                        /* add to yaffs device table    */
    yaffs_add_device(&comm_dev);                                        /* add to yaffs device table    */

    yaffs_mount(boot_dev_name);
    yaffs_mount(comm_dev_name);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
  END
*********************************************************************************************************/


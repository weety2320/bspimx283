/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: nand_dev.h
**
** 创   建   人: Lu.Zhenping (卢振平)
**
** 文件创建日期: 2015 年 01 月 21 日
**
** 描        述: nand 驱动
*********************************************************************************************************/
#ifndef _NAND_DEV_H_
#define _NAND_DEV_H_

/*********************************************************************************************************
  函数声明
*********************************************************************************************************/
#define NAND_MAX_CHIPS              8
#define CONFIG_SYS_NAND_BASE        0x40000000
#define CONFIG_SYS_MAX_NAND_DEVICE  1

/*********************************************************************************************************
** 函数名称: nandInit
** 功能描述: nand 驱动初始化
** 输  入  : 无
** 输  出  : 无
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID nandInit(VOID);

/*********************************************************************************************************
** 函数名称: mtdDevCreateEx
** 功能描述: mtd 设备创建
** 输  入  : pcDevName  :  设备名
** 输  出  : 错误号
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT  mtdDevCreateEx(PCHAR  pcDevName);

#endif                                                                   /*  _NAND_DEV_H_               */
/*********************************************************************************************************
  END
*********************************************************************************************************/

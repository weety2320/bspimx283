#connect to J-Link GDB Server
target remote localhost:2331

#monitor long 0x800401e0 = 0x00000002
monitor sleep 500

#reset target
monitor reset
monitor sleep 300

#select endian
monitor endian little

#set JTAG speed to 12000
monitor speed 12000

#initialize memory controler
#monitor long 0x53000000 = 0x00000000

#wait for moment
monitor sleep 20

#set cpu to svc mode(on cpu reset)
monitor reg cpsr = 0xd3

#debug in ram
monitor reg pc   = 0x40000000

#**  start controller **#
monitor long 0x800E0040 = 0x00000001

#load the debug image
load

#Set break points
#break vector
break t_main
#continue

#debug begin

